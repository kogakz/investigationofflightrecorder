# OpenJDK18のソース解析をした結果

## まとめ

ファイル構造
| No  | filenname                 | 説明                 |
| --- | ------------------------- | ------------------ |
| 1   | jfrPeriodic.cpp           | イベントを書き込むためのマクロの定義 |
| 2   | metadata.xml              | profileなどの元ネタ※    |
| 3   | default.jfc & profile.jfc | JDK付属の設定ファイルのサンプル  |

※2022/10/18 Tom SchindlさんのBlogでmetadataを整形していたものが以下であることが分かったので、イベントの妥当性はこれで終了する。
https://bestsolution-at.github.io/jfr-doc/openjdk-18.html

https://tomsondev.bestsolution.at/2020/09/14/javaflightrecorder-event-documentation/

このtwitterでJFRのdevelopperのErik Gahlinさんとやり取りして抽出方法とかを議論して妥当性を確認している。
https://twitter.com/tomsontom/status/1304532661726310401

Erikさん曰く、メンテナンスが面倒だからWebサイトを作ってないとのこと。


2022/10/30追記
以下のコマンドでメタデータを出力することができる
```shell-session{.line-numbers}
## JDK11の場合
$ jfr metadata recording.jfr
## JDK17の場合
$ jfr metadata 
```
参考元：
  https://stackoverflow.com/questions/68174220/how-to-identify-jfr-event-types

回答者: Kire Haglin(Oracleの Hotspot RuntimeTeamのFlight Recorderの開発者で、OpenJDKの`jdk.jfr` と`jdk.management.jfr`のcontributor)

## 詳細

OpenJDK18のソースを取得してきて調べてみた結果をまとめる

### 対象ソースの特定

ソース取得
```shell-session{.line-numbers}
# git clone https://github.com/egahlin/jdk18.git
```

profileに定義してあるイベント名 JavaThreadStatistics でソースを検索
```shell-session{.line-numbers}
# grep -r JavaThreadStatistics ./
./src/hotspot/share/jfr/periodic/jfrPeriodic.cpp:TRACE_REQUEST_FUNC(JavaThreadStatistics) {
./src/hotspot/share/jfr/periodic/jfrPeriodic.cpp:  EventJavaThreadStatistics event;
./src/hotspot/share/jfr/metadata/metadata.xml:  <Event name="JavaThreadStatistics" category="Java Application, Statistics" label="Java Thread Statistics" period="everyChunk">
./src/jdk.jfr/share/conf/jfr/profile.jfc:    <event name="jdk.JavaThreadStatistics">
./src/jdk.jfr/share/conf/jfr/default.jfc:    <event name="jdk.JavaThreadStatistics">
./test/jdk/jdk/jfr/event/runtime/TestJavaThreadStatisticsEventBean.java: * @run main/othervm jdk.jfr.event.runtime.TestJavaThreadStatisticsEventBean
./test/jdk/jdk/jfr/event/runtime/TestJavaThreadStatisticsEventBean.java:public class TestJavaThreadStatisticsEventBean {
./test/jdk/jdk/jfr/event/runtime/TestJavaThreadStatisticsEventBean.java:    private final static String EVENT_NAME = EventNames.JavaThreadStatistics;
./test/jdk/jdk/jfr/event/runtime/TestJavaThreadStatisticsEvent.java: * @run main/othervm jdk.jfr.event.runtime.TestJavaThreadStatisticsEvent
./test/jdk/jdk/jfr/event/runtime/TestJavaThreadStatisticsEvent.java:public class TestJavaThreadStatisticsEvent {
./test/jdk/jdk/jfr/event/runtime/TestJavaThreadStatisticsEvent.java:    private static final String EVENT_NAME = EventNames.JavaThreadStatistics;
./test/jdk/jdk/jfr/event/runtime/TestJavaThreadStatisticsEvent.java:            Thread thread = new Thread(TestJavaThreadStatisticsEvent::sleepRandom);
./test/lib/jdk/test/lib/jfr/EventNames.java:    public final static String JavaThreadStatistics = PREFIX + "JavaThreadStatistics";
```


### jfrPeriodic.cpp
イベントを書き込むためのマクロの定義が以下のようになっていた。
```c{.line-numbers}
#define TRACE_REQUEST_FUNC(id)    void JfrPeriodicEventSet::request##id(void)


TRACE_REQUEST_FUNC(JavaThreadStatistics) {
  EventJavaThreadStatistics event;
  event.set_activeCount(ThreadService::get_live_thread_count());
  event.set_daemonCount(ThreadService::get_daemon_thread_count());
  event.set_accumulatedCount(ThreadService::get_total_thread_count());
  event.set_peakCount(ThreadService::get_peak_thread_count());
  event.commit();
}
```

は、上記のマクロを展開すると以下に置き換えられる想定。
```c{.line-numbers}
void JfrPeriodicEventSet::requestJavaThreadStatistics(void) {
```

JfrPeriodicEventSetで検索したところ、
jfrJniMethod.cppと
GenerateJfrFiles.javaが引っかかった

```shell-session{.line-numbers}
#  grep -r JfrPeriodicEventSet .
./src/hotspot/share/jfr/jni/jfrJniMethod.cpp:  JfrPeriodicEventSet::requestEvent((JfrEventId)eventTypeId);
./src/hotspot/share/jfr/periodic/jfrPeriodic.cpp:#define TRACE_REQUEST_FUNC(id)    void JfrPeriodicEventSet::request##id(void)
./make/src/classes/build/tools/jfr/GenerateJfrFiles.java:            out.write("class JfrPeriodicEventSet : public AllStatic {");
```

#### jfrJniMethod.cpp
中身を確認したところ、以下のようになっていた。


```cpp{.line-numbers}
#define NO_TRANSITION(result_type, header) extern "C" { result_type JNICALL header {
#define NO_TRANSITION_END } }

JVM_ENTRY_NO_ENV(jboolean, jfr_emit_event(JNIEnv* env, jobject jvm, jlong eventTypeId, jlong timeStamp, jlong when))
  JfrPeriodicEventSet::requestEvent((JfrEventId)eventTypeId);
  return thread->has_pending_exception() ? JNI_FALSE : JNI_TRUE;
JVM_END
)

```
JVM_ENTRY_NO_ENVは、下記のソースで定義されていおり、JNIの定義の簡易記述と想定
// TODO: さらにマクロでかぶさっているので、別途追加調査
./src/hotspot/share/runtime/interfaceSupport.inline.hpp:#define JVM_ENTRY_NO_ENV(result_type, header

```cpp{.line-numbers}
#define JVM_ENTRY_NO_ENV(result_type, header)                        \
extern "C" {                                                         \
  result_type JNICALL header {                                       \
    JavaThread* thread = JavaThread::current();                      \
    MACOS_AARCH64_ONLY(ThreadWXEnable __wx(WXWrite, thread));        \
    ThreadInVMfromNative __tiv(thread);                              \
    debug_only(VMNativeEntryWrapper __vew;)                          \
    VM_ENTRY_BASE(result_type, header, thread)
```

#### GenerateJfrFiles.java

ヘッダファイル抜粋
```text {.line-numbers}
 Purpose of this program is twofold:

 1) Generate C++ classes to be used when writing native events for HotSpot.
    C++のソースを作る

 2) Generate metadata (label, descriptions, field layout etc.) from XML
 (metadata.xml) into a binary format (metadata.bin) that can be read quickly
 during startup by the jdk.jfr module.

   jdk.jfrのモジュールが参照するためのmetadata.binファイルを、
 　meatadata.xml,metadata.xsdから生成する

 INPUT FILES:
 -  metadata.xml  File that contains descriptions of events and types
 -  metadata.xsd  Schema that verifies that metadata.xml is legit XML

 OUTPUT FILES:

 MODE: headers

 - jfrEventIds.hpp      List of IDs so events can be identified from native
 - jfrTypes.hpp         List of IDs so types can be identified from native
 - jfrPeriodic.hpp      Dispatch mechanism so Java can emit native periodic events
 - jfrEventControl.hpp  Data structure for native event settings.
 - jfrEventClasses.hpp  C++ event classes that can write data into native buffers

 MODE: metadata

  - metadata.bin        Binary representation of the information in metadata.xml
```



### metadata.xml

引っかかった箇所の抜粋
```xml {.line-numbers}
  <Event name="JavaThreadStatistics" category="Java Application, Statistics" label="Java Thread Statistics" period="everyChunk">
    <Field type="long" name="activeCount" label="Active Threads" description="Number of live active threads including both daemon and non-daemon threads" />
    <Field type="long" name="daemonCount" label="Daemon Threads" description="Number of live daemon threads" />
    <Field type="long" name="accumulatedCount" label="Accumulated Threads" description="Number of threads created and also started since JVM start" />
    <Field type="long" name="peakCount" label="Peak Threads" description="Peak live thread count since JVM start or when peak count was reset" />
  </Event>
```

対象XMLのヘッダの抜粋
```cpp {.line-numbers}
Internal events are only to be used during development,
i.e. temporary time measurements, without the need to modify .jfc files etc.

Code that commits an internal event should NEVER be checked in.

Example usage:

#include "jfr/jfrEvents.hpp"

void foo() {
  EventDuration event;
  bar();
  baz();
  event.commit();
}

$ make images
$ java -XX:StartFlightRecording:settings=none,filename=dump.jfr ...  
  ...
$ jfr print dump.jfr

Programmatic access:
try (var rf = new RecordingFile(Path.of("dump.jfr)) {
  while (rf.hasMoreEvents()) {
    RecordedEvent e = rf.readEvent();
    System.out.println(e.getName() + " " + e.getDuration()));
  }
};
```

頭の文章
Internal events are only to be used during development,
i.e. temporary time measurements, without the need to modify .jfc files etc.

は、下記のinternal="true"が設定されているイベントのことをさしていると想定
jfcファイル はflight recorderの設定ファイル(default.jfcやprofile.jfcのことを想定)


```xml{.line-numbers}
 <Event name="Duration" category="Java Virtual Machine, Internal" label="Duration" startTime="true" thread="true" stackTrace="false" internal="true">
  </Event>

  <Event name="Instant" category="Java Virtual Machine, Internal" label="Instant" startTime="false" thread="true" stackTrace="false" internal="true">
  </Event>

  <Event name="Value" category="Java Virtual Machine, Internal" label="Value" startTime="false" thread="true" stackTrace="false" internal="true">
    <Field type="ulong" name="value" label="Value"/>
  </Event>

  <Event name="Text" category="Java Virtual Machine, Internal" label="Text" startTime="false" thread="true" stackTrace="false" internal="true">
    <Field type="string" name="text" label="Text"/>
  </Event>
  
  <Event name="ZThreadDebug" category="Java Virtual Machine, GC, Detailed" label="ZGC Thread Event" description="Temporary latency measurements used during development and debugging of ZGC" thread="true" internal="true">
    <Field type="uint" name="gcId" label="GC Identifier" relation="GcId"/>
    <Field type="string" name="name" label="Name" />
  </Event>
```

### default.jfc & profile.jfc

検索に引っかかった個所の抜粋

```xml{.line-numbers}
    <event name="jdk.JavaThreadStatistics">
      <setting name="enabled">true</setting>
      <setting name="period">1000 ms</setting>
    </event>

```
ヘッダには以下のような説明がある 

```text{.line-numbers}
Recommended way to edit .jfc files is to use the configure command of the 'jfr' tool, 
i.e. jfr configure, 
or JDK Mission Control
see Window -> Flight Recorder Template Manager
```

jfr configureコマンドを打てば利用できるイベントでもリストアップできそう
todo : jfr configure --interactiveで出力を確認する


```shell-session{.line-numbers}
jfr configure --help
jfr configure: unknown option --help

Usage:
 jfr configure [--interactive] [--verbose]
               [--input <files>] [--output <file>]
               [option=value]* [event-setting=value]*
  --interactive           Interactive mode where the configuration is
                          determined by a set of questions.

  --verbose               Displays the modified settings.

  --input <files>         A comma-separated list of .jfc files from which
                          the new configuration is based. If no file is
                          specified, the default file in the JDK is used
                          (default.jfc). If 'none' is specified, the new
                          configuration starts empty.

  --ouput <file>          The filename of the generated output file. If not
                          specified, the filename custom.jfc will be used.

  option=value            The option value to modify. For available options,
                          see listed input files below.

  event-setting=value     The event setting value to modify. Use the form:
                          <event-name>#<setting-name>=<value>
                          To add a new event setting, prefix the event name
                          with '+'.

The whitespace delimiter can be omitted for timespan values, i.e. 20ms. For
more information about the settings syntax, see Javadoc of the jdk.jfr package.

Options for default.jfc:

  gc=<off|normal|detailed|high|all>

  allocation-profiling=<off|low|medium|high|maximum>

  compiler=<off|normal|detailed|all>

  method-profiling=<off|normal|high|max>

  thread-dump=<off|once|60s|10s|1s>

  exceptions=<off|errors|all>

  memory-leaks=<off|types|stack-traces|gc-roots>

  locking-threshold=<timespan>

  file-threshold=<timespan>

  socket-threshold=<timespan>

  class-loading=<true|false>


To run interactive configuration wizard:

  jfr configure --interactive

Example usage:

  jfr configure gc=high method-profiling=high --output high.jfc

  jfr configure jdk.JavaMonitorEnter#threshold=1ms --output locks.jfc

  jfr configure +HelloWorld#enabled=true +HelloWorld#stackTrace=true

  jfr configure --input default.jfc,third-party.jfc --output unified.jfc

  jfr configure --input none +Hello#enabled=true --output minimal.jfc

```



## TestJavaThreadStatisticsEventBean.java

MXBEANから、イベント情報が取れるかをテストするためのコード
１つでも取れてたらOKとして、取得情報の値の妥当性もチェックする


抜粋
```java{.line-numbers}
package jdk.jfr.event.runtime;

import static jdk.test.lib.Asserts.assertTrue;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.time.Duration;
import java.util.List;

import jdk.jfr.Recording;
import jdk.jfr.consumer.RecordedEvent;
import jdk.test.lib.jfr.EventNames;
import jdk.test.lib.jfr.Events;

/**
 * @test
 * @key jfr
 * @requires vm.hasJFR
 * @library /test/lib
 *
 * @run main/othervm jdk.jfr.event.runtime.TestJavaThreadStatisticsEventBean
 */
public class TestJavaThreadStatisticsEventBean {
    private final static String EVENT_NAME = EventNames.JavaThreadStatistics;

    // Compare JFR thread counts to ThreadMXBean counts
    public static void main(String[] args) throws Throwable {
        long mxDaemonCount = -1;
        long mxActiveCount = -1;

        // Loop until we are sure no threads were started during the recording.
        // 記録中にスレッドが起動しなくなるまで、レコードをとり続ける。
        Recording recording = null;
        ThreadMXBean mxBean = ManagementFactory.getThreadMXBean();
        long totalCountBefore = -1;
        while (totalCountBefore != mxBean.getTotalStartedThreadCount()) {
            recording = new Recording();
            recording.enable(EVENT_NAME).withThreshold(Duration.ofMillis(10));
            totalCountBefore = mxBean.getTotalStartedThreadCount();
            recording.start();
            mxDaemonCount = mxBean.getDaemonThreadCount();
            mxActiveCount = mxBean.getThreadCount();
            recording.stop();
            final String msg = "testCountByMXBean: threadsBefore=%d, threadsAfter=%d%n";
            System.out.format(msg, totalCountBefore, mxBean.getTotalStartedThreadCount());
        }

        // 記録が１つでもとれていれば、テストはOKとする
        // つまり、１つも取れていなかったら、NGとする。

        List<RecordedEvent> events= Events.fromRecording(recording);
        boolean isAnyFound = false;
        for (RecordedEvent event : events) {
            System.out.println("Event:" + event);
            isAnyFound = true;
            // 上記のループ中にmxBeanから取った値と、eventのフィールド値を比較して一致するかをチェック
            Events.assertField(event, "daemonCount").equal(mxDaemonCount);
            Events.assertField(event, "activeCount").equal(mxActiveCount);
            // 上記のループ中にmxBeanから取った値と、eventのフィールド値を比較してmxBeanからとった値以上であることを確認する。
            Events.assertField(event, "accumulatedCount").atLeast(mxActiveCount);
            Events.assertField(event, "peakCount").atLeast(mxActiveCount);
        }
        assertTrue(isAnyFound, "Correct event not found");
    }

}

```

### TestJavaThreadStatisticsEvent.java

スレッドを生成して、関連する情報が取れるかを検証するテストコード


```java{.line-numbers}

package jdk.jfr.event.runtime;

import static jdk.test.lib.Asserts.assertTrue;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import jdk.jfr.Recording;
import jdk.jfr.consumer.RecordedEvent;
import jdk.test.lib.jfr.EventNames;
import jdk.test.lib.jfr.Events;

/**
 * @test
 * @key jfr
 * @requires vm.hasJFR
 * @library /test/lib
 * @run main/othervm jdk.jfr.event.runtime.TestJavaThreadStatisticsEvent
 */
public class TestJavaThreadStatisticsEvent {
    private static final String EVENT_NAME = EventNames.JavaThreadStatistics;

    private static final int THREAD_COUNT = 10;
    private static final Random RAND = new Random(4711);

    // verify thread count: 0 <= daemon <= active <= peak <= accumulated.
    // ランダム時間スリープするスレッドを10個作って、終了を待って記録を終了する
    public static void main(String[] args) throws Throwable {
        Recording recording = new Recording();
        recording.enable(EVENT_NAME).withThreshold(Duration.ofMillis(10));
        recording.start();

        
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < THREAD_COUNT; i++) {
            Thread thread = new Thread(TestJavaThreadStatisticsEvent::sleepRandom);
            thread.start();
            threads.add(thread);
        }
        for (Thread thread : threads) {
            thread.join();
        }

        recording.stop();
        List<RecordedEvent> events = Events.fromRecording(recording);
        boolean isAnyFound = false;
        for (RecordedEvent event : events) {
            System.out.println("Event:" + event);
            isAnyFound = true;
            // verify thread count: 0 <= daemon <= active <= peak <= accumulated.
            long daemonCount = Events.assertField(event, "daemonCount").atLeast(0L).getValue();
            long activeCount = Events.assertField(event, "activeCount").atLeast(daemonCount).getValue();
            long peakCount = Events.assertField(event, "peakCount").atLeast(activeCount).atLeast(1L).getValue();
            Events.assertField(event, "accumulatedCount").atLeast(peakCount).getValue();
        }
        assertTrue(isAnyFound, "Correct event not found");
    }

    static void sleepRandom() {
        try {
            Thread.sleep(RAND.nextInt(10));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

```

### /test/lib/jdk/test/lib/jfr/EventNames.java
テストコード用のイベント名定義

```java{.line-numbers}

package jdk.test.lib.jfr;

import jdk.jfr.EventType;

/**
 * Contains id for events that are shipped with the JDK.
 *
 */
public class EventNames {

    public final static String PREFIX = "jdk.";
    private static final String GC_CATEGORY = "GC";

    // JVM Configuration
    public final static String JVMInformation = PREFIX + "JVMInformation";
    public final static String InitialSystemProperty = PREFIX + "InitialSystemProperty";
    public final static String IntFlag = PREFIX + "IntFlag";

    //省略

    // つまり jdk.JavaThreadStatistics
    public final static String JavaThreadStatistics = PREFIX + "JavaThreadStatistics";
    
```




