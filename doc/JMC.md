## JMC公式サイト記載概要
<https://www.oracle.com/java/technologies/jdk-mission-control.html>

### Overview

- JMC 8 Release Notes
  - JMCのリリースノート
- Eclipse JMC Extension Plugins
  - JMCで使っているPluginに関する説明っぽい。今回の調査には関係なし。
- Oracle JDK Mission Control and Java Flight Recorder(White Paper)
  - データ収集の仕組みの概要で、JMCを使ってみると良いという説明というレベル
- Oracle Java SE Subscription (Data Sheet)
  - 関係なし
- Maximizing Value. Protecting Investments. Java SE Advanced for ISVs (Webcast)
  - 関係なし（Java SE Advanced の宣伝）

### Getting Started

- Java Flight Recorder Runtime Guide
  - リンク先で見つけず

### FAQ

- JDK Mission Control Frequently Asked Questions
  - JMCのFAQ
    - 過去のバージョンでとれない情報があるけどなんで？
      - バグです。
      - バージョンは？とかといったQAがある
    - 気になったのは
      - Q: メソッドのサンプルが予想よりも少ないのはなぜですか?
        A3: Flight Recorder は、ネイティブ コードを実行しているスレッドをサンプリングしません。
        というQA
    - Graalでネイティブ化した場合、元々Javaで実装されていたところは取れるのかは確認したいと思った
- System Requirements and Supported Platforms for JDK Mission Control
  - JMCを使うためのOSやCPU Architectureの表
  - 今回は関係なし。
