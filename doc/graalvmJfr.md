# GraalVM-Jfr

## まとめ
1. JFRのパッケージとソースの対応
   package com.oracle.svm.core.jfr
   substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jfr/

2. Issueの調査結果（古い順）
jdk.SocketReadはまだ対応されていない想定に見える&masterのソースにも対象コードが見つからなかった。
   1. SocketReadの対応したIssue[Add support for Socket JFR events (#5497)](https://github.com/oracle/graal/pull/5497)
     このIssueは最終的にmergeされずにクローズされていた
   2. NativeImageのJFRサポートをしたIssue[JFR Support in Native Image](https://github.com/oracle/graal/issues/5410)
      こちらにはNativeImageでサポート済みのJFRイベントのリストの管理表
      この表ではjdk.SocketReadは対象になっていないことになっている。
   3. jdk.SocketReadをバイトコードでInstrumentするしたケースもNativeImage実装で動くようにしようとしているIssuse[Use JFR at build time if JFR is included in the native image](https://github.com/oracle/graal/pull/5556)
      まだマージされていない

## やったこと

### graalvmのgithubをjfrで検索

package com.oracle.svm.core.jfr
substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jfr/logging/JfrLogConfiguration.java

ということから、パッケージ構造を確認
substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jfr/

### SocketReadで検索

- ソース: 対象なし
- Issue : 以下の4つが引っかかった
  - Use JFR at build time if JFR is included in the native image (#5556) マージされていない[5556調査結果まとめ](doc/../Issue5556確認結果.md)
  - Add support for Socket JFR events (#5497) マージされていない[5497調査結果まとめ](doc/../Issue5497確認結果.md)
  - JFR Support in Native Image (#5410) 機能開発管理用のドキュメント
  - NoClassDefFoundError: Ljava/net/SocketTimeoutException (#1710)


### discussions

https://github.com/oracle/graal/discussions?discussions_q=jfr

| No  |                                                     タイトル                                                      | チェック |
| --- | ----------------------------------------------------------------------------------------------------------------- | -------- |
| 1   | [Native Image Committer Community Meeting 2022-03-10 #4385](https://github.com/oracle/graal/discussions/4385)     | 済       |
| 2   | [Native Image Committer Community Meeting 2022-04-07 #4472](https://github.com/oracle/graal/discussions/4472)     | 済       |
| 3   | [Native Image Committer Community Meeting 2021-12-16 #4130](https://github.com/oracle/graal/discussions/4130)     | 済       |
| 4   | [Native Image Committer Community Meeting 2021-09-20 #3818](https://github.com/oracle/graal/discussions/3818)     | 済       |
| 5   | [Native Image Committer Community Meeting 2021-11-18 #4031](https://github.com/oracle/graal/discussions/4031)     | 済       |
| 6   | [Native Image Committer Community Meeting 2022-02-10 #4310](https://github.com/oracle/graal/discussions/4310)     | 済       |
| 7   | [Native Image Committer Community Meeting 2022-05-05 #4541](https://github.com/oracle/graal/discussions/4541)     | 済       |
| 8   | [Native Image Committer Community Meeting 2022-06-30 #4687](https://github.com/oracle/graal/discussions/4687)     | 済       |
| 9   | [Native Image Committer and Community Meeting 2022-09-22 #5046](https://github.com/oracle/graal/discussions/5046) | 済       |
| 10  | [Native Image Committer and Community Meeting 2022-08-25 #4845](https://github.com/oracle/graal/discussions/4845) | 対応中   |
| 11  | [Native Image Committer Community Meeting 2021-10-20 #3932](https://github.com/oracle/graal/discussions/3932)     |          |

まとめ
|  マージ日  |                                                                                  タイトル                                                                                  |                                                      直訳                                                      |                                                                      備考                                                                      |
| ---------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| 2022/03/30 | [Implementation of ExecuteVMOperationEvent #4368](https://github.com/oracle/graal/pull/4368)                                                                               | ExecuteVMOperation イベントの実装                                                                              |                                                                                                                                                |
| 2022/02/22 | [Implementation of Safepoint events #4330](https://github.com/oracle/graal/pull/4330)                                                                                      | Safepoint イベントの実装                                                                                       |                                                                                                                                                |
| 2022/02/18 | [Implementation of GarbageCollection event #4312](https://github.com/oracle/graal/pull/4312)                                                                               | GarbageCollection イベントの実装                                                                               |                                                                                                                                                |
|            | [ Working on stack sampling events, this is the first PR with some infrastructure for it](https://github.com/oracle/graal/commit/3fc931beb2762850cbb64f71c9ec63b19b4c7eca) | スタック サンプリング イベントに取り組んでいます。これは、そのためのインフラストラクチャを備えた最初の PR です |                                                                                                                                                |
| 2022/04/16 | [JFR: emit a large event if fails to write as an small event  #4469](https://github.com/oracle/graal/pull/4469)                                                            | 小さなイベントとして書き込みに失敗した場合、大きなイベントを発行する                                           |                                                                                                                                                |
| 2022/03/30 | [[GR-37311] Implement JFR ExecuteVMOperation event. #4426](https://github.com/oracle/graal/pull/4426)                                                                      | JFR ExecuteVMOperation イベントを実装します                                                                    |                                                                                                                                                |
| 2022/01/18 | [Adding support for JFR ExecutionSample event. #4005](https://github.com/oracle/graal/pull/4005)                                                                           | ExecutionSample イベントの追加                                                                                 |                                                                                                                                                |
| 2021/12/18 | [ [GR-35806] Support Flight Recorder at image build time. #4125](https://github.com/oracle/graal/pull/4125)                                                                | イメージのビルド時に Flight Recorder をサポート                                                                |                                                                                                                                                |
| -          | [Refactor Jfr into core and hosted #3746 Merging blocked by JDK 8 removal](https://github.com/oracle/graal/pull/)                                                          | Jfr をコアとホストにリファクタリング #3746 JDK 8 の削除によりマージがブロックされる                            | 3770に置換                                                                                                                                     |
| -          | [ Refactor and add framework for native jfr event support#3770](https://github.com/oracle/graal/pull/3770)                                                                 | ネイティブ jfr イベント サポートのフレームワークをリファクタリングして追加                                     | JFR の実装 (およびその依存関係) を別のプロジェクトに含める。ほとんどすべての VM 内部イベントは、JfrNativeEventWriter  を使用する必要があるため |
| -          | [Add JFR support for threads.](https://github.com/oracle/graal/commit/99b234fcfdb6acf0659315360f6c86c463630f37)                                                            | スレッドの JFRサポート追加                                                                                     | マージされず。関連する改修は`Improving JFR support`にマージされていた                                                                          |
| 2022/11/08 | [Improving JFR support.](https://github.com/oracle/graal/commit/c466c8129964068a14033a6bb73d93be658edb76)                                                                  | JFR サポートの改善                                                                                             | Issue 3720(Thread and ThreadGroup の対応)の取り込み                                                                                            |
| 2022/11/17 | [Don't use generics for certain uninterruptible methods.](https://github.com/oracle/graal/commit/007b77dfedacd0b2eb1e0564889287941665122d)                                 | 特定の割り込み不可能なメソッドにジェネリックを使用しない                                                       | 上記の改善っぽい                                                                                                                               |
| 2022/02/03 | [Implementation of GC phase pause events #4272](https://github.com/oracle/graal/pull/4272)                                                                                 | GC フェーズ一時停止イベントの実装                                                                              | 同日に修正あり                                                                                                                                 |
| 2022/01/18 | [Adding support for JFR ExecutionSample event. #4005](https://github.com/oracle/graal/pull/4005)                                                                           | ExecutionSample イベントの追加                                                                                 | 2021/12/26のディスカッションでも対象になっていた                                                                                               |
| 2022/02/03 | [Improve JFR testing infrastructure](https://github.com/oracle/graal/commit/04668a0ee9994be0abce62313bb0f6e8adf5a048)                                                      | JFR テスト インフラストラクチャの改善                                                                          |                                                                                                                                                |
| 2022/01/25 | [[GR-34389] Moved all JFR-related files to com.oracle.svm.core.](https://github.com/oracle/graal/commit/8676f60f2619876a02ef61ec9d4e244fd743f8dc)                          | すべての JFR 関連ファイルを com.oracle.svm.core に移動                                                         | com.oracle.svm.core.jfr,com.oracle.svm.hosted.jfr                                                                                              |
| 2022/05/21 | [First step for sampling based profiling: [GR-18215] SIGPROF signal handling #4536](https://github.com/oracle/graal/pull/4536)                                             | SIGPROF シグナル処理                                                                                           |                                                                                                                                                |
| 2022/06/29 | [Add support for JFR monitor enter events #4651](https://github.com/oracle/graal/pull/4651)                                                                                | JFRモニター入力イベントのサポートを追加                                                                        |                                                                                                                                                |
| 2022/09/30 | [[GR-18215] [GR-37463] JFR StackTrace and JFR Method repositories. Sampling-based profiler and JFR integration. #5039](https://github.com/oracle/graal/pull/5039)          | JFR StackTrace および JFR Method リポジトリ。サンプリングベースのプロファイラーと JFR の統合                   |                                                                                                                                                |
| 2022/08/22 | [[GR-40264] Introduce --enable-monitoring option. #4823 ](https://github.com/oracle/graal/discussions/4845)                                                                | --enable-monitoring オプションを導入                                                                           |                                                                                                                                                |
| 2022/08/09 | [[GR-39475] [GR-40095] Initial jvmstat support in GraalVM Native Image. #4803](https://github.com/oracle/graal/pull/4803)                                                  | GraalVM Native Imageでの初期の jvmstat サポート                                                                |                                                                                                                                                |
| 2022/06/26 | [[GR-40033] Implement JFR MonitorWaitEvent.](https://github.com/oracle/graal/commit/26cea859b63ab611afef487275e433dda651963a)                                              | JFR MonitorWaitEvent を実装                                                                                    |                                                                                                                                                |
| 2022/06/07 | [Add support for JFR event ThreadSleep.](https://github.com/oracle/graal/commit/665718f0ec0e62946195711f09e8f810ad5a71d8)                                                  | JFR イベント ThreadSleep のサポート追加                                                                        |                                                                                                                                                |
| 2022/06/30 | [Add support for JFR monitor enter events #4651](https://github.com/oracle/graal/pull/4651)                                                                                | Monitor Enterイベントのサポート追加 events                                                                     |                                                                                                                                                |

- JFR関連のソースのパス
  -  [com.oracle.svm.core.jfr](sources.md#comoraclesvmcorejfr)
  -  [com.oracle.svm.hosted.jfr](sources.md#comoraclesvmhostedjfr)




### 変更履歴
https://github.com/oracle/graal/blob/b6e1c8d0844c475c97ad7ef52796b192b59ef2fc/substratevm/CHANGELOG.md

Version 22.1.0
(GR-26814) (GR-37018) (GR-37038) (GR-37311) Red Hat added support for the following JFR events: SafepointBegin, SafepointEnd, GarbageCollection, GCPhasePause, GCPhasePauseLevel*, and ExecuteVMOperation. All GC-related JFR events are currently limited to the serial GC.

Version 22.3.0
(GR-39390) (GR-39649) (GR-40033) Red Hat added support for the JFR events JavaMonitorEnter, JavaMonitorWait, and ThreadSleep.
(GR-40264) Add --enable-monitoring=<all,heapdump,jfr,jvmstat> option to enable fine-grained control over monitoring features enabled in native executables. -H:±AllowVMInspection is now deprecated and will be removed in a future release.




#### Native Image Committer Community Meeting 2022-03-10 #4385

|  マージ日  |                                           タイトル                                           |               直訳                |
| ---------- | -------------------------------------------------------------------------------------------- | --------------------------------- |
| 2022/03/30 | [Implementation of ExecuteVMOperationEvent #4368](https://github.com/oracle/graal/pull/4368) | ExecuteVMOperation イベントの実装 |
| 2022/02/22 | [Implementation of Safepoint events #4330](https://github.com/oracle/graal/pull/4330)        | Safepoint イベントの実装          |
| 2022/02/18 | [Implementation of GarbageCollection event #4312](https://github.com/oracle/graal/pull/4312) | GarbageCollection イベントの実装  |

#### Native Image Committer Community Meeting 2022-04-07 #4472

|  マージ日  |                                                                                  タイトル                                                                                  |                                                      直訳                                                      |
| ---------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- |
|            | [ Working on stack sampling events, this is the first PR with some infrastructure for it](https://github.com/oracle/graal/commit/3fc931beb2762850cbb64f71c9ec63b19b4c7eca) | スタック サンプリング イベントに取り組んでいます。これは、そのためのインフラストラクチャを備えた最初の PR です |
| 2022/04/16 | [JFR: emit a large event if fails to write as an small event  #4469](https://github.com/oracle/graal/pull/4469)                                                            | 小さなイベントとして書き込みに失敗した場合、大きなイベントを発行する                                           |
| 2022/03/30 | [[GR-37311] Implement JFR ExecuteVMOperation event. #4426](https://github.com/oracle/graal/pull/4426)                                                                      | JFR ExecuteVMOperation イベントを実装します                                                                    |

#### Native Image Committer Community Meeting 2021-12-16 #4130
|  マージ日  |                                                     タイトル                                                      |                                        直訳                                         |                                                                      備考                                                                      |
| ---------- | ----------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| 2022/01/18 | [Adding support for JFR ExecutionSample event. #4005](https://github.com/oracle/graal/pull/4005)                  | ExecutionSample イベントの追加                                                      |                                                                                                                                                |
| 2021/12/18 | [ [GR-35806] Support Flight Recorder at image build time. #4125](https://github.com/oracle/graal/pull/4125)       | イメージのビルド時に Flight Recorder をサポート                                     |                                                                                                                                                |
| -          | [Refactor Jfr into core and hosted #3746 Merging blocked by JDK 8 removal](https://github.com/oracle/graal/pull/) | Jfr をコアとホストにリファクタリング #3746 JDK 8 の削除によりマージがブロックされる | 3770に置換                                                                                                                                     |
| -          | [ Refactor and add framework for native jfr event support#3770](https://github.com/oracle/graal/pull/3770)        | ネイティブ jfr イベント サポートのフレームワークをリファクタリングして追加          | JFR の実装 (およびその依存関係) を別のプロジェクトに含める。ほとんどすべての VM 内部イベントは、JfrNativeEventWriter  を使用する必要があるため |

※Issue内でコメントされていたPR(3720をマージしてから、3770をマージする)
[Improving JFR support](https://github.com/oracle/graal/pull/3720)`JFR サポートの改善`
Thread and ThreadGroup の対応

#### Native Image Committer Community Meeting 2021-09-20 #3818
|  マージ日   |                                                    タイトル                                                     |                               直訳                               |
| ----------- | --------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- |
| 2021/09ごろ | [Update JFR Native Image Support for compatibility with JDK 17 #3726(https://github.com/oracle/graal/pull/3726) | JDK 17 との互換性のための JFR ネイティブ イメージ サポートの更新 |

#### Native Image Committer Community Meeting 2021-11-18 #4031

|  マージ日  |                                                                  タイトル                                                                  |                           直訳                           |                                 備考                                  |
| ---------- | ------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------- | --------------------------------------------------------------------- |
| -          | [Add JFR support for threads.](https://github.com/oracle/graal/commit/99b234fcfdb6acf0659315360f6c86c463630f37)                            | スレッドの JFRサポート追加                               | マージされず。関連する改修は`Improving JFR support`にマージされていた |
| 2022/11/08 | [Improving JFR support.](https://github.com/oracle/graal/commit/c466c8129964068a14033a6bb73d93be658edb76)                                  | JFR サポートの改善                                       | Issue 3720(Thread and ThreadGroup の対応)の取り込み                   |
| 2022/11/17 | [Don't use generics for certain uninterruptible methods.](https://github.com/oracle/graal/commit/007b77dfedacd0b2eb1e0564889287941665122d) | 特定の割り込み不可能なメソッドにジェネリックを使用しない | 上記の改善っぽい                                                      |

#### [Native Image Committer Community Meeting 2022-02-10 #4310](https://github.com/oracle/graal/discussions/4310)

|  マージ日  |                                                                     タイトル                                                                      |                          直訳                          |                       備考                        |
| ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------- |
| 2022/02/03 | [Implementation of GC phase pause events #4272](https://github.com/oracle/graal/pull/4272)                                                        | GC フェーズ一時停止イベントの実装                      | 同日に修正あり                                    |
| 2022/01/18 | [Adding support for JFR ExecutionSample event. #4005](https://github.com/oracle/graal/pull/4005)                                                  | ExecutionSample イベントの追加                         | 2021/12/26のディスカッションでも対象になっていた  |
| 2022/02/03 | [Improve JFR testing infrastructure](https://github.com/oracle/graal/commit/04668a0ee9994be0abce62313bb0f6e8adf5a048)                             | JFR テスト インフラストラクチャの改善                  |                                                   |
| 2022/01/25 | [[GR-34389] Moved all JFR-related files to com.oracle.svm.core.](https://github.com/oracle/graal/commit/8676f60f2619876a02ef61ec9d4e244fd743f8dc) | すべての JFR 関連ファイルを com.oracle.svm.core に移動 | com.oracle.svm.core.jfr,com.oracle.svm.hosted.jfr |

#####  [Native Image Committer Community Meeting 2022-05-05 #4541](https://github.com/oracle/graal/discussions/4541)


|  マージ日  |                                                            タイトル                                                            |         直訳         | 備考 |
| ---------- | ------------------------------------------------------------------------------------------------------------------------------ | -------------------- | ---- |
| 2022/05/21 | [First step for sampling based profiling: [GR-18215] SIGPROF signal handling #4536](https://github.com/oracle/graal/pull/4536) | SIGPROF シグナル処理 |      |

#### [Native Image Committer Community Meeting 2022-06-30 #4687](https://github.com/oracle/graal/discussions/4687)


|  マージ日  |                                          タイトル                                           |                  直訳                   | 備考 |
| ---------- | ------------------------------------------------------------------------------------------- | --------------------------------------- | ---- |
| 2022/06/29 | [Add support for JFR monitor enter events #4651](https://github.com/oracle/graal/pull/4651) | JFRモニター入力イベントのサポートを追加 |      |

heapdumpの対応関係
|  マージ日  |                                                                        タイトル                                                                        | 直訳 | 備考 |
| ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | ---- | ---- |
| 2022/01/08 | [[GR-38965] Support heap dumps in GraalVM CE Native Image. #4622](https://github.com/oracle/graal/pull/4622)                                           |      |      |
| 2022/01/02 | [[GR-38951] Introduce -XX:+DumpHeapAndExit option. #4612](https://github.com/oracle/graal/pull/4612)                                                   |      |      |
| 2022/12    | [[GR-34179] Upgrade CE debug info feature to provide information about Java types for Windows/PECOFF #3732](https://github.com/oracle/graal/pull/3732) |      |      |


#### [Native Image Committer and Community Meeting 2022-09-22 #5046](https://github.com/oracle/graal/discussions/5046)
|  マージ日  |                                                                             タイトル                                                                              |                                             直訳                                             | 備考 |
| ---------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ---- |
| 2022/09/30 | [[GR-18215] [GR-37463] JFR StackTrace and JFR Method repositories. Sampling-based profiler and JFR integration. #5039](https://github.com/oracle/graal/pull/5039) | JFR StackTrace および JFR Method リポジトリ。サンプリングベースのプロファイラーと JFR の統合 |      |

JFR StackTrace リポジトリの実装 (エントリ ポイント JfrStackTraceRepository)。
JFR メソッド リポジトリの実装 (エントリ ポイントJfrMethodRepository)。
サンプリングベースのプロファイラーと JFR の統合 (エントリーポイントSubstrateSigprofHandler)。
VMSemaphore（エントリポイント）の実装VMSemaphore。
中断不可能なフレーム ルックアップ (エントリ ポイントCodeInfoAccess)。
memcmp関数 (エントリ ポイントLibC)。
マイナー コードのリファクタリング (JfrVisitedおよびを参照JfrVisitedHashtable)。

|  マージ日  |                                  タイトル                                  |      直訳      | 備考 |
| ---------- | -------------------------------------------------------------------------- | -------------- | ---- |
| 2022/09/09 | [[GR-40874] Perf support #4811](https://github.com/oracle/graal/pull/4811) | Pefrのサポート |      |

#### [Native Image Committer and Community Meeting 2022-08-25 #4845](https://github.com/oracle/graal/discussions/4845)

|  マージ日  |                                                           タイトル                                                            |                      直訳                       | 備考 |
| ---------- | ----------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------- | ---- |
| 2022/08/22 | [[GR-40264] Introduce --enable-monitoring option. #4823 ](https://github.com/oracle/graal/discussions/4845)                   | --enable-monitoring オプションを導入            |      |
| 2022/08/09 | [[GR-39475] [GR-40095] Initial jvmstat support in GraalVM Native Image. #4803](https://github.com/oracle/graal/pull/4803)     | GraalVM Native Imageでの初期の jvmstat サポート |      |
| 2022/06/26 | [[GR-40033] Implement JFR MonitorWaitEvent.](https://github.com/oracle/graal/commit/26cea859b63ab611afef487275e433dda651963a) | JFR MonitorWaitEvent を実装                     |      |
| 2022/06/07 | [Add support for JFR event ThreadSleep.](https://github.com/oracle/graal/commit/665718f0ec0e62946195711f09e8f810ad5a71d8)     | JFR イベント ThreadSleep のサポート追加         |      |
| 2022/06/30 | [Add support for JFR monitor enter events #4651](https://github.com/oracle/graal/pull/4651)                                   | Monitor Enterイベントのサポート追加 events      |      |

#### [変更履歴](https://github.com/oracle/graal/blob/b6e1c8d0844c475c97ad7ef52796b192b59ef2fc/substratevm/CHANGELOG.md)


Version 22.1.0
(GR-26814) (GR-37018) (GR-37038) (GR-37311) Red Hat added support for the following JFR events: SafepointBegin, SafepointEnd, GarbageCollection, GCPhasePause, GCPhasePauseLevel*, and ExecuteVMOperation. All GC-related JFR events are currently limited to the serial GC.

Version 22.3.0
(GR-39390) (GR-39649) (GR-40033) Red Hat added support for the JFR events JavaMonitorEnter, JavaMonitorWait, and ThreadSleep.
(GR-40264) Add --enable-monitoring=<all,heapdump,jfr,jvmstat> option to enable fine-grained control over monitoring features enabled in native executables. -H:±AllowVMInspection is now deprecated and will be removed in a future release.



| マージ日 |                                                 タイトル                                                  | 直訳 | 備考 |
| -------- | --------------------------------------------------------------------------------------------------------- | ---- | ---- |
|          | []()                                                                                                      |      |      |