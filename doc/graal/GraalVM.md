# 参考サイト
GraalVM 公式
https://www.graalvm.org/downloads/

リファレンスマニュアル
https://www.graalvm.org/22.2/reference-manual/




Howtoガイド
https://www.graalvm.org/22.2/reference-manual/native-image/guides/

Nativeイメージの基本
https://www.graalvm.org/22.2/reference-manual/native-image/basics/



ね




概要
https://www.graalvm.org/22.2/docs/introduction/


インストール手順
https://www.graalvm.org/22.2/docs/getting-started/

todo : ネイティブイメージ関係のドキュメント構成を確認する
https://www.graalvm.org/22.2/reference-manual/native-image/

todo : JDK Flight Recorder (JFR) with Native Image
https://www.graalvm.org/22.0/reference-manual/native-image/JFR/


todo :  ネイティブイメージ生成の設定確認（必要な時）
https://www.graalvm.org/22.2/reference-manual/native-image/overview/BuildConfiguration/

todo :  ネイティブイメージ生成のビルドツール(Maven/Gradle設定)
https://graalvm.github.io/native-build-tools/latest/index.html

todo : Junitの使い方
https://medium.com/graalvm/gradle-and-maven-plugins-for-native-image-with-initial-junit-testing-support-dde00a8caf0b

todo : VSCodeを使ったデバッグのための拡張機能セットアップ
https://www.graalvm.org/22.2/tools/vscode/graalvm-extension/

ブレークポイントを設定したりできる





## インストール
### GraalVM本体
Installation on Linux Platforms を参照

```shell-session{.line-numbers}
#wget https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-22.2.0/graalvm-ce-java17-linux-amd64-22.2.0.tar.gz
#tar  -xvxf graalvm-ce-java17-linux-amd64-22.2.0.tar.gz
```

以下の設定を入れた環境変数用のファイルを作成
※都度構築時に
環境変数設定
```shell-session{.line-numbers}
export PATH=/path/to/<graalvm>/bin:$PATH
export JAVA_HOME=/path/to/<graalvm>
```


インストール確認でjava -versionをたたいて
OpenJDK Runtime Environment GraalVM CE 22.2.0 
と出ているのを確認
```shell-session{.line-numbers}
$ java -version
openjdk version "17.0.4" 2022-07-19
OpenJDK Runtime Environment GraalVM CE 22.2.0 (build 17.0.4+8-jvmci-22.2-b06)
OpenJDK 64-Bit Server VM GraalVM CE 22.2.0 (build 17.0.4+8-jvmci-22.2-b06, mixed mode, sharing)
```

### NativeImageのインストール
https://www.graalvm.org/22.2/reference-manual/native-image/

zlib
gcc
libstdc++-static
がインストールされていることが前提


```shell-session{.line-numbers}
$gu install native-image
Downloading: Component catalog from www.graalvm.org
Processing Component: Native Image
Downloading: Component native-image: Native Image from github.com
Installing new component: Native Image (org.graalvm.native-image, version 22.2.0)
```

パッケージマネージャーに合わせて以下を実行（Ubuntuでやったのでaptを使った）
```shell-session{.line-numbers}

$ sudo yum install gcc glibc-devel zlib-devel
## または
$ sudo apt-get install build-essential libz-dev zlib1g-dev
## または
$ sudo dnf install gcc glibc-devel zlib-devel libstdc++-static
```

Todo: ネイティブイメージ作成

```shell-session{.line-numbers}
 $javac HelloWorld.java
 $native-image HelloWorld
 $ ./helloWorld
```

