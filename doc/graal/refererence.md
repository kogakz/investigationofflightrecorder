# リファレンスマニュアル流し読み

## GettingStart
https://www.graalvm.org/22.2/reference-manual/native-image/
インストールの仕方や、ネイティブイメージの作成例

## Howto
https://www.graalvm.org/22.2/reference-manual/native-image/guides/

xxxしたいときにはって感じで、やりたいことごとの参照先が書いてある
- 環境変数へのアクセス
- ロギングをネイティブ実行可能ファイルに追加する
- JFR を使用したネイティブ実行可能ファイルのビルドと実行
- Java モジュールをネイティブ実行可能ファイルにビルドする
- Micronaut アプリケーションをネイティブ実行可能ファイルにビルドする
- ネイティブ共有ライブラリを構築する
- Polyglot ネイティブ実行可能ファイルをビルドする (Java および JavaScript)
- Spring Boot アプリケーションをネイティブ実行可能ファイルにビルドする
- 静的またはほぼ静的なネイティブ実行可能ファイルをビルドする
- リフレクションを使用してネイティブ実行可能ファイルをビルドする
- ネイティブ実行可能ファイルをコンテナー化し、Docker コンテナーで実行する
- ネイティブ実行可能ファイルからヒープ ダンプを作成する
- GDB を使用してネイティブ実行可能ファイルをデバッグする
- リソースをネイティブ実行可能ファイルに含める
- プロファイルに基づく最適化を使用してネイティブ実行可能ファイルを最適化する
- GraalVM ダッシュボードを使用してネイティブ実行可能ファイルのサイズを最適化する
- Gradle を使用して Java アプリケーションからネイティブ実行可能ファイルをビルドする
- Maven を使用して Java アプリケーションからネイティブ実行可能ファイルをビルドする
- ネイティブ イメージ Gradle プラグインで共有到達可能性メタデータを使用する
- ネイティブ実行可能ファイルでシステム プロパティを使用する

## Native Image Basics
https://www.graalvm.org/22.2/reference-manual/native-image/basics/

ネイティブイメージのに関する基本的な用語や考え方が書いてあるっぽい
ビルド時間と実行時間

## Native Image Build Overview
https://www.graalvm.org/22.2/reference-manual/native-image/overview/Build-Overview/

native-imageコマンドの概要が説明されている

## Reachability Metadata
https://www.graalvm.org/22.2/reference-manual/native-image/metadata/

ネイティブイメージ生成をする際にどういったクラスを取り込む必要があるかを補助するための情報(Metadata)について説明してある

目次構成は以下（まだ読んでないが基礎情報＋利用例？）

- Computing Metadata in Code
- Specifying Metadata with JSON
- Metadata Types
- Reflection
- Java Native Interface
- Resources and Resource Bundles
- Dynamic Proxy
- Serialization
- Predefined Classes

## Optimizations and Performance
https://www.graalvm.org/22.2/reference-manual/native-image/optimizations-and-performance/

ネイティブイメージをさらに最適化するための方法へのリンク先
- profileに基づく最適化
- GCの指定による最適化
- クラスの初期化方法の最適化（ビルド中に初期させるとアプリケーション起動が早くなるらしい）


## Debugging and Diagnostics
https://www.graalvm.org/22.2/reference-manual/native-image/debugging-and-diagnostics/

### Debug Info Feature 
https://www.graalvm.org/22.2/reference-manual/native-image/debugging-and-diagnostics/DebugInfo/

```text{.line-numbers}
-g,-sourceオプション
```
gccのオプションぽいものをつかってデバッグ情報の埋めるという説明

### Debugging Native Executables
 https://www.graalvm.org/22.2/reference-manual/native-image/debugging-and-diagnostics/Debugging/
VSCodeのExtensionの紹介

### Create a Heap Dump from a Native Executable
https://www.graalvm.org/22.2/reference-manual/native-image/guides/create-heap-dump/
Javaのプロセスと同じようにヒープダンプを出力させる方法

- 起動時のヒープを出力する方法
  - 自前の実装は不要で、起動オプションを指定するだけで良い。
- シグナル受信時にHeapDumpを出力する方法
  - シグナルハンドラの実装が必要
- 任意の個所でヒープダンプを出力する方法
  - 自前で実装する必要がある

### JDK Flight Recorder (JFR) with Native Image
  https://www.graalvm.org/22.2/reference-manual/native-image/debugging-and-diagnostics/JFR/
フライトレコーダーの使い方
todo : 読んで試す

#### Configure JFR System Logging
JFRのログを取る方法
```shell-session{.line-numbers}
-XX:FlightRecorderLogging=jfr,system=debug
-XX:FlightRecorderLogging=all=trace
-XX:FlightRecorderLogging=jfr*=error
```


以下の制約がある
- ほとんどの VM 内部イベントや、スタック トレースやメモリ リーク検出などの高度な機能がまだありません。
- Windows 用の GraalVM ディストリビューションではサポートされていません。
- JFR は、GraalVM JDK 11 でビルドされたネイティブ実行可能ファイルでのみサポートされます。
  
できること
- JFR 機能のサブセット (カスタム イベント、システム イベント、およびディスク ベースの記録) 


カスタムイベントを利用する方法は、以下のガイドを参照
https://www.graalvm.org/22.2/reference-manual/native-image/guides/build-and-run-native-executable-with-jfr/


### Native Image Inspection Tool
 https://www.graalvm.org/22.2/reference-manual/native-image/debugging-and-diagnostics/InspectTool/
- ネイティブイメージのメソッドを一覧にする方法(InspectTool)
- ソフトウェア部品表 (SBOM) ：利用しているライブラリの情報の埋め込み

### Points-to Analysis Reports
 https://www.graalvm.org/22.2/reference-manual/native-image/debugging-and-diagnostics/StaticAnalysisReports/
メソッドの呼び出し関係や、オブジェクトの依存関係を確認したい場合の方法

Call tree （メソッドの呼び出し関係）
```text{.line-numbers}
VM Entry Points
├── entry <entry-method> id=<entry-method-id>
│   ├── directly calls <callee> id=<callee-id> @bci=<invoke-bci>
│   │   └── <callee-sub-tree>
│   ├── virtually calls <callee> @bci=<invoke-bci>
│   │   ├── is overridden by <override-method-i> id=<override-method-i-id>
│   │   │   └── <callee-sub-tree>
│   │   └── is overridden by <override-method-j> id-ref=<override-method-j-id>
│   └── interfacially calls <callee> @bci=<invoke-bci>
│       ├── is implemented by <implementation-method-x> id=<implementation-method-x-id>
│       │   └── <callee-sub-tree>
│       └── is implemented by <implementation-method-y> id-ref=<implementation-method-y-id>
├── entry <entry-method> id=<entry-method-id>
│   └── <callee-sub-tree>
└── ...
```

Object tree （オブジェクトの依存関係）


```text{.line-numbers}
Heap roots
├── root <root-field> value:
│   └── <value-type> id=<value-id> toString=<value-as-string> fields:
│       ├── <field-1> value=null
│       ├── <field-2> toString=<field-2-value-as-string> (expansion suppressed)
│       ├── <field-3> value:
│       │   └── <field-3-value-type> id=<field-3-value-id> toString=<field-3-value-as-string> fields:
│       │       └── <object-tree-rooted-at-field-3>
│       ├── <array-field-4> value:
│       │   └── <array-field-4-value-type> id=<array-field-4-value-id> toString=<array-field-4-value-as-string> elements (excluding null):
│       │       ├── [<index-i>] <element-index-i-value-type> id=<element-index-i-value-id> toString=<element-index-i-value-as-string> fields:
│       │       │   └── <object-tree-rooted-at-index-i>
│       │       └── [<index-j>] <element-index-j-value-type> id=<element-index-j-value-id> toString=<element-index-j-value-as-string> elements (excluding null):
│       │           └── <object-tree-rooted-at-index-j>
│       ├── <field-5> value:
│       │   └── <field-5-value-type> id-ref=<field-5-value-id> toString=<field-5-value-as-string>
│       ├── <field-6> value:
│       │   └── <field-6-value-type> id=<field-6-value-id> toString=<field-6-value-as-string> (no fields)
│       └── <array-field-7> value:
│           └── <array-field-7-value-type> id=<array-field-7-id> toString=<array-field-7-as-string> (no elements)
├── root <root-field> id-ref=<value-id> toString=<value-as-string>
├── root <root-method> value:
│   └── <object-tree-rooted-at-constant-embeded-in-the-method-graph>
└── ...
```

## Dynamic Features of Java

### Accessing Resources in Native Image
https://www.graalvm.org/22.2/reference-manual/native-image/dynamic-features/Resources/

ネイティブイメージにクラスパス上のリソースを組み込む方法
(何もしないと組み込まれない)

### Certificate Management in Native Image
https://www.graalvm.org/22.2/reference-manual/native-image/dynamic-features/CertificateManagement/

証明書ファイルの指定方法

## Dynamic Proxy in Native Image
https://www.graalvm.org/22.2/reference-manual/native-image/dynamic-features/DynamicProxy

java.lang.reflect.Proxy
を使う場合の注意点（リンク先あり）

## Java Native Interface (JNI) in Native Image
https://www.graalvm.org/22.2/reference-manual/native-image/dynamic-features/JNI/

- java->native
- native->java
どっちもできる。
設定は必要

## CA Security Services in Native Image
https://www.graalvm.org/22.2/reference-manual/native-image/dynamic-features/JCASecurityServices/

Java Cryptography Architecture (JCA) 
- デジタル署名
- メッセージ ダイジェスト
- 証明書と証明書の検証
- 暗号化 (対称/非対称ブロック/ストリーム暗号)
- 鍵の生成と管理
- 安全な乱数生成など
に関連する

ちょっと使うときには注意が必要そう。
もともと詳しくないところなので使うことがあったら調べる。

## Reflection in Native Image
https://www.graalvm.org/22.2/reference-manual/native-image/dynamic-features/Reflection/

Native Image はリフレクションを部分的にサポート
- リフレクションによってアクセスされるプログラム要素を事前に知る必要がある
- 生成するときに解決できない場合はコンフィグを書く必要がある
  - ReflectionConfigurationFiles=/path/to/reflectconfig

## URL Protocols in Native Image
https://www.graalvm.org/22.2/reference-manual/native-image/dynamic-features/URLProtocols/

最小限のバイナリから始めるために、デフォルトで有効になっているものは絞られている

サポートされているもの
- デフォルトで有効
  - file
  - resource
- デフォルトで無効
  - http
  - https
    - 有効にするとJCA関連も含まれる。＝大きくなる
  
他のプロトコルはテストされていない。
--enable-url-protocols=<protocols>
で有効にできるが、ちゃんと動かないかもしれない。サポートもされない

## Interoperability with Native Code
https://www.graalvm.org/22.2/reference-manual/native-image/native-code-interoperability/

Java コードをネイティブ共有ライブラリに変換できる。
共有ライブラリをネイティブコードから呼び出す方法は、2つある
- JNIのAPIを使う
- Native Image C API(GraalVMのネイティブイメージ固有のAPI)
  - やり取りするのは非オブジェクト型という制約はある

## LLVM Backend for Native Image
https://www.graalvm.org/22.2/reference-manual/native-image/LLVMBackend/

- 何ができるか？
  - ユーザーは GraalVM Native Image で直接サポートされていない代替アーキテクチャをターゲットにする
- どうやるか
  - LLVM 中間表現とLLVM コンパイラを使用してネイティブ実行可能ファイルを生成する代替バックエンドを利用する

面白そうだけど、今回は深堀しない

## Workshops and Interactive Labs
https://www.graalvm.org/22.2/reference-manual/native-image/workshops/

お試し用の環境をOracle CloudのLabで使える
- https://luna.oracle.com/
-  “Native Image”で検索

4つ紹介されていた
- GraalVM Native Image Quick Start
  - このラボは、クラウド ネイティブ Java アプリケーションの構築を開始したいと考えているネイティブ イメージを初めて使用する開発者を対象としています。
- GraalVM, Reflection and Native Image
  - このラボではnative-image、リフレクションを使用する場合にビルダーを事前構成し、ネイティブ イメージを使用して Java アプリケーションを事前コンパイルと互換性を持たせる方法を示します。
- GraalVM Native Image, Spring and Containerisation 
  - このラボでは、Spring Boot アプリケーションをネイティブ実行可能ファイルにパッケージ化する方法を示します。それをコンテナ化します。
  - 小さな Distroless コンテナー イメージを作成することで、フットプリントをさらに縮小します。
- GraalVM Native Image, Micronaut and OCI MySQL:
  - このラボでは、Oracle Cloud Infrastructure で Micronaut、GraalVM Native Image、MySQL データベース サービスを使用して、移植可能なクラウド ネイティブ Java アプリケーションを構築する方法を学習します。


## Contributing to Native Image
https://www.graalvm.org/22.2/reference-manual/native-image/contributing/

- バグ レポート、質問、機能強化のリクエスト
- Pull request
などの紹介