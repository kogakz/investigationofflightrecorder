# ボトルネックの検出(スレッドを待たせているイベントの確認)
Find Bottlenecks

JMCを使う方法と、jfrコマンドを使う2つの方法を説明している。

## JMCを利用する場合
  - ボトルネック箇所がグラフ上色がついてわかる。
  - 詳細はトラブルシュートを参照

## jfrコマンドを利用する方法

チェックするべきイベント（thread-stalling events）として、下記の12種類が紹介されている。

- IOPerfomanceの説明に該当イベントあり該当する
  - DISK
    - jdk.FileRead
    - jdk.FileWrite
  - NW
    - jdk.SocketRead
    - jdk.SocketWrite
- Synchronizationの説明に該当イベントあり
  - jdk.JavaMonitorWait
- 直接イベント名が記載されている説明箇所なし
  - jdk.JavaErrorThrow
  - jdk.JavaExceptionThrow
  - jdk.JavaMonitorEnter
  - jdk.ThreadStart
  - jdk.ThreadEnd
  - jdk.ThreadSleep
  - jdk.ThreadPark

※デフォルトの設定では、20ミリ秒より長いイベントのみが記録されます。(このしきい値は、フライト記録を開始するときに変更できます。)

要約すると、アプリケーションがファイルへの書込み(一度に一部分のみ)などの短いタスクを多数実行しているか、非常に短時間の同期に時間を費やしているとかの場合は、イベントとして記録されない可能性があります。