# Garbage Collection Performance

Garbage Collection Performance
GCによる問題は、通常個々のGCの時間がかかりすぎるか、GCの一時休止(GCの一時休止の合計)に多くの時間を費やしているかのいずれかです。

実際の状況を確認する際の注意点と、分析方法、GC問題の解消方法を説明する。

- [Garbage Collection Performance](#garbage-collection-performance)
  - [取得時の注意点](#取得時の注意点)
  - [取得後の分析方法](#取得後の分析方法)
    - [JMCを利用する場合](#jmcを利用する場合)
    - [jfrコマンドを利用する場合](#jfrコマンドを利用する場合)
  - [GC問題の解消方法](#gc問題の解消方法)
  - [その他参考サイト](#その他参考サイト)


## 取得時の注意点
以下を注意してフライドレコーダーの情報を取得する。
- アプリケーションのプロファイリング・フライトレコーダーの記録が必要
- ヒープ統計は含めないようにする。（OLD GCが実行されてしまうため）
- 長い期間とるようにする

## 取得後の分析方法

JMCを使う方法と、jfrコマンドを使う方法の2つを紹介する。

### JMCを利用する場合
- 見れること
    - 自動分析結果ページのGCセクション : Full GCの発生有無
- JVM内部のGCページ : GCによる停止時間の確認
    - 休止の合計（Sum of Pauses) がアプリケーションが出停止していた時間


### jfrコマンドを利用する場合
- 見れること
    - jdk.GCPhasePauseベントを出力する
- 実行例
    - `jfr print --events jdk.GCPhasePause イベントファイル名.jfr`

## GC問題の解消方法
- 個々のGCに時間がかかりすぎる場合
  - GCの戦略による影響の場合、GCの方法を変更する必要があります。
    - 一時休止の回数とスループット・パフォーマンスの対比となると、各GCには異なるトレードオフがあります。
    - [ガベージ・コレクション・チューニング・ガイド](https://docs.oracle.com/javase/jp/18/gctuning/index.html) の `2. エルゴノミクス 動作ベースのチューニング`を参考にチューニングする
- OutOfMemory 例外が"Java heap space"と同時に発生するようなケースの場合、ファイナライズの多用による影響かを確認するためには別の手段を利用する
  - トラブルシュートガイド の `3 メモリー・リークのトラブルシューティング`の`ネイティブ・メモリー・リークの診断`の`ファイナライズを保留中のオブジェクトのモニター`を参照 

- アプリケーションが一時休止している時間が多すぎる場合
  - Javaヒープ・サイズ(初期・最大）を増やす
    - 増やす際の目安として、`GC構成`を参照して必要なヒープサイズを推定する
  - GC対象になる一時オブジェクトを減らす
    - `TLAB割当て`を見て割り当てられているメモリサイズを確認する
      - メモリー不足が最も多い割当てサイトを確認します。
      - クラスまたはスレッドごとに表示して、最も多くの割当てを消費しているものを確認できます。
      - 補足説明
        - 小さいオブジェクトは`Thread Local Area Buffer (TLAB)内部`に割り当てられる
          - TLABが一杯になると、スレッドが新しい(TLAB)を獲得する <- これを減らす
        - 大きいオブジェクトは`Thread Local Area Buffer (TLAB)外部`に割り当てられる <- これを減らす
    - TLABはスレッド間でメモリ確保する際の競合を避けるために取得されるバッファ用クラス
      - 概要を知るために参考としたサイト
        - https://www.slideshare.net/kenjikazumura/gcjvm
        - http://hsmemo.github.io/articles/no-MX5_-HV.html

## その他参考サイト

[Flight Recorder + JMCを使った解析の事例](https://bell-sw.com/announcements/2020/09/02/Hunting-down-memory-issues-with-JDK-Flight-Recorder/)

