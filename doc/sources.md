# ソース
## com.oracle.svm.core.jfr

graal-master/substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jfr$ tree
.
├── HasJfrSupport.java
├── JfrBuffer.java
├── JfrBufferAccess.java
├── JfrBufferType.java
├── JfrBuffers.java
├── JfrChunkWriter.java
├── JfrConstantPool.java
├── JfrEvent.java
├── JfrEventWriteStatus.java
├── JfrEventWriterAccess.java
├── JfrFeature.java
├── JfrFrameType.java
├── JfrFrameTypeSerializer.java
├── JfrGCName.java
├── JfrGCNames.java
├── JfrGlobalMemory.java
├── JfrJavaEvents.java
├── JfrManager.java
├── JfrMetadataTypeLibrary.java
├── JfrMethodRepository.java
├── JfrNativeEventSetting.java
├── JfrNativeEventWriter.java
├── JfrNativeEventWriterData.java
├── JfrNativeEventWriterDataAccess.java
├── JfrOptionSet.java
├── JfrRecorderThread.java
├── JfrSerializerSupport.java
├── JfrStackTraceRepository.java
├── JfrSymbolRepository.java
├── JfrThreadLocal.java
├── JfrThreadRepository.java
├── JfrThreadState.java
├── JfrThreadStateSerializer.java
├── JfrTicks.java
├── JfrType.java
├── JfrTypeRepository.java
├── JfrUnlockedChunkWriter.java
├── Package_jdk_jfr_internal_event_helper.java
├── SubstrateJVM.java
├── Target_com_sun_jmx_mbeanserver_MXBeanIntrospector.java
├── Target_com_sun_jmx_mbeanserver_MXBeanLookup.java
├── Target_javax_management_MBeanServerFactory.java
├── Target_jdk_jfr_FlightRecorder.java
├── Target_jdk_jfr_internal_EventHandlerCreator.java
├── Target_jdk_jfr_internal_EventWriter.java
├── Target_jdk_jfr_internal_JVM.java
├── Target_jdk_jfr_internal_Options.java
├── Target_jdk_jfr_internal_Repository.java
├── Target_jdk_jfr_internal_SecuritySupport.java
├── Target_jdk_jfr_internal_StringPool.java
├── Target_jdk_jfr_internal_event_EventConfiguration.java
├── Target_jdk_jfr_internal_handlers_EventHandler.java
├── Target_jdk_jfr_internal_instrument_JDKEvents.java
├── Target_jdk_jfr_internal_jfc_JFC.java
├── Target_jdk_jfr_internal_jfc_JFCParser.java
├── events
│   ├── EndChunkNativePeriodicEvents.java
│   ├── EveryChunkNativePeriodicEvents.java
│   ├── ExecuteVMOperationEvent.java
│   ├── ExecutionSampleEvent.java
│   ├── JVMInformation.java
│   ├── JavaMonitorEnterEvent.java
│   ├── JavaMonitorWaitEvent.java
│   ├── SafepointBeginEvent.java
│   ├── SafepointEndEvent.java
│   ├── ThreadEndEvent.java
│   ├── ThreadParkEvent.java
│   ├── ThreadSleepEvent.java
│   └── ThreadStartEvent.java
├── logging
│   ├── JfrLogConfiguration.java
│   ├── JfrLogTag.java
│   ├── JfrLogging.java
│   └── Target_jdk_jfr_internal_LogTag.java
├── traceid
│   ├── JfrTraceId.java
│   ├── JfrTraceIdEpoch.java
│   └── JfrTraceIdMap.java
└── utils
    ├── JfrVisited.java
    └── JfrVisitedTable.java

## com.oracle.svm.hosted.jfr


/graal-master/substratevm/src/com.oracle.svm.hosted/src/com/oracle/svm/hosted$ tree 
.
├── AArch64CPUFeatureAccessFeature.java
├── AMD64CPUFeatureAccessFeature.java
├── BootLoaderSupport.java
├── BuildArtifactsExporter.java
├── CPUFeatureAccessFeatureBase.java
├── ClassLoaderFeature.java
├── ClassLoaderSupportImpl.java
├── ClassNewInstanceFeature.java
├── ClassPredefinitionFeature.java
├── ClassValueFeature.java
├── ConcurrentReachabilityHandler.java
├── ConditionalConfigurationRegistry.java
├── ConfigurationTypeResolver.java
├── DeadlockWatchdog.java
├── ExceptionSynthesizer.java
├── FallbackFeature.java
├── FeatureHandler.java
├── FeatureImpl.java
├── HostedConfiguration.java
├── ImageClassLoader.java
├── ImageSingletonsSupportImpl.java
├── LinkAtBuildTimeSupport.java
├── Log4ShellFeature.java
├── LoggingFeature.java
├── ModuleAccess.java
├── ModuleLayerFeature.java
├── NativeImageClassLoaderOptions.java
├── NativeImageClassLoaderPostProcessing.java
├── NativeImageClassLoaderSupport.java
├── NativeImageGenerator.java
├── NativeImageGeneratorRunner.java
├── NativeImageOptions.java
├── NativeImageSystemClassLoader.java
├── NativeImageSystemIOWrappers.java
├── NativeImageUtil.java
├── NativeSecureRandomFilesCloser.java
├── ProgressReporter.java
├── ProgressReporterCHelper.java
├── ProgressReporterJsonHelper.java
├── ProtectionDomainFeature.java
├── ReachabilityHandler.java
├── ReachabilityHandlerFeature.java
├── ResourcesFeature.java
├── SVMHost.java
├── SecurityServicesFeature.java
├── ServiceLoaderFeature.java
├── SubstitutionReportFeature.java
├── SubstrateDiagnosticFeature.java
├── SubstrateStrengthenGraphs.java
├── SystemInOutErrFeature.java
├── TemporaryBuildDirectoryProviderImpl.java
├── VMFeature.java
├── ameta
│   ├── AnalysisConstantFieldProvider.java
│   ├── AnalysisConstantReflectionProvider.java
│   ├── EmptyMemoryAcessProvider.java
│   ├── HostedDynamicHubFeature.java
│   └── package-info.java
├── analysis
│   ├── AnnotationsProcessor.java
│   ├── CallChecker.java
│   ├── CustomTypeFieldHandler.java
│   ├── DynamicHubInitializer.java
│   ├── Inflation.java
│   ├── NativeImagePointsToAnalysis.java
│   ├── NativeImageReachabilityAnalysisEngine.java
│   ├── PointsToCustomTypeFieldHandler.java
│   ├── SVMAnalysisMetaAccess.java
│   ├── SVMParsingSupport.java
│   ├── SubstrateUnsupportedFeatures.java
│   ├── UserLimitationsChecker.java
│   ├── flow
│   │   ├── SVMMethodTypeFlowBuilder.java
│   │   └── package-info.java
│   └── package-info.java
├── annotation
│   ├── AnnotationArrayValue.java
│   ├── AnnotationClassValue.java
│   ├── AnnotationEnumValue.java
│   ├── AnnotationExceptionProxyValue.java
│   ├── AnnotationMemberValue.java
│   ├── AnnotationMetadata.java
│   ├── AnnotationPrimitiveValue.java
│   ├── AnnotationStringValue.java
│   ├── AnnotationSubstitutionField.java
│   ├── AnnotationSubstitutionMethod.java
│   ├── AnnotationSubstitutionType.java
│   ├── AnnotationSupport.java
│   ├── AnnotationTypeFeature.java
│   ├── AnnotationValue.java
│   ├── ConstantAnnotationMarkerSubstitutionType.java
│   ├── CustomSubstitution.java
│   ├── CustomSubstitutionField.java
│   ├── CustomSubstitutionMethod.java
│   ├── CustomSubstitutionType.java
│   ├── SubstrateAnnotationExtractor.java
│   ├── TypeAnnotationValue.java
│   └── package-info.java
├── c
│   ├── BuiltinDirectives.java
│   ├── CAnnotationProcessor.java
│   ├── CAnnotationProcessorCache.java
│   ├── CConstantValueSupportImpl.java
│   ├── CGlobalDataFeature.java
│   ├── CInterfaceError.java
│   ├── DirectivesExtension.java
│   ├── NativeCodeContext.java
│   ├── NativeLibraries.java
│   ├── OffsetOfSupportImpl.java
│   ├── SizeOfSupportImpl.java
│   ├── codegen
│   │   ├── CCompilerInvoker.java
│   │   ├── CSourceCodeWriter.java
│   │   ├── QueryCodeWriter.java
│   │   └── package-info.java
│   ├── function
│   │   ├── CEntryPointSupport.java
│   │   └── package-info.java
│   ├── info
│   │   ├── AccessorInfo.java
│   │   ├── ConstantInfo.java
│   │   ├── ElementInfo.java
│   │   ├── EnumConstantInfo.java
│   │   ├── EnumInfo.java
│   │   ├── EnumLookupInfo.java
│   │   ├── EnumValueInfo.java
│   │   ├── InfoTreeBuilder.java
│   │   ├── InfoTreeVisitor.java
│   │   ├── NativeCodeInfo.java
│   │   ├── PointerToInfo.java
│   │   ├── PropertyInfo.java
│   │   ├── RawPointerToInfo.java
│   │   ├── RawStructureInfo.java
│   │   ├── SizableInfo.java
│   │   ├── StructBitfieldInfo.java
│   │   ├── StructFieldInfo.java
│   │   ├── StructInfo.java
│   │   └── package-info.java
│   ├── libc
│   │   ├── HostedBionicLibC.java
│   │   ├── HostedGLibC.java
│   │   ├── HostedLibCBase.java
│   │   ├── HostedLibCFeature.java
│   │   ├── HostedMuslLibC.java
│   │   └── HostedNoLibC.java
│   ├── query
│   │   ├── NativeInfoTreeVisitor.java
│   │   ├── QueryParserUtil.java
│   │   ├── QueryResultFormat.java
│   │   ├── QueryResultParser.java
│   │   ├── RawStructureLayoutPlanner.java
│   │   ├── SizeAndSignednessVerifier.java
│   │   └── package-info.java
│   └── util
│       ├── FileUtils.java
│       └── package-info.java
├── cenum
│   ├── CEnumCallWrapperMethod.java
│   ├── CEnumCallWrapperSubstitutionProcessor.java
│   └── package-info.java
├── classinitialization
│   ├── AllowAllHostedUsagesClassInitializationSupport.java
│   ├── ClassInitializationConfiguration.java
│   ├── ClassInitializationFeature.java
│   ├── ClassInitializationOptions.java
│   ├── ClassInitializationSupport.java
│   ├── ClassInitializerGraphBuilderPhase.java
│   ├── ClassOrPackageConfig.java
│   ├── EarlyClassInitializerAnalysis.java
│   ├── InitKind.java
│   ├── ProvenSafeClassInitializationSupport.java
│   ├── TypeInitializerGraph.java
│   └── package-info.java
├── code
│   ├── AnalysisMethodCalleeWalker.java
│   ├── AnalysisToHostedGraphTransplanter.java
│   ├── CCallStubMethod.java
│   ├── CEntryPointCallStubMethod.java
│   ├── CEntryPointCallStubSupport.java
│   ├── CEntryPointData.java
│   ├── CEntryPointJavaCallStubMethod.java
│   ├── CEntryPointLiteralFeature.java
│   ├── CFunctionCallStubMethod.java
│   ├── CFunctionLinkages.java
│   ├── CFunctionPointerCallStubMethod.java
│   ├── CFunctionPointerCallStubSupport.java
│   ├── CFunctionSubstitutionProcessor.java
│   ├── CompilationGraph.java
│   ├── CompilationInfo.java
│   ├── CompileQueue.java
│   ├── DeoptimizationUtils.java
│   ├── EntryPointCallStubMethod.java
│   ├── FactoryMethod.java
│   ├── FactoryMethodSupport.java
│   ├── FrameInfoHostedMethodData.java
│   ├── HostedCodeCacheProvider.java
│   ├── HostedDirectCallTrampolineSupport.java
│   ├── HostedImageHeapConstantPatch.java
│   ├── HostedPatcher.java
│   ├── HostedReplacements.java
│   ├── HostedRuntimeConfigurationBuilder.java
│   ├── InliningUtilities.java
│   ├── NativeMethodSubstitutionProcessor.java
│   ├── NonBytecodeStaticMethod.java
│   ├── RestrictHeapAccessAnnotationChecker.java
│   ├── RestrictHeapAccessCalleesImpl.java
│   ├── SharedRuntimeConfigurationBuilder.java
│   ├── SimpleSignature.java
│   ├── SubstrateCompilationDirectives.java
│   ├── SubstrateGraphMaker.java
│   ├── SubstrateGraphMakerFactory.java
│   ├── SubstrateHostedCompilationIdentifier.java
│   ├── SubstrateLIRBackendFeature.java
│   ├── UninterruptibleAnnotationChecker.java
│   ├── aarch64
│   │   ├── AArch64HostedPatcherFeature.java
│   │   ├── AArch64HostedTrampolineSupport.java
│   │   └── package-info.java
│   ├── amd64
│   │   ├── AMD64HostedPatcherFeature.java
│   │   ├── AMD64HostedTrampolineSupport.java
│   │   └── package-info.java
│   └── package-info.java
├── config
│   ├── ConfigurationParserUtils.java
│   ├── HybridLayout.java
│   ├── HybridLayoutSupport.java
│   ├── ReflectionRegistryAdapter.java
│   └── package-info.java
├── dashboard
│   ├── CodeBreakdownJsonObject.java
│   ├── DashboardDumpFeature.java
│   ├── DashboardOptions.java
│   ├── HeapBreakdownJsonObject.java
│   ├── PointsToJsonObject.java
│   └── ToJson.java
├── diagnostic
│   ├── HostedHeapDump.java
│   └── HostedHeapDumpFeature.java
├── doc-files
│   ├── LinkAtBuildTimeHelp.txt
│   └── LinkAtBuildTimePathsHelp.txt
├── fieldfolding
│   └── StaticFinalFieldFoldingFeature.java
├── heap
│   ├── HeapDumpFieldsMapFeature.java
│   ├── ImageHeapMapFeature.java
│   ├── PodFactorySubstitutionMethod.java
│   ├── PodSupport.java
│   ├── SVMImageHeapScanner.java
│   ├── SVMImageHeapVerifier.java
│   └── package-info.java
├── image
│   ├── AbstractImage.java
│   ├── CCLinkerInvocation.java
│   ├── DisallowedImageHeapObjectFeature.java
│   ├── ExecutableImageViaCC.java
│   ├── HeapHistogram.java
│   ├── ImageHeapFillerObjectsFeature.java
│   ├── LIRNativeImageCodeCache.java
│   ├── LLVMToolchain.java
│   ├── NativeImage.java
│   ├── NativeImageBFDNameProvider.java
│   ├── NativeImageCodeCache.java
│   ├── NativeImageCodeCacheFactory.java
│   ├── NativeImageDebugInfoFeature.java
│   ├── NativeImageDebugInfoProvider.java
│   ├── NativeImageHeap.java
│   ├── NativeImageHeapWriter.java
│   ├── NativeImageViaCC.java
│   ├── ObjectGroupHistogram.java
│   ├── RelocatableBuffer.java
│   ├── SharedLibraryImageViaCC.java
│   ├── StringInternFeature.java
│   ├── package-info.java
│   └── sources
│       ├── SourceCache.java
│       └── SourceManager.java
├── javafx
│   ├── JavaFXFeature.java
│   └── package-info.java
├── jdk
│   ├── AccessControlContextReplacerFeature.java
│   ├── JDKInitializationFeature.java
│   ├── JDKRegistrations.java
│   ├── JNIRegistrationAWTSupport.java
│   ├── JNIRegistrationAwt.java
│   ├── JNIRegistrationJava.java
│   ├── JNIRegistrationJavaNet.java
│   ├── JNIRegistrationJavaNio.java
│   ├── JNIRegistrationManagementExt.java
│   ├── JNIRegistrationPrefs.java
│   ├── JNIRegistrationSupport.java
│   ├── JNIRegistrationsJavaZip.java
│   ├── JRTFeature.java
│   ├── localization
│   │   ├── CharsetSubstitutionsFeature.java
│   │   └── LocalizationFeature.java
│   └── package-info.java
├── jfr
│   ├── JfrEventFeature.java
│   └── JfrEventSubstitution.java
├── jni
│   ├── JNIAccessFeature.java
│   ├── JNIAutomaticFeature.java
│   ├── JNICallTrampolineMethod.java
│   ├── JNICallWrapperFeature.java
│   ├── JNIFeature.java
│   ├── JNIFieldAccessorMethod.java
│   ├── JNIFunctionTablesFeature.java
│   ├── JNIGraphKit.java
│   ├── JNIJavaCallVariantWrapperMethod.java
│   ├── JNIJavaCallWrapperMethod.java
│   ├── JNILibraryLoadFeature.java
│   ├── JNINativeCallWrapperMethod.java
│   ├── JNINativeCallWrapperSubstitutionProcessor.java
│   └── JNIPrimitiveArrayOperationMethod.java
├── lambda
│   ├── LambdaProxyRenamingSubstitutionProcessor.java
│   ├── LambdaSubstitutionType.java
│   ├── StableLambdaProxyNameFeature.java
│   └── package-info.java
├── meta
│   ├── HostedArrayClass.java
│   ├── HostedClass.java
│   ├── HostedConstantFieldProvider.java
│   ├── HostedConstantReflectionProvider.java
│   ├── HostedField.java
│   ├── HostedInstanceClass.java
│   ├── HostedInterface.java
│   ├── HostedMemoryAccessProvider.java
│   ├── HostedMetaAccess.java
│   ├── HostedMethod.java
│   ├── HostedPrimitiveType.java
│   ├── HostedSnippetReflectionProvider.java
│   ├── HostedType.java
│   ├── HostedUniverse.java
│   ├── KnownOffsetsFeature.java
│   ├── MaterializedConstantFields.java
│   ├── SharedConstantFieldProvider.java
│   ├── TypeCheckBuilder.java
│   ├── UniverseBuilder.java
│   └── package-info.java
├── methodhandles
│   └── MethodHandleFeature.java
├── nodes
│   ├── DeoptProxyNode.java
│   └── package-info.java
├── option
│   ├── HostedOptionCustomizer.java
│   ├── HostedOptionParser.java
│   ├── HostedOptionProvider.java
│   ├── RuntimeOptionFeature.java
│   └── package-info.java
├── package-info.java
├── phases
│   ├── AnalysisGraphBuilderPhase.java
│   ├── CInterfaceEnumTool.java
│   ├── CInterfaceInvocationPlugin.java
│   ├── ConstantFoldLoadFieldPlugin.java
│   ├── DeoptimizationTargetBciBlockMapping.java
│   ├── DevirtualizeCallsPhase.java
│   ├── EarlyConstantFoldLoadFieldPlugin.java
│   ├── EnumSwitchPlugin.java
│   ├── HostedGraphBuilderPhase.java
│   ├── HostedGraphKit.java
│   ├── ImageBuildStatisticsCounterPhase.java
│   ├── ImplicitAssertionsPhase.java
│   ├── InjectedAccessorsPlugin.java
│   ├── InlineBeforeAnalysisPolicyImpl.java
│   ├── IntrinsifyMethodHandlesInvocationPlugin.java
│   ├── SharedGraphBuilderPhase.java
│   ├── StrengthenStampsPhase.java
│   ├── SubstrateClassInitializationPlugin.java
│   ├── SubstrateGraphBuilderPhase.java
│   ├── VerifyDeoptLIRFrameStatesPhase.java
│   ├── VerifyNoGuardsPhase.java
│   └── package-info.java
├── reflect
│   ├── AnnotationEncoder.java
│   ├── ReflectionDataBuilder.java
│   ├── ReflectionExpandSignatureMethod.java
│   ├── ReflectionFeature.java
│   ├── ReflectionGraphKit.java
│   ├── ReflectionHostedSupport.java
│   ├── ReflectionMetadata.java
│   ├── ReflectionMetadataEncoderImpl.java
│   ├── proxy
│   │   ├── DynamicProxyFeature.java
│   │   └── ProxyRegistry.java
│   └── serialize
│       └── SerializationFeature.java
├── snippets
│   ├── ExceptionUnwindFeature.java
│   ├── ImplicitExceptionsFeature.java
│   ├── IntrinsificationPluginRegistry.java
│   ├── ReflectionPlugins.java
│   ├── SubstrateGraphBuilderPlugins.java
│   └── package-info.java
├── substitute
│   ├── AnnotatedField.java
│   ├── AnnotatedMethod.java
│   ├── AnnotationSubstitutionProcessor.java
│   ├── ComputedValue.java
│   ├── ComputedValueField.java
│   ├── DeletedElementException.java
│   ├── DeletedFieldsPlugin.java
│   ├── DeletedMethod.java
│   ├── InjectedFieldsType.java
│   ├── PolymorphicSignatureWrapperMethod.java
│   ├── SubstitutionField.java
│   ├── SubstitutionMethod.java
│   ├── SubstitutionReflectivityFilter.java
│   ├── SubstitutionType.java
│   ├── UnsafeAutomaticSubstitutionProcessor.java
│   └── package-info.java
├── thread
│   ├── CEntryPointFeature.java
│   ├── VMThreadLocalCollector.java
│   ├── VMThreadMTFeature.java
│   ├── VMThreadSTFeature.java
│   └── package-info.java
├── util
│   ├── DiagnosticUtils.java
│   └── VMErrorReporter.java
└── xml
    ├── JavaxXmlClassAndResourcesLoaderFeature.java
    ├── XMLParsersRegistration.java
    └── package-info.java