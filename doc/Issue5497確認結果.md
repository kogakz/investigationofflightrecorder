# Add support for Socket JFR events (#5497)
SocketReadの対応したIssue[Add support for Socket JFR events (#5497)](https://github.com/oracle/graal/pull/5497)


対象ソース
substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jdk/JavaNetSocketSubstitutions.java
1. 最初はJavaSocketSubstitutionsとして作成
2. rename
3. mergeされずに1か月間にクローズされていた

substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jfr/events/SocketReadEvent.java
→最初作成されたが、jdk.jfr.events.SocketReadEventを使うようにリファクタされた結果削除されている


## substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jdk/JavaNetSocketSubstitutions.java
対象ファイルなし
