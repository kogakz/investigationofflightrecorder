# Exception occurred when setting value "150/s" for class jdk.jfr.internal.Control** について


```shell-session{.line-numbers}
#./jfrdemo -XX:+FlightRecorder -XX:StartFlightRecording="filename=recording.jfr"
[warn][jfr,setting] Exception occurred when setting value "150/s" for class jdk.jfr.internal.Control
```

warnが発生したので調べたところ、類似事象に引っかかった。
https://github.com/oracle/graal/issues/4431

- 内容サマリ
  - OpenJDK17でsetThrottleがstartup時に呼び出されてしまうため出ている
  - setThrottleを呼ばれても何もしないよう、22.3向けにマージされた。
    - 10/23現在
      - CE版は22.2
      - Enterpriseは22.3
  - PR
    - https://github.com/oracle/graal/pull/4895
  - 変更ソース
    - Target_jdk_jfr_internal_JVM.java
  - ソースパス
    - https://github.com/oracle/graal/tree/master/substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jfr


## 実装確認

###  修正後ソース

- 何もせずにtrueのみ返却
- コメントでなぜそうしているかを書いている
 
```java{.line-numbers}
    public boolean setThrottle(long eventTypeId, long eventSampleSize, long periodMs) {
        // Not supported but this method is called during JFR startup, so we can't throw an error.
        return true;
    }

```

### 修正前のソース

- "SubstrateJVM"の"setThrottle"を呼び出した結果を返却している
 
```java{.line-numbers}
   @Substitute
    @TargetElement(onlyWith = JDK17OrLater.class)
    public boolean setThrottle(long eventTypeId, long eventSampleSize, long periodMs) {
        return SubstrateJVM.get().setThrottle(eventTypeId, eventSampleSize, periodMs);
    }
```

### 修正案のソース

- 下記のようにメッセージを出す案が出ていた。
- しかし、そのためにloggerも追加でインポートする必要もあるため、そこまでは不要では？ということで上記の修正結果となった。
- 追加インポートパッケージ
  - jdk.jfr.internal.LogLevel;
  - jdk.jfr.internal.Logger;


```java{.line-numbers}
    /** See {@link JVM#setThrottle}. */
    public boolean setThrottle(long eventTypeId, long eventSampleSize, long periodMs) {
        Logger.log(LogTag.JFR_SYSTEM_THROTTLE, LogLevel.WARN, "JFR event throttling is not yet supported in native images.");
        return true;
    }
```