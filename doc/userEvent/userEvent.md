# Build and Run Native Executables with JFR
https://www.graalvm.org/22.2/reference-manual/native-image/guides/build-and-run-native-executable-with-jfr/

jdk.jfr.Event APIを使って、イベントを記録する方法を説明するガイド


JDK Flight Recorder (JFR) は、JVM に組み込まれた実行中の Java アプリケーションに関する診断およびプロファイリング データを収集するためのツールです。GraalVM ネイティブ イメージは JFR イベントをサポートし、ユーザーは Java HotSpot VM で JFR を使用する場合と同様のエクスペリエンスで jdk.jfr.Event API を使用できます。

ネイティブ実行可能ファイルの実行時に JFR イベントを記録するには、JFR サポートと JFR 記録を有効にする必要があります。
このガイドでは、その方法について説明します。



## 実行時にJFRを有効にする方法

JFRイベントの記録をサポートするようにネイティブ実行可能ファイルをビルドするためには、
ビルド時にJFRをincludeし、次にシステムを有効にし、記録を開始し、ネイティブ実行可能ファイルの実行時にログを構成する必要があります。

次のオプションがサポートされています。

`-H:+AllowVMInspection:` VM インスペクションを有効にします
`-XX:+FlightRecorder:` JFRを有効にするために使用します
`-XX:StartFlightRecording:` アプリケーションの起動時に記録を開始するために使用します
`-XX:FlightRecorderLogging:` JFRシステムのログ出力を構成するために使用します

以下の手順に従って、JFR をサポートするネイティブ実行可能ファイルをビルドし、実行時にイベントを記録する練習をしてください。

- サンプルの説明
  - サンプルアプリケーションの構成
    - 単純なクラスと JDK ライブラリ クラスのみ
- 実装内容
  - パッケージ@Labelからの注釈でラベル付けされたイベントを作成します。
- 実行結果
  - 何も出力されず、そのイベントだけが実行されます。

### 次のコードをJFRDemo.javaという名前のファイルに保存します。
```java{.line-numbers}
 import jdk.jfr.Event;
 import jdk.jfr.Description;
 import jdk.jfr.Label;

 public class JFRDemo {

   @Label("Hello World")
   @Description("Build and run a native executable with JFR.")
   static class HelloWorldEvent extends Event {
       @Label("Message")
       String message;
   }

   public static void main(String... args) {
       HelloWorldEvent event = new HelloWorldEvent();
       event.message = "Hello, World!";
       event.commit();
   }
 }
```

### GraalVM JDK で Java ファイルをコンパイルします。

以下の手順で
- JFRDemo$HelloWorldEvent.class 
- JFRDemo.class
が生成されることを確認した 

```shell-session{.line-numbers}
#javac JFRDemo.java
#ls -ltr
-rw-r--r-- 1 kogakz kogakz 466 Oct 23 08:22  JFRDemo.java
-rw-r--r-- 1 kogakz kogakz 475 Oct 23 08:22  JFRDemo.class
-rw-r--r-- 1 kogakz kogakz 512 Oct 23 08:22 'JFRDemo$HelloWorldEvent.class'
```


### VM インスペクションを有効にしてネイティブ実行可能ファイルをビルドします。

以下の手順で2ファイルが生成されていることを確認した
- jfrdemo : 実行可能ファイル
- jfrdemo.build_artifacts.txt: ビルドの指示書的な？

なお、`[2/7] Performing analysis...`のところで、1回終了コード137でコマンドが異常終了したが、OOMによるものだった。[^1]
プロセスを落としてやり直したところ正常に終了した。


```shell-session{.line-numbers}
#native-image -H:+AllowVMInspection JFRDemo
WARNING: Unknown module: org.graalvm.nativeimage.llvm specified to --add-exports
WARNING: Unknown module: org.graalvm.nativeimage.llvm specified to --add-exports
WARNING: Unknown module: org.graalvm.nativeimage.llvm specified to --add-exports
========================================================================================================================
GraalVM Native Image: Generating 'jfrdemo' (executable)...
========================================================================================================================
[1/7] Initializing...                                                                                    (3.1s @ 0.11GB)
 Version info: 'GraalVM 22.2.0 Java 17 CE'
 Java version info: '17.0.4+8-jvmci-22.2-b06'
 C compiler: gcc (linux, x86_64, 9.4.0)
 Garbage collector: Serial GC
[2/7] Performing analysis...  [*****]                                                                   (10.8s @ 0.52GB)
   3,768 (79.53%) of  4,738 classes reachable
   4,627 (53.98%) of  8,571 fields reachable
  16,914 (47.18%) of 35,847 methods reachable
      27 classes,    12 fields, and   380 methods registered for reflection
      59 classes,    59 fields, and    52 methods registered for JNI access
       4 native libraries: dl, pthread, rt, z
[3/7] Building universe...                                                                               (1.1s @ 0.35GB)
[4/7] Parsing methods...      [*]                                                                        (0.9s @ 0.47GB)
[5/7] Inlining methods...     [***]                                                                      (0.4s @ 1.00GB)
[6/7] Compiling methods...    [***]                                                                      (5.3s @ 0.80GB)
[7/7] Creating image...                                                                                  (1.6s @ 0.58GB)
   5.73MB (36.00%) for code area:     9,717 compilation units
   9.55MB (60.02%) for image heap:  140,461 objects and 5 resources
 647.61KB ( 3.97%) for other data
  15.92MB in total
------------------------------------------------------------------------------------------------------------------------
Top 10 packages in code area:                               Top 10 object types in image heap:
 739.25KB java.util                                            1.26MB java.lang.String
 386.09KB java.lang                                            1.23MB byte[] for code metadata
 344.88KB jdk.jfr.internal                                     1.18MB byte[] for general heap data
 273.99KB java.text                                          883.16KB java.lang.Class
 234.86KB java.util.regex                                    808.51KB byte[] for java.lang.String
 218.67KB java.util.concurrent                               539.25KB java.util.HashMap$Node
 198.42KB com.oracle.svm.jni                                 323.81KB com.oracle.svm.core.hub.DynamicHubCompanion
 159.29KB java.math                                          276.36KB java.util.HashMap$Node[]
 136.24KB jdk.jfr.internal.consumer                          253.74KB java.lang.String[]
 128.30KB java.lang.invoke                                   172.72KB java.lang.Object[]
   2.91MB for 156 more packages                                2.34MB for 1058 more object types
------------------------------------------------------------------------------------------------------------------------
                        0.9s (3.7% of total time) in 27 GCs | Peak RSS: 1.94GB | CPU load: 8.19
------------------------------------------------------------------------------------------------------------------------
Produced artifacts:
 /home/kogakz/gitouhon2022-2nd/userEvent/jfrdemo (executable)
 /home/kogakz/gitouhon2022-2nd/userEvent/jfrdemo.build_artifacts.txt (txt)
========================================================================================================================
Finished generating 'jfrdemo' in 24.7s.


#ls -ltr
total 16320
-rw-r--r-- 1 kogakz kogakz      466 Oct 23 08:22  JFRDemo.java
-rw-r--r-- 1 kogakz kogakz      475 Oct 23 08:22  JFRDemo.class
-rw-r--r-- 1 kogakz kogakz      512 Oct 23 08:22 'JFRDemo$HelloWorldEvent.class'
-rwxr-xr-x 1 kogakz kogakz 16691616 Oct 23 08:29  jfrdemo
-rw-r--r-- 1 kogakz kogakz       22 Oct 23 08:29  jfrdemo.build_artifacts.txt
```

build_artifacts.txtの中身
```text{.line-numbers}
[EXECUTABLE]
jfrdemo
```

### 実行可能ファイルを実行し、記録を開始します。

以下の手順で`recording.jfr`が生成されることを確認した。
補足：warnが発生したので調べた[^2]ところ、既知事象だったらしく修正が22.3でマージされる予定。

```shell-session{.line-numbers}
#./jfrdemo -XX:+FlightRecorder -XX:StartFlightRecording="filename=recording.jfr"
[warn][jfr,setting] Exception occurred when setting value "150/s" for class jdk.jfr.internal.Control

# ls -ltr 
total 16424
-rw-r--r-- 1 kogakz kogakz      466 Oct 23 08:22  JFRDemo.java
-rw-r--r-- 1 kogakz kogakz      512 Oct 23 09:08 'JFRDemo$HelloWorldEvent.class'
-rw-r--r-- 1 kogakz kogakz      475 Oct 23 09:08  JFRDemo.class
-rwxr-xr-x 1 kogakz kogakz 16691616 Oct 23 09:09  jfrdemo
-rw-r--r-- 1 kogakz kogakz       22 Oct 23 09:09  jfrdemo.build_artifacts.txt
-rw-r--r-- 1 kogakz kogakz   103339 Oct 23 09:10  recording.jfr
```

### JFRファイルの確認

作成したJFRの中身を、以下の手段で参照することができた。
- Windows環境にあるJMCを使って、jfrのファイルを開いてイベントを確認できた。
- jfrコマンドで見ることもできた
  
なお、ガイドにはVisualVMを起動して、使いやすい方法で記録ファイルの内容を表示するとあったが、確認した所入ってなかった。
- [visualvmのドキュメント](https://visualvm.github.io/graal.html)にはCE版にも入っているとは書いてあった。
- WSL2上のCUIのみの環境だからかもしれないが、深堀はしていない。

jfrコマンドを使った参照例
```shell-session{.line-numbers}
#jfr print recording.jfr  | head 
jdk.ActiveSetting {
  startTime = 09:10:05.809
  id = 22245
  name = "threshold"
  value = "0 ns"
  eventThread = "main" (javaThreadId = 1)
}

jdk.ActiveSetting {
  startTime = 09:10:05.809
```

JMCでの参照例

![JMCの実行画面](images/221023_103228.png)


`graalvm-ce-java17-22.2.0/bin/`配下のコマンドを確認したが、下記の通りjvisualvmは入っていなかった。
```shell-session{.line-numbers}
$ls ../graalvm-ce-java17-22.2.0/bin/
gu         java     javap     jdb        jfr     jinfo  jmod      jrunscript  jstat    native-image            rebuild-images
jar        javac    jcmd      jdeprscan  jhsdb   jlink  jpackage  jshell      jstatd   native-image-configure  rmiregistry
jarsigner  javadoc  jconsole  jdeps      jimage  jmap   jps       jstack      keytool  polyglot                serialver
```

[^1]: ImageBuildが137で異常終了した原因調査の[詳細は別ページ](imageBuildError137.md)にまとめた
[^2]: Exception occurred when setting value "150/s" for class jdk.jfr.internal.Controlの[詳細は別ページ](ExceptionSetThrottle.md)にまとめた


