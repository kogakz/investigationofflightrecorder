#  ImageBuildが137で異常終了した原因調査

```shell-session{.line-numbers}
[2/7] Performing analysis...  [Error: Image build request failed with exit status 137`
```
成功する前に一度上記のようなエラーで失敗した。
137はOOMによるエラーということなので、一度VsCodeを再起動した。

https://gitter.im/graalvm/native-image?at=5cd41de4f251e60ffa506ea1

David M. Lloyd @dmlloyd 5月 09 2019 21:32
```text{.line-numbers}
an exit code of 137 means the OS killed it with a signal 9 (SIGKILL)on Linux, 
this normally happens when the OOM killer gets stabby
so, you probably just ran out of OS memory

jerrinot so I guess there's something wrong with CodeInfoTable on ARM64
I think I speak for everyone when I say that it's definitely @bobmcwhirter's fault :)
(actually I have no idea. hopefully I can get to my other system today, where I can dig into this a bit more)

exit code trick: exit codes which are >128 are usually signal numbers, so subtract 128 from the exit code to get the signal number
then man 7 signal to look it up just in case you don't have them all memorized :)
```
stabbyは怒った、イライラしたという俗語

終了コードが128以上の場合、終了コードから128を引いたシグナル番号で終了したとみるのが通例。

つまり、終了コードが137 の場合は`137- 128 = Signal 9`で終了したと見ればよい。
また、シグナル番号は`man 7 signal`で調べられる。



