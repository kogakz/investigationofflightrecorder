package com.example;

public class ThreadPool implements Runnable {

  private static Counter lock = new Counter();

  private static int count = 0;

  @Override
  public void run() {

    for (int i = 0; i < 30000; i++) {
      System.out.println(Thread.currentThread().getName() + ":" + count);
      count++;
    }

  }

}