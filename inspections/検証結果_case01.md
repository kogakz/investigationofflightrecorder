# Case01の検証結果

---

## Java(G1GC)でのGCの発生状況の確認結果

|            ページ            |   図   |                       確認できたこと                       |
| ---------------------------- | ------ | ---------------------------------------------------------- |
| レポートページ               | 図01-1 | フルGCの摘出を警告している。                               |
| ガベージコレクションのページ | 図01-2 | コレクタ名でソートし、G1Fullが発生した回数、時刻、停止時間 |
| ガベージコレクションのページ | 図01-3 | 最長の一時停止でソートすることで、停止時間の傾向           |
| イベントブラウザページ       | 図01-4 | configurationの部分でヒープサイズとか設定を確認できる      |


<figure><img src='images/221106_084653.png' width='600px'><figcaption>図01-1. レポート</figcaption></figure>
<figure><img src='images/221106_090350.png' width='600px'><figcaption>図01-2. ガベージコレクションのページ(コレクタ名でソート)</figcaption></figure>
```text{.line-numbers}
対象時刻のGCログ抜粋
[2022-11-03T08:58:18.519+0900][135322ms] GC(959) Pause Full (G1 Compaction Pause) 397M->97M(400M) 6.622ms
```
<figure><img src='images/221106_085727.png' width='600px'><figcaption>図01-3. ガベージコレクションのページ(最長の一時停止でソート)</figcaption></figure>
<figure><img src='images/221106_100444.png' width='600px'><figcaption>図01-4. タイトル</figcaption></figure>

---

## Nativeでの確認結果

GCによる停止時間は見れた。
出方が違うのはGCの種類の問題なのか、native化による影響なのかがわからない。

|            ページ            |   図   |                       確認できたこと                       |             気になったこと             |
| ---------------------------- | ------ | ---------------------------------------------------------- | -------------------------------------- |
| レポートページ               | 図01-5 | 特に指摘は出てなかった。                                   | 特になし                               |
| ガベージコレクションのページ | 図01-6 | コレクタ名でソートし、G1Fullが発生した回数、時刻、停止時間 | ヒープのグラフが出ない                 |
| ガベージコレクションのページ | 図01-7 | 最長の一時停止でソートすることで、停止時間の傾向           | GCの違いによる差なのかがわからない     |
| イベントブラウザページ       | 図01-8 | configurationの情報は出ていなかった                        | 他にも出てなかった。                   |
| イベントブラウザページ       | 図01-9 | FullGCの発生タイミング                                     | JavaでもSerialGCだと同じように出るのか |



<figure><img src='images/221106_142458.png' width='600px'><figcaption>図01-5. レポート</figcaption></figure>

<figure><img src='images/221106_142857.png' width='600px'><figcaption>図01-6. ガベージコレクションのページ(コレクタ名でソート)</figcaption></figure>

<figure><img src='images/221106_143025.png' width='600px'><figcaption>図01-7. ガベージコレクションのページ(最長の一時停止でソート)</figcaption></figure>

<figure><img src='images/221106_143226.png' width='600px'><figcaption>図01-8. イベントブラウザ(Configure)</figcaption></figure>

<figure><img src='images/221106_143542.png' width='600px'><figcaption>図01-9. イベントブラウザ(Phases)</figcaption></figure>

kogakz@koga_hp:~/gitouhon2022-2nd/inspections/case01/jfr/native$ jfr summary native-post-startup.jfr 

 Version: 2.0
 Chunks: 1
 Start: 2022-11-06 09:56:10 (UTC)
 Duration: 318 s

 Event Type                              Count  Size (bytes) 
=============================================================
 jdk.JavaMonitorWait                     16040        433080
 jdk.ThreadSleep                          3086         49376
 jdk.ExceptionStatistics                   316          5372
 jdk.ActiveSetting                         251          8681
 jdk.GCPhasePauseLevel2                    221          6922
 jdk.GCPhasePauseLevel1                    192          6077
 jdk.GCPhasePause                           48          1317
 jdk.InitialSystemProperty                  37          1581
 jdk.InitialEnvironmentVariable             37          4181
 jdk.SafepointBegin                         30           450
 jdk.GarbageCollection                      29           639
 jdk.ExecuteVMOperation                     29           532
 jdk.JavaThreadStatistics                    2            26
 jdk.PhysicalMemory                          2            30
 jdk.ThreadStart                             2            26
 jdk.ThreadEnd                               1            11
 jdk.Metadata                                1         68869
 jdk.CheckPoint                              1           925
 jdk.JavaMonitorEnter                        1            26
 jdk.ActiveRecording                         1           142
 jdk.JVMInformation                          1           439
 jdk.OSInformation                           1            64
 jdk.ClassLoadingStatistics                  1            12
 jdk.ThreadPark                              0             0
 jdk.JavaMonitorInflate                      0             0
 jdk.BiasedLockRevocation                    0             0
 jdk.BiasedLockSelfRevocation                0             0
 jdk.BiasedLockClassRevocation               0             0
 jdk.ReservedStackActivation                 0             0
 jdk.ClassLoad                               0             0
 jdk.ClassDefine                             0             0
 jdk.ClassUnload                             0             0
 jdk.IntFlagChanged                          0             0
 jdk.UnsignedIntFlagChanged                  0             0
 jdk.LongFlagChanged                         0             0
 jdk.UnsignedLongFlagChanged                 0             0
 jdk.DoubleFlagChanged                       0             0
 jdk.BooleanFlagChanged                      0             0
 jdk.StringFlagChanged                       0             0
 jdk.GCHeapSummary                           0             0
 jdk.MetaspaceSummary                        0             0
 jdk.MetaspaceGCThreshold                    0             0
 jdk.MetaspaceAllocationFailure              0             0
 jdk.MetaspaceOOM                            0             0
 jdk.MetaspaceChunkFreeListSummary           0             0
 jdk.PSHeapSummary                           0             0
 jdk.G1HeapSummary                           0             0
 jdk.ParallelOldGarbageCollection            0             0
 jdk.YoungGarbageCollection                  0             0
 jdk.OldGarbageCollection                    0             0
 jdk.G1GarbageCollection                     0             0
 jdk.G1MMU                                   0             0
 jdk.EvacuationInformation                   0             0
 jdk.GCReferenceStatistics                   0             0
 jdk.ObjectCountAfterGC                      0             0
 jdk.G1EvacuationYoungStatistics             0             0
 jdk.G1EvacuationOldStatistics               0             0
 jdk.G1BasicIHOP                             0             0
 jdk.G1AdaptiveIHOP                          0             0
 jdk.PromoteObjectInNewPLAB                  0             0
 jdk.PromoteObjectOutsidePLAB                0             0
 jdk.PromotionFailed                         0             0
 jdk.EvacuationFailed                        0             0
 jdk.ConcurrentModeFailure                   0             0
 jdk.GCPhasePauseLevel3                      0             0
 jdk.GCPhasePauseLevel4                      0             0
 jdk.GCPhaseConcurrent                       0             0
 jdk.AllocationRequiringGC                   0             0
 jdk.TenuringDistribution                    0             0
 jdk.G1HeapRegionTypeChange                  0             0
 jdk.Compilation                             0             0
 jdk.CompilerPhase                           0             0
 jdk.CompilationFailure                      0             0
 jdk.CompilerInlining                        0             0
 jdk.SweepCodeCache                          0             0
 jdk.CodeCacheFull                           0             0
 jdk.SafepointStateSynchronization           0             0
 jdk.SafepointWaitBlocked                    0             0
 jdk.SafepointCleanup                        0             0
 jdk.SafepointCleanupTask                    0             0
 jdk.SafepointEnd                            0             0
 jdk.Shutdown                                0             0
 jdk.ObjectAllocationInNewTLAB               0             0
 jdk.ObjectAllocationOutsideTLAB             0             0
 jdk.OldObjectSample                         0             0
 jdk.DumpReason                              0             0
 jdk.DataLoss                                0             0
 jdk.VirtualizationInformation               0             0
 jdk.SystemProcess                           0             0
 jdk.CPUInformation                          0             0
 jdk.CPUTimeStampCounter                     0             0
 jdk.CPULoad                                 0             0
 jdk.ThreadCPULoad                           0             0
 jdk.ThreadContextSwitchRate                 0             0
 jdk.NetworkUtilization                      0             0
 jdk.ClassLoaderStatistics                   0             0
 jdk.ThreadAllocationStatistics              0             0
 jdk.ExecutionSample                         0             0
 jdk.NativeMethodSample                      0             0
 jdk.ThreadDump                              0             0
 jdk.NativeLibrary                           0             0
 jdk.ModuleRequire                           0             0
 jdk.ModuleExport                            0             0
 jdk.CompilerStatistics                      0             0
 jdk.CompilerConfiguration                   0             0
 jdk.CodeCacheStatistics                     0             0
 jdk.CodeCacheConfiguration                  0             0
 jdk.CodeSweeperStatistics                   0             0
 jdk.CodeSweeperConfiguration                0             0
 jdk.IntFlag                                 0             0
 jdk.UnsignedIntFlag                         0             0
 jdk.LongFlag                                0             0
 jdk.UnsignedLongFlag                        0             0
 jdk.DoubleFlag                              0             0
 jdk.BooleanFlag                             0             0
 jdk.StringFlag                              0             0
 jdk.ObjectCount                             0             0
 jdk.G1HeapRegionInformation                 0             0
 jdk.GCConfiguration                         0             0
 jdk.GCSurvivorConfiguration                 0             0
 jdk.GCTLABConfiguration                     0             0
 jdk.GCHeapConfiguration                     0             0
 jdk.YoungGenerationConfiguration            0             0
 jdk.ZPageAllocation                         0             0
 jdk.ZThreadPhase                            0             0
 jdk.ZStatisticsCounter                      0             0
 jdk.ZStatisticsSampler                      0             0
 jdk.ShenandoahHeapRegionStateChange         0             0
 jdk.ShenandoahHeapRegionInformation         0             0
 EveryChunkPeriodEvents                      0             0
 jdk.JavaExceptionThrow                      0             0
 EndChunkPeriodEvents                        0             0
 jdk.JavaErrorThrow                          0             0


---

## Java(Serial GC)でのGCの発生状況の確認結果

|            ページ            |   図    |                    確認できたこと                     |           気になったこと            |
| ---------------------------- | ------- | ----------------------------------------------------- | ----------------------------------- |
| レポートページ               | 図01-10 | 頻繁に割り当てられている箇所についてレポートが出た    | FULL GCについてのコメントはなかった |
| ガベージコレクションのページ | 図01-11 | Nativeとは違って、ヒープの情報が出る。                | コレクタ名が異なる                  |
| ガベージコレクションのページ | 図01-12 | 最長の一時停止でソートすることで、停止時間の傾向      | 特になし                            |
| イベントブラウザページ       | 図01-13 | configurationの部分でヒープサイズとか設定を確認できる | 特になし                            |

<figure><img src='images/221106_182544.png' width='600px'><figcaption>図01-10. レポートページ</figcaption></figure>
<figure><img src='images/221106_181931.png' width='600px'><figcaption>図01-11. ガベージコレクションのページ </figcaption></figure>
<figure><img src='images/221106_182818.png' width='600px'><figcaption>図01-12. ガベージコレクションのページ</figcaption></figure>
<figure><img src='images/221106_183224.png' width='600px'><figcaption>図01-13. イベントブラウザページ</figcaption></figure>
