# Case08の検証結果

時間のかかっているメソッドの特定ができること

## 確認項目

JMCで時間のかかっているメソッドを見れること

## 実施条件

下記サイトのコードをもとにランダム配列をバブルソートで並べ替え
- https://qiita.com/gigegige/items/be079dbc14c62decb329

## Javaでの確認結果
レポートによってバブルソートで実行時間中の大半を占めているというレポートが出ている。
※実行時間は約2分

|              ページ              |   図   |                             確認できたこと                              |
| -------------------------------- | ------ | ----------------------------------------------------------------------- |
| レポートページ                   | 図08-1 | バブルソートメソッドで時間がかかっているとレポートが出ている。          |
| メソッドプロファイリングのページ | 図08-2 | 99.9%以上がバブルソートメソッドでカウントに引っかかっていることが分かる |

<figure><img src='images/221130_225356.png' width='600px'><figcaption>図08-1. レポート</figcaption></figure>

<figure><img src='images/221130_225502.png' width='600px'><figcaption>図08-2. メソッドプロファイリングのページ</figcaption></figure>

## Nativeでの確認結果

native化したクライアントではメソッドプロファイリングのレポートは出ていなかった
※実行時間は約2分でJavaと同程度かかっており、時間が短くなっていて出ていないわけではない。

|              ページ              |   図   |    確認できたこと    |
| -------------------------------- | ------ | -------------------- |
| レポートページ                   | 図08-3 | 特に情報は出ていない |
| メソッドプロファイリングのページ | 図08-4 | 何の情報も出ていない |

<figure><img src='images/221130_230135.png' width='600px'><figcaption>図08-3. レポート</figcaption></figure>

<figure><img src='images/221130_230324.png' width='600px'><figcaption>図08-4. メソッドプロファイリングのページ</figcaption></figure>

 Event Type                              Count  Size (bytes) 
=============================================================
 jdk.JavaMonitorWait                      6516        174172
 jdk.ActiveSetting                         251          8179
 jdk.ExceptionStatistics                   127          2125
 jdk.InitialEnvironmentVariable             38          4455
 jdk.InitialSystemProperty                  37          1581
 jdk.GCPhasePauseLevel2                      9           281
 jdk.GCPhasePauseLevel1                      8           252
 jdk.GCPhasePause                            2            53
 jdk.SafepointBegin                          2            30
 jdk.JavaThreadStatistics                    2            24
 jdk.PhysicalMemory                          2            28
 jdk.ThreadStart                             2            26
 jdk.ThreadEnd                               1            11
 jdk.Metadata                                1         68870
 jdk.CheckPoint                              1           844
 jdk.GarbageCollection                       1            21
 jdk.ExecuteVMOperation                      1            19
 jdk.ActiveRecording                         1           128
 jdk.JVMInformation                          1           282
 jdk.OSInformation                           1            64
 jdk.ClassLoadingStatistics                  1            12