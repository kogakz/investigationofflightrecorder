package com.example;

import java.util.concurrent.Executors;

public class App {

    public static void main(String[] args) throws InterruptedException {
        var es = Executors.newFixedThreadPool(10);
        es.execute(new ThreadPool());
        es.execute(new ThreadPool());
        es.execute(new ThreadPool());
        es.shutdown();
    }

}
