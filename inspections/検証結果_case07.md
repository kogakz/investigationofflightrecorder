# Case07の検証結果

ソケットIOを観測できること

## 確認項目

- JMCでソケットIOの接続先が確認できること
- 読み取り、書き込み量が確認できること
- かかった時間が見れること

## 実施条件

下記サイトのサンプルコードを参考にしてクライアント、サーバ側のアプリケーションを実装
サーバ側でクライアントからのデータ読み込み後に、クライアント返却前にSleepを入れてから、クライアントに返却

- サーバ側のプログラムの元ネタ
  - https://www.techscore.com/tech/Java/JavaSE/Network/2-3/
- クライアント側のプログラムの元ネタ
  - https://www.techscore.com/tech/Java/JavaSE/Network/2-4/

## Javaでの確認結果（クライアントプログラム側）

読み込みについては、サーバからのレスポンス待ちでIO待ちイベントを発生させることができた。
書き込みは発生させられなかった。（ソケットへの書き込み自体は終わってしまうためと想定）

|            ページ            |   図   |                                          確認できたこと                                          |
| ---------------------------- | ------ | ------------------------------------------------------------------------------------------------ |
| レポートページ               | 図07-1 | ソケット読み取りで時間がかかっているという報告が上がっている。書き込みについては上がっていない。 |
| ソケットIOのページ           | 図07-2 | 対象の接続先のIP/ポートがイベントで見えている。                                                  |
| ソケットIOのページ（グラフ） | 図07-3 | 当該時間帯で読み込みしたバイト数なども見れる。                                                   |
| イベントページ（グラフ）     | 図07-4 | 各イベントでの時間や読み込んだサイズが確認できた。                                               |

<figure><img src='images/221130_215613.png' width='600px'><figcaption>図07-1. レポート</figcaption></figure>

<figure><img src='images/221130_215742.png' width='600px'><figcaption>図07-2. ソケットIOのページ</figcaption></figure>

<figure><img src='images/221130_215954.png' width='600px'><figcaption>図07-3. ソケットIOのページ（グラフ）</figcaption></figure>

<figure><img src='images/221130_220430.png' width='600px'><figcaption>図07-4. イベントページ</figcaption></figure>

## Nativeでの確認結果

native化したクライアントでは、読み込みでもイベントを発生しなかった。



|         ページ         |   図   |                     確認できたこと                     |
| ---------------------- | ------ | ------------------------------------------------------ |
| レポートページ         | 図07-5 | Javaブロッキングのレポートが出ている                   |

<figure><img src='images/221130_220648.png' width='600px'><figcaption>図07-5. レポート</figcaption></figure>

`src/jdk.jfr/share/classes/jdk/jfr/events/SocketReadEvent.jav
```java{.line-numbers}
import jdk.jfr.internal.Type;

@Name(Type.EVENT_NAME_PREFIX + "SocketRead")
@Label("Socket Read")
@Category("Java Application")
```

 Event Type                              Count  Size (bytes) 
=============================================================
 jdk.JavaMonitorWait                     10017        268711
 jdk.ActiveSetting                         251          8179
 jdk.ExceptionStatistics                   198          3332
 jdk.InitialEnvironmentVariable             38          4471
 jdk.InitialSystemProperty                  37          1588
 jdk.JavaThreadStatistics                    2            24
 jdk.PhysicalMemory                          2            28
 jdk.ThreadStart                             2            26
 jdk.ThreadEnd                               1            11
 jdk.Metadata                                1         68870
 jdk.CheckPoint                              1           844
 jdk.ActiveRecording                         1           135
 jdk.SafepointBegin                          1            15
 jdk.JVMInformation                          1           289
 jdk.OSInformation                           1            64
 jdk.ClassLoadingStatistics                  1            12