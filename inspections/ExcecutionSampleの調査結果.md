

```java
package com.oracle.svm.core.jfr;

import org.graalvm.compiler.serviceprovider.JavaVersionUtil;
import org.graalvm.nativeimage.CurrentIsolate;
import org.graalvm.nativeimage.IsolateThread;
import org.graalvm.nativeimage.Platform;
import org.graalvm.nativeimage.Platforms;
import org.graalvm.nativeimage.StackValue;
import org.graalvm.word.UnsignedWord;
import org.graalvm.word.WordFactory;

import com.oracle.svm.core.SubstrateUtil;
import com.oracle.svm.core.UnmanagedMemoryUtil;
import com.oracle.svm.core.Uninterruptible;
import com.oracle.svm.core.jfr.events.ExecutionSampleEvent;
import com.oracle.svm.core.jfr.events.ThreadEndEvent;
import com.oracle.svm.core.jfr.events.ThreadStartEvent;
import com.oracle.svm.core.thread.JavaThreads;
import com.oracle.svm.core.thread.Target_java_lang_Thread;
import com.oracle.svm.core.thread.ThreadListener;
import com.oracle.svm.core.thread.VMOperation;
import com.oracle.svm.core.threadlocal.FastThreadLocalFactory;
import com.oracle.svm.core.threadlocal.FastThreadLocalLong;
import com.oracle.svm.core.threadlocal.FastThreadLocalObject;
import com.oracle.svm.core.threadlocal.FastThreadLocalWord;
import com.oracle.svm.core.util.VMError;

/**
 * This class holds various JFR-specific thread local values.
 *
 * Each thread uses both a Java and a native {@link JfrBuffer}:
 * <ul>
 * <li>The Java buffer is accessed by JFR events that are implemented as Java classes and written
 * using {@code EventWriter}.</li>
 * <li>The native buffer is accessed when {@link JfrNativeEventWriter} is used to write an
 * event.</li>
 * </ul>
 *
 * It is necessary to have separate buffers as a native JFR event (e.g., a GC or an allocation)
 * could otherwise destroy an Java-level JFR event. All methods that access a {@link JfrBuffer} must
 * be uninterruptible to avoid races with JFR code that is executed at a safepoint (such code may
 * modify the buffers of other threads).
 */
public class JfrThreadLocal implements ThreadListener {
    private static final FastThreadLocalObject<Target_jdk_jfr_internal_EventWriter> javaEventWriter = FastThreadLocalFactory.createObject(Target_jdk_jfr_internal_EventWriter.class,
                    "JfrThreadLocal.javaEventWriter");
    private static final FastThreadLocalWord<JfrBuffer> javaBuffer = FastThreadLocalFactory.createWord("JfrThreadLocal.javaBuffer");
    private static final FastThreadLocalWord<JfrBuffer> nativeBuffer = FastThreadLocalFactory.createWord("JfrThreadLocal.nativeBuffer");
    private static final FastThreadLocalWord<UnsignedWord> dataLost = FastThreadLocalFactory.createWord("JfrThreadLocal.dataLost");
    private static final FastThreadLocalLong threadId = FastThreadLocalFactory.createLong("JfrThreadLocal.threadId");
    private static final FastThreadLocalLong parentThreadId = FastThreadLocalFactory.createLong("JfrThreadLocal.parentThreadId");

    private long threadLocalBufferSize;

    @Platforms(Platform.HOSTED_ONLY.class)
    public JfrThreadLocal() {
    }

    public void initialize(long bufferSize) {
        this.threadLocalBufferSize = bufferSize;
    }

    @Uninterruptible(reason = "Accesses a JFR buffer.")
    @Override
    public void beforeThreadRun(IsolateThread isolateThread, Thread javaThread) {
        // We copy the thread id to a thread-local in the IsolateThread. This is necessary so that
        // we are always able to access that value without having to go through a heap-allocated
        // Java object.
        Target_java_lang_Thread t = SubstrateUtil.cast(javaThread, Target_java_lang_Thread.class);
        threadId.set(isolateThread, t.getId());
        parentThreadId.set(isolateThread, JavaThreads.getParentThreadId(javaThread));

        SubstrateJVM.getThreadRepo().registerThread(javaThread);

        // Emit ThreadStart event before thread.run().
        ThreadStartEvent.emit(isolateThread);

        // Register ExecutionSampleEvent after ThreadStart event and before thread.run().
        ExecutionSampleEvent.tryToRegisterExecutionSampleEventCallback();
    }
```

```java
package com.oracle.svm.core.jfr.events;

import java.util.concurrent.TimeUnit;

import org.graalvm.nativeimage.CurrentIsolate;
import org.graalvm.nativeimage.ImageSingletons;
import org.graalvm.nativeimage.IsolateThread;
import org.graalvm.nativeimage.StackValue;
import org.graalvm.nativeimage.Threading;
import org.graalvm.nativeimage.impl.ThreadingSupport;

import com.oracle.svm.core.Uninterruptible;
import com.oracle.svm.core.jfr.JfrEvent;
import com.oracle.svm.core.jfr.JfrNativeEventWriter;
import com.oracle.svm.core.jfr.JfrNativeEventWriterData;
import com.oracle.svm.core.jfr.JfrNativeEventWriterDataAccess;
import com.oracle.svm.core.jfr.JfrThreadState;
import com.oracle.svm.core.jfr.JfrTicks;
import com.oracle.svm.core.jfr.SubstrateJVM;
import com.oracle.svm.core.thread.PlatformThreads;

public final class ExecutionSampleEvent {

    private static long intervalMillis;
    private static final ExecutionSampleEventCallback CALLBACK = new ExecutionSampleEventCallback();

    @Uninterruptible(reason = "Called from uninterruptible code.", calleeMustBe = false)
    public static void tryToRegisterExecutionSampleEventCallback() {
        if (intervalMillis > 0) {
            ImageSingletons.lookup(ThreadingSupport.class).registerRecurringCallback(intervalMillis, TimeUnit.MILLISECONDS, CALLBACK);
        }
    }

    public static void setSamplingInterval(long intervalMillis) {
        ExecutionSampleEvent.intervalMillis = intervalMillis;
    }

    //書き込みメソッド
    // ExecutionSampleEventCallback.runから呼び出される
    @Uninterruptible(reason = "Accesses a JFR buffer.")
    public static void writeExecutionSample(long elapsedTicks, long threadId, long stackTraceId, long threadState) {
        SubstrateJVM svm = SubstrateJVM.get();
        if (SubstrateJVM.isRecording() && svm.isEnabled(JfrEvent.ExecutionSample)) {
            JfrNativeEventWriterData data = StackValue.get(JfrNativeEventWriterData.class);
            JfrNativeEventWriterDataAccess.initializeThreadLocalNativeBuffer(data);

            JfrNativeEventWriter.beginSmallEvent(data, JfrEvent.ExecutionSample);
            JfrNativeEventWriter.putLong(data, elapsedTicks);
            JfrNativeEventWriter.putLong(data, threadId);
            JfrNativeEventWriter.putLong(data, stackTraceId);
            JfrNativeEventWriter.putLong(data, threadState);
            JfrNativeEventWriter.endSmallEvent(data);
        }
    }

    private static final class ExecutionSampleEventCallback implements Threading.RecurringCallback {
        @Override
        public void run(Threading.RecurringCallbackAccess access) {
            IsolateThread isolateThread = CurrentIsolate.getCurrentThread();
            Thread javaThread = PlatformThreads.fromVMThread(isolateThread);
            ExecutionSampleEvent.writeExecutionSample(
                            JfrTicks.elapsedTicks(),
                            SubstrateJVM.getThreadId(isolateThread),
                            SubstrateJVM.get().getStackTraceId(JfrEvent.ExecutionSample, 0),
                            JfrThreadState.getId(javaThread.getState()));
        }
    }
}
```