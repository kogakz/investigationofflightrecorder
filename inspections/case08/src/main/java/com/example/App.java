package com.example;

import java.util.concurrent.Executors;
import java.util.Random;


// 
// https://qiita.com/gigegige/items/be079dbc14c62decb329

public class App {

    static void bubble_sort(int[] d) {
        // iはi回目の交換する回数
        for (int i = d.length-1; i > 0; i-- )   {

            // jは交換する箇所の前からの番号を表している。
            for (int j = 0; j < i; j++) {

                if(d[j]>d[j+1]){
                  //降順にしたい場合は不等号を逆に
                  int box = d[j];
                  d[j] = d[j+1];
                  d[j+1] = box;

                  //System.out.println(d[j] + ":" +d[j+1]);

                } else{
                  //そのまま
                }
            }
        }
    }
    static void print_data(int[] d) {
        for(int i = 0; i < d.length; i++) 
            System.out.println(d[i] + " ");
    }
    public static void main(String[] args) {

        Long seed = System.currentTimeMillis();
        System.out.println("seed is " + seed);
        Random random = new Random(seed);
        
        int[] data = new int[300000];

        for(int i=0; i<data.length; i++){
            data[i] = random.nextInt(data.length);
        }
        
        print_data(data);
        bubble_sort(data);
        print_data(data);
    }
}
