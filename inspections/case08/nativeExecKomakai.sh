#!/bin/bash
./target/app08 \
-XX:+FlightRecorder \
-XX:StartFlightRecording="settings=jfc/komakai.jfc,duration=6m,name=native-startup,dumponexit=true,filename=jfr/native/native-komakai.jfr" \
-XX:+PrintGC -XX:+VerboseGC \
-Xms400m -Xmx400m 2> log/native/gc.log
