package com.example;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;

/**
 * 約300MBのヒープを確保する
 *
 */
public class App {
    // https://atmarkit.itmedia.co.jp/ait/articles/0501/25/news127.html
    public static void printDigest(byte[] digest) {//ダイジェストを16進数で表示する
        for (int i = 0; i < digest.length; i++) {
            int d = digest[i];
            if (d < 0) {//byte型では128～255が負値になっているので補正
                d += 256;
            }
            if (d < 16) {//0～15は16進数で1けたになるので、2けたになるよう頭に0を追加
                System.out.print("0");
            }
            System.out.print(Integer.toString(d, 16));//ダイジェスト値の1バイトを16進数2けたで表示
        }
        System.out.println();
    }

    public static void main(String[] args) throws InterruptedException, NoSuchAlgorithmException {

        Long seed = System.currentTimeMillis();
        System.out.println("seed is " + seed);

        while ( ( System.currentTimeMillis() -seed ) < 600000) {
            


            ArrayList<byte[]> list = new ArrayList<>();
            Random random = new Random(seed);

            for (int index = 0; index < 300; index++) {
            
                Thread.sleep(100);
                byte[] buf = new byte[1000000];
                random.nextBytes(buf);

                list.add(buf);
            }

            System.out.println("List Size = " + list.size());

            byte[] data = list.get(random.nextInt(list.size()));
            MessageDigest sha3_256 = MessageDigest.getInstance("SHA3-256");

            printDigest(sha3_256.digest(data));
        }

    }

}
