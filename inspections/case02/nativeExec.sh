#!/bin/bash
# ヒープサイズを 200MB
./target/app \
-XX:+FlightRecorder \
-XX:StartFlightRecording="settings=jfc/profile.jfc,duration=6m,name=native-startup,dumponexit=true,filename=jfr/native/native-start.jfr" \
-XX:StartFlightRecording="settings=jfc/profile.jfc,delay=5m,maxage=10m,name=native-post-startup,dumponexit=true,filename=./jfr/native/native-post-startup.jfr" \
-XX:+PrintGC -XX:+VerboseGC \
-Xms400m -Xmx400m 2> log/native/gc.log
