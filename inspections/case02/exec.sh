#!/bin/bash
# ヒープサイズを 200MB
java \
-Dcom.sun.management.jmxremote=true \
-Djava.rmi.server.hostname=127.0.0.1 \
-Dcom.sun.management.jmxremote.port=7091 \
-Dcom.sun.management.jmxremote.authenticate=false \
-Dcom.sun.management.jmxremote.ssl=false \
-Dcom.sun.management.jmxremote.registry.ssl=false \
-Djava.net.preferIPv4Stack=true \
-XX:StartFlightRecording=settings=profile,duration=6m,name=app-startup,dumponexit=true,filename=./jfr/java/app-startup.jfr \
-XX:StartFlightRecording=settings=profile,delay=5m,maxage=10m,name=post-startup,dumponexit=true,filename=./jfr/java/post-startup.jfr \
-Xlog:gc:log/java/gc.log:time,uptimemillis:filecount=5,filesize=3M \
-XX:TLABSize=512k \
-Xms400m -Xmx400m \
-jar target/case02-1.0-SNAPSHOT.jar
