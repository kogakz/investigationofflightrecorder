# Case06の検証結果

## 確認項目

- JMCでファイルのパスを確認できること
- かかった時間が見れること
- jdk.FileReadイベントで、ファイルからの読み取り量を確認できること（おそらく0と書いてあるので取れないかも）
- jdk.FileWriteイベントで、ファイルへの書き込み量を確認できること

## 実施条件

- 閾値を下げる
- 大量データに読み書きさせる

## Javaでの確認結果

設定の閾値も下げることで、一部のファイルでIO情報を見ることができた。

| ページ         | 図     | 確認できたこと                                                   |
| -------------- | ------ | ---------------------------------------------------------------- |
| レポートページ | 図06-1 | レポートには特に出ていない。                                     |
| ファイルIO     | 図06-2 | 書き込みに時間がかかったファイル名、IO時間のイベントが見れている |


<figure><img src='images/221130_231406.png' width='600px'><figcaption>図06-1. レポート</figcaption></figure>

<figure><img src='images/221130_231513.png' width='600px'><figcaption>図06-2. ファイルIO</figcaption></figure>

## Nativeでの確認結果

Native化したアプリではフライトレコーダーでのイベントを見ることができなかった。

| ページ         | 図     | 確認できたこと                   |
| -------------- | ------ | -------------------------------- |
| レポートページ | 図06-3 | レポートには特に出ていない。     |
| ファイルIO     | 図06-4 | ファイルIOのページにも出ていない |

<figure><img src='images/221130_231904.png' width='600px'><figcaption>図06-3. レポート</figcaption></figure>

<figure><img src='images/221130_231935.png' width='600px'><figcaption>図06-4. ファイルIO</figcaption></figure>

 Event Type                              Count  Size (bytes) 
=============================================================
 jdk.ExecutionSample                      2956         32149
 jdk.ActiveSetting                         251          8179
 jdk.JavaMonitorWait                       114          2951
 jdk.InitialEnvironmentVariable             38          4182
 jdk.InitialSystemProperty                  37          1544
 jdk.ThreadStart                            22           243
 jdk.ThreadEnd                              21           209
 jdk.JavaThreadStatistics                    2            23
 jdk.PhysicalMemory                          2            27
 jdk.ExceptionStatistics                     2            32
 jdk.Metadata                                1         68868
 jdk.CheckPoint                              1          1706
 jdk.JavaMonitorEnter                        1            24
 jdk.ActiveRecording                         1           128
 jdk.SafepointBegin                          1            14
 jdk.JVMInformation                          1           280
 jdk.OSInformation                           1            63
 jdk.ClassLoadingStatistics                  1            11