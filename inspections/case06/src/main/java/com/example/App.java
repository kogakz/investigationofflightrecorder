package com.example;

import java.util.concurrent.Executors;

/**
 *  Case06
 *  確認項目
 *    JMCでファイルのパスを確認できること
 *      かかった時間が見れること
 *         jdk.FileReadイベントで、ファイルからの読み取り量を確認できること（おそらく0と書いてあるので取れないかも）
 *         jdk.FileWriteイベントで、ファイルへの書き込み量を確認できること
 *   実施条件
 *     閾値を下げる
 *     大量データに読み書きさせる
 *     NFSか何かでわざと帯域を絞って発生させる？
 */
public class App {

    public static void main(String[] args) throws InterruptedException {
        var es = Executors.newFixedThreadPool(50);
        for(int i=0; i<40; i++){
            es.execute(new ThreadPool());
        }
        es.shutdown();
    }

}
