package com.example;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ThreadPool implements Runnable {

  @Override
  public void run() {
    File file = new File("./out/write" + Thread.currentThread().getName() + ".txt");

    String paddingStr = String.format("%1000s", "b");

    try {
      
      FileWriter filewriter = new FileWriter(file);
      for (int i = 0; i < 10000; i++) {
        filewriter.write(paddingStr);
        filewriter.flush();
      }
      filewriter.close();

    } catch (IOException e) {
      System.out.println(e);
    }
  }

}
