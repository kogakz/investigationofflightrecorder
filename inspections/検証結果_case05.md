# Case05の検証結果

## 確認項目

- JMCでロック待ちスレッドを見れること
- jdk.JavaMonitorWaitイベントを見れること


## 実施条件

```java{.line-numbers}
package com.example;

 public class Counter  {

    private static int count = 0;

    public synchronized void countUp() {

      for (int i = 0; i < 3000; i++) {
        System.out.println(Thread.currentThread().getName() + ":" + count);
        count++;
      }
    }
  }

```

```java{.line-numbers}
package com.example;

public class ThreadPool implements Runnable {

  private static Counter lock = new Counter();

  //private static int count = 0;

  @Override
  public void run() {

    lock.countUp();

  }

}
```


```java{.line-numbers}
package com.example;

import java.util.concurrent.Executors;

public class App {

    public static void main(String[] args) throws InterruptedException {
        var es = Executors.newFixedThreadPool(10);
        es.execute(new ThreadPool());
        es.execute(new ThreadPool());
        es.execute(new ThreadPool());
        es.shutdown();
    }

}

```

## Javaでの確認結果

サマリページでの警告～イベントの発生状況、発生時の詳細情報をJMCで確認することができた。

|         ページ         |   図   |                     確認できたこと                     |
| ---------------------- | ------ | ------------------------------------------------------ |
| レポートページ         | 図05-1 | Javaブロッキングのレポートが出ている                   |
| ブロックインスタンス   | 図05-2 | ブロック原因のインスタンス、ブロックされているスレッド |
| スレッドページ         | 図05-3 | スレッド2/3がブロックされている状況                    |
| イベントブラウザページ | 図05-4 | ブロックイベントを確認できる                           |

<figure><img src='images/221118_010907.png' width='600px'><figcaption>図05-1. レポート</figcaption></figure>

<figure><img src='images/221118_011054.png' width='600px'><figcaption>図05-2. ブロックインスタンス</figcaption></figure>

<figure><img src='images/221118_011342.png' width='600px'><figcaption>図05-3. スレッド</figcaption></figure>

<figure><img src='images/221203_072757.png' width='600px'><figcaption>図05-4. イベントブラウザページ</figcaption></figure>

## Nativeでの確認結果

javaでのコードと同様に見ることができた。

|         ページ         |   図   |                     確認できたこと                     |
| ---------------------- | ------ | ------------------------------------------------------ |
| レポートページ         | 図05-5 | Javaブロッキングのレポートが出ている                   |
| ブロックインスタンス   | 図05-6 | ブロック原因のインスタンス、ブロックされているスレッド |
| スレッド               | 図05-7 | スレッド1,2,3がブロックされている状況                  |
| イベントブラウザページ | 図05-8 | ブロックイベント                                       |


<figure><img src='images/221203_071807.png' width='600px'><figcaption>図05-5. レポート</figcaption></figure>

<figure><img src='images/221203_071919.png' width='600px'><figcaption>図05-6. ブロックインスタンス</figcaption></figure>

<figure><img src='images/221203_072059.png' width='600px'><figcaption>図05-7. スレッド</figcaption></figure>

<figure><img src='images/221203_072028.png' width='600px'><figcaption>図05-8. イベントブラウザページ</figcaption></figure>

 Event Type                              Count  Size (bytes) 
=============================================================
 jdk.ActiveSetting                         251          8179
 jdk.InitialEnvironmentVariable             38          4414
 jdk.InitialSystemProperty                  37          1544
 jdk.ExecutionSample                        26           262
 jdk.JavaMonitorWait                        19           478
 jdk.ThreadStart                             5            56
 jdk.ThreadEnd                               4            37
 jdk.JavaMonitorEnter                        2            48
 jdk.JavaThreadStatistics                    2            23
 jdk.PhysicalMemory                          2            27
 jdk.Metadata                                1         68868
 jdk.CheckPoint                              1          1241
 jdk.ActiveRecording                         1           128
 jdk.SafepointBegin                          1            14
 jdk.JVMInformation                          1           281
 jdk.OSInformation                           1            63
 jdk.ClassLoadingStatistics                  1            11


# おまけ（ロックをかけずに多重書き出ししてみたら）


```java{.line-numbers}
package com.example;

public class ThreadPool implements Runnable {

  private static Counter lock = new Counter();

  private static int count = 0;

  @Override
  public void run() {

    for (int i = 0; i < 30000; i++) {
      System.out.println(Thread.currentThread().getName() + ":" + count);
      count++;
    }

  }
}
```

## Javaでの確認結果

java.io.PrintStreamでブロッキングが発生しているが、スレッド全体ががっつり止まることはない。


|          ページ          |   図    |                                  確認できたこと                                  |
| ------------------------ | ------- | -------------------------------------------------------------------------------- |
| レポートページ           | 図05-9  | Javaブロッキングのレポートが出ている                                             |
| ロックインスタンス       | 図05-10 | ブロック原因のインスタンス、ブロックされているスレッド                           |
| スレッドページ(全体表示) | 図05-11 | ちょこちょこブロックされている様子が見えるが、完全にスレッド間で排他ではないよう |
| スレッドページ(拡大表示) | 図05-12 | 拡大してみても同じ感じ                                                           |
| イベントブラウザページ   | 図05-13 | 特に詳細を見ていない                                                             |

<figure><img src='images/221203_102410.png' width='600px'><figcaption>図05-9. ロックなしでやった時のレポート</figcaption></figure>

<figure><img src='images/221203_102505.png' width='600px'><figcaption>図05-10. ロック・インスタンス</figcaption></figure>

<figure><img src='images/221203_102605.png' width='600px'><figcaption>図05-11. スレッドページ(全体表示）</figcaption></figure>

<figure><img src='images/221203_102845.png' width='600px'><figcaption>図05-12. スレッドページ(拡大表示）</figcaption></figure>

<figure><img src='images/221203_103049.png' width='600px'><figcaption>図05-13. イベントブラウザページ</figcaption></figure>

## Nativeでの確認結果

java.io.PrintStreamでブロッキングが発生するのは同じだが、ブロックのされ方が違う。
ほとんどIOのブロック待ちになっっている。
実行時間も長くなってる。


|          ページ          |   図    |                            確認できたこと                             |
| ------------------------ | ------- | --------------------------------------------------------------------- |
| レポートページ           | 図05-14 | Javaブロッキングのレポートが出ている                                  |
| ロックインスタンス       | 図05-15 | 実行時間6.95秒のうちほとんどがブロック状態（2.31＋2.20＋2.43=6.94秒） |
| スレッドページ(全体表示) | 図05-16 | ブロック状態がほとんど                                                |
| スレッドページ(拡大表示) | 図05-17 | 拡大してみると、ちょこちょことスレッドが順番に動いている              |
| イベントブラウザページ   | 図05-18 | 特に詳細を見ていない                                                  |




<figure><img src='images/221203_102301.png' width='600px'><figcaption>図05-14. レポートページ</figcaption></figure>

<figure><img src='images/221203_103818.png' width='600px'><figcaption>図05-15. ロックインスタンス</figcaption></figure>

<figure><img src='images/221203_103328.png' width='600px'><figcaption>図05-16. スレッドページ(全体表示)</figcaption></figure>

<figure><img src='images/221203_104603.png' width='600px'><figcaption>図05-17. スレッドページ(拡大表示) </figcaption></figure>

<figure><img src='images/221203_104759.png' width='600px'><figcaption>図05-18. イベントブラウザページ</figcaption></figure>