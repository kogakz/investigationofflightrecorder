#!/bin/bash
# ヒープサイズを 200MB
java \
-XX:StartFlightRecording=settings=jfc/detail.jfc,duration=6m,name=app-startup,dumponexit=true,filename=./jfr/java/app-startup.jfr \
-XX:StartFlightRecording=settings=jfc/detail.jfc,delay=5m,maxage=10m,name=post-startup,dumponexit=true,filename=./jfr/java/post-startup.jfr \
-Xlog:gc:log/java/gc.log:time,uptimemillis:filecount=5,filesize=3M \
-XX:TLABSize=512k \
-Xms400m -Xmx400m \
-jar target/EchoClient-1.0-SNAPSHOT.jar 
