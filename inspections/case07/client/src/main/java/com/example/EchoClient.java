package com.example;

import java.net.Socket;
import java.net.ServerSocket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.IOException;

/**
 * case07 ソケットIOを観測できること
 *   `確認項目`
 *      JMCでソケットIOの接続先が確認できること
 *      読み取り、書き込み量が確認できること
 *      かかった時間が見れること
 *    `実施条件`
 *      簡易的なネットワーク通信を行うプログラムで測定する
 *      閾値を下げて収集、またはSleepするようなものサーバプロセスを用意する
 *  下記サイトのサンプルコードを参考にしてます。   
 *  https://www.techscore.com/tech/Java/JavaSE/Network/2-4/
 */
public class EchoClient {

  public static final int ECHO_PORT = 10007;
 
  public static void main(String args[]) {
     Socket socket = null;
     try {
       socket = new Socket("127.0.0.1", ECHO_PORT);
       System.out.println("接続しました"
                          + socket.getRemoteSocketAddress());
       BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
       PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
       BufferedReader keyIn = new BufferedReader(new InputStreamReader(System.in));
       String input;

      for(int i=1; i< 101; i++){
        out.println( i + "回目の送信");
        System.out.println("データを" + i + "回送信しました");
        String line = in.readLine();
        if (line != null) {
           System.out.println(line);
        } else {
           break;
        }
      } 

/*
       while ( (input = keyIn.readLine()).length() > 0 ) {
         out.println(input);
         String line = in.readLine();
         if (line != null) {
           System.out.println(line);
         } else {
           break;
         }
       }
       */
     } catch (IOException e) {
       e.printStackTrace();
     } finally {
       try {
         if (socket != null) {
           socket.close();
         }
       } catch (IOException e) {}
       System.out.println("切断されました "
                          + socket.getRemoteSocketAddress());
     }
   }
 
}