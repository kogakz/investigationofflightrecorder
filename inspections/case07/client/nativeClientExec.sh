#!/bin/bash
./target/EchoClient \
-XX:+FlightRecorder \
-XX:StartFlightRecording="settings=jfc/detail.jfc,duration=6m,name=native-startup,dumponexit=true,filename=jfr/native/native-start.jfr" \
-XX:+PrintGC -XX:+VerboseGC \
-Xms400m -Xmx400m 2> log/native/gc.log
