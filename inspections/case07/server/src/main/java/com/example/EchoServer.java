package com.example;

import java.net.Socket;
import java.net.ServerSocket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.IOException;

/**
 * case07 ソケットIOを観測できること
 *   `確認項目`
 *      JMCでソケットIOの接続先が確認できること
 *      読み取り、書き込み量が確認できること
 *      かかった時間が見れること
 *    `実施条件`
 *      簡易的なネットワーク通信を行うプログラムで測定する
 *      閾値を下げて収集、またはSleepするようなものサーバプロセスを用意する
 *  下記サイトのサンプルコードを参考にしてます。   
 *  https://www.techscore.com/tech/Java/JavaSE/Network/2-3/
 */
public class EchoServer {

  public static final int ECHO_PORT = 10007;

  public static void main(String args[]) throws InterruptedException{
    ServerSocket serverSocket = null;
    Socket socket = null;
    try {
      int sleep_value = 10000;
      if(args.length>0){
        sleep_value = Integer.parseInt(args[0]);
      }

      serverSocket = new ServerSocket(ECHO_PORT);
      System.out.println("EchoServerが起動しました(port="
          + serverSocket.getLocalPort() + ")");
      socket = serverSocket.accept();
      System.out.println("接続されました "
          + socket.getRemoteSocketAddress());
      BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
      String line;
      while ((line = in.readLine()) != null) {
        System.out.println("受信: " + line);
        
        //スリープしてから返却
        Thread.sleep(sleep_value);
        out.println(line);

        System.out.println("送信: " + line);

        //スリープしてから次の読み込みを開始
        Thread.sleep(sleep_value);

      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        if (socket != null) {
          socket.close();
        }
      } catch (IOException e) {
      }
      try {
        if (serverSocket != null) {
          serverSocket.close();
        }
      } catch (IOException e) {
      }
    }
  }

}
