# Case02の検証結果

## 確認項目

jdk.ObjectAllocationOutsideTLABイベントを見れること

## 実施条件

TLAB<アプリケーションで取得するバッファサイズで動かす。
具体的条件としては、以下とした。

- TLABサイズを512KBに指定
  - `$java -XX:TLABSize=512k`
- アプリケーションで取得するバッファサイズを約1MBとした
  - 観測しやすいよう`byte[] buf = new byte[1000000];`をループで回した

## Javaでの確認結果

サマリページでの警告～イベントの発生状況、発生時の詳細情報をJMCで確認することができた。

|              ページ              |   図   |                              確認できたこと                              |
| -------------------------------- | ------ | ------------------------------------------------------------------------ |
| レポートページ                   | 図02-1 | メモリの76.2%をTLAB外に割り当てていることを警告していること              |
| TALB割り当てページのメソッドタブ | 図02-2 | mainメソッド内でほとんど行われていること                                 |
| TALB割り当てページのクラスタブ   | 図02-3 | `byte[]`の割り当て発生していること                                       |
| イベントブラウザページ           | 図02-4 | Allocation outsde TLABの各イベントの情報から、割り当てられたメモリサイズ |


<figure><img src='images/221105_155444.png' width='600px'><figcaption>図02-1. レポート図</figcaption></figure>

<figure><img src='images/221105_160129.png' width='600px'><figcaption>図02-2. TALB外の割り当てがmainスレッド</figcaption></figure>

<figure><img src='images/221105_160227.png' width='600px'><figcaption>図02-3. メソッドタブ</figcaption></figure>

<figure><img src='images/221105_160440.png' width='600px'><figcaption>図02-3. クラスタブ</figcaption></figure>

<figure><img src='images/221105_160931.png' width='600px'><figcaption>図02-4. イベント・ブラウザページ</figcaption></figure>

## Nativeでの確認結果

TLABのサイズ指定方法が不明のためデフォルトで実施してみた



|         ページ         |   図   |                  確認できたこと                  |
| ---------------------- | ------ | ------------------------------------------------ |
| レポートページ         | 図02-5 | TLABの警告なし                                   |
| TALB割り当てページ     | 図02-6 | 特になんのイベントも出ていない                   |
| イベントブラウザページ | 図02-7 | Allocation outsde TLABのイベントは出ていないこと |

<figure><img src='images/221107_235217.png' width='600px'><figcaption>図02-5. レポート図</figcaption></figure>
<figure><img src='images/221107_235249.png' width='600px'><figcaption>図02-6. TLAB割り当てページ</figcaption></figure>
<figure><img src='images/221107_235342.png' width='600px'><figcaption>図02-7. イベント・ブラウザページ</figcaption></figure>


 Event Type                              Count  Size (bytes) 
=============================================================
 jdk.JavaMonitorWait                     16078        434106
 jdk.ThreadSleep                          3085         49360
 jdk.ExceptionStatistics                   315          5355
 jdk.ActiveSetting                         251          8681
 jdk.GCPhasePauseLevel2                    221          6922
 jdk.GCPhasePauseLevel1                    192          6077
 jdk.GCPhasePause                           48          1317
 jdk.InitialEnvironmentVariable             38          4218
 jdk.InitialSystemProperty                  37          1581
 jdk.SafepointBegin                         30           450
 jdk.GarbageCollection                      29           639
 jdk.ExecuteVMOperation                     29           532
 jdk.JavaThreadStatistics                    2            26
 jdk.PhysicalMemory                          2            30
 jdk.ThreadStart                             2            26
 jdk.ThreadEnd                               1            11
 jdk.Metadata                                1         68869
 jdk.CheckPoint                              1           925
 jdk.JavaMonitorEnter                        1            26
 jdk.ActiveRecording                         1           142
 jdk.JVMInformation                          1           439
 jdk.OSInformation                           1            64
 jdk.ClassLoadingStatistics                  1            12
