

GCImpl抜粋

```PlantUML
activate GCImpl
GCImpl->GCImpl: ccollectImpl
activate GCImpl
GCImpl->GCImpl: doCollectImpl
activate GCImpl
GCImpl->GCImpl :doCollectOnce
activate GCImpl


```

```Java
private boolean collectImpl(GCCause cause, long requestingNanoTime, boolean forceFullGC) {
    boolean outOfMemory;
    precondition();

    NoAllocationVerifier nav = noAllocationVerifier.open();
    try {
        //開始時間を記録
        long startTicks = JfrTicks.elapsedTicks();
        try {
            // GCの実装？呼び出し
            outOfMemory = doCollectImpl(cause, requestingNanoTime, forceFullGC, false);
            
            if (outOfMemory) {
                // Avoid running out of memory with a full GC that reclaims softly reachable
                // objects
                ReferenceObjectProcessing.setSoftReferencesAreWeak(true);
                try {
                    // outOfMemoryが帰ってきたら、forceFullGC,forceNoIncremental をtrueにして再度GCを行う
                    outOfMemory = doCollectImpl(cause, requestingNanoTime, true, true);
                } finally {
                    ReferenceObjectProcessing.setSoftReferencesAreWeak(false);
                }
            }
        } finally {
            // GCイベントを書き出し
            JfrGCEvents.emitGarbageCollectionEvent(getCollectionEpoch(), cause, startTicks);
        }
    } finally {
        nav.close();
    }

    postcondition();
    return outOfMemory;
}

//collectImplから呼び出される
// フラグによって doCollectOnce の呼び出し方を変える
//  
private boolean doCollectImpl(GCCause cause, long requestingNanoTime, boolean forceFullGC, boolean forceNoIncremental) {
    CommittedMemoryProvider.get().beforeGarbageCollection();

    boolean incremental = !forceNoIncremental && !policy.shouldCollectCompletely(false);
    boolean outOfMemory = false;

    if (incremental) {
        long startTicks = JfrGCEvents.startGCPhasePause();
        try {
            // メインのGC処理？（doCollectImpl）の呼び出し complete を falseで
            outOfMemory = doCollectOnce(cause, requestingNanoTime, false, false);
        } finally {
            
            JfrGCEvents.emitGCPhasePauseEvent(getCollectionEpoch(), "Incremental GC", startTicks);
        }
    }
    if (!incremental || outOfMemory || forceFullGC || policy.shouldCollectCompletely(incremental)) {
        if (incremental) { // uncommit unaligned chunks
            CommittedMemoryProvider.get().afterGarbageCollection();
        }
        long startTicks = JfrGCEvents.startGCPhasePause();
        try {
            // メインのGC処理？（doCollectImpl）の呼び出し complete を tureで
            outOfMemory = doCollectOnce(cause, requestingNanoTime, true, incremental);
        } finally {
            JfrGCEvents.emitGCPhasePauseEvent(getCollectionEpoch(), "Full GC", startTicks);
        }
    }

    HeapImpl.getChunkProvider().freeExcessAlignedChunks();
    CommittedMemoryProvider.get().afterGarbageCollection();
    return outOfMemory;
}

// doCollectImplから呼び出される
// complete を true  ：
//             false  : 
private boolean doCollectOnce(GCCause cause, long requestingNanoTime, boolean complete, boolean followsIncremental) {
    assert !followsIncremental || complete : "An incremental collection cannot be followed by another incremental collection";
    completeCollection = complete;

    // GC前のサイズ？
    accounting.beforeCollection(completeCollection);
    policy.onCollectionBegin(completeCollection, requestingNanoTime);

    Timer collectionTimer = timers.collection.open();
    try {
        if (!followsIncremental) { // we would have verified the heap after the incremental GC
            verifyBeforeGC();
        }
        scavenge(!complete);
        verifyAfterGC();
    } finally {
        collectionTimer.close();
    }

    // GCの実施？
    HeapImpl.getHeapImpl().getAccounting().setEdenAndYoungGenBytes(WordFactory.zero(), accounting.getYoungChunkBytesAfter());

    // GC後のサイズ？
    accounting.afterCollection(completeCollection, collectionTimer);
    policy.onCollectionEnd(completeCollection, cause);

    UnsignedWord usedBytes = getChunkBytes();
    UnsignedWord freeBytes = policy.getCurrentHeapCapacity().subtract(usedBytes);
    ReferenceObjectProcessing.afterCollection(freeBytes);

    return usedBytes.aboveThan(policy.getMaximumHeapSize()); // out of memory?
}

// GC Logの（GC前）出力箇所
private void printGCBefore(String cause) {
    Log verboseGCLog = Log.log();
    HeapImpl heap = HeapImpl.getHeapImpl();
    sizeBefore = ((SubstrateGCOptions.PrintGC.getValue() || SerialGCOptions.PrintHeapShape.getValue()) ? getChunkBytes() : WordFactory.zero());
    if (SubstrateGCOptions.VerboseGC.getValue() && getCollectionEpoch().equal(1)) {
        verboseGCLog.string("[Heap policy parameters: ").newline();
        verboseGCLog.string("  YoungGenerationSize: ").unsigned(getPolicy().getMaximumYoungGenerationSize()).newline();
        verboseGCLog.string("      MaximumHeapSize: ").unsigned(getPolicy().getMaximumHeapSize()).newline();
        verboseGCLog.string("      MinimumHeapSize: ").unsigned(getPolicy().getMinimumHeapSize()).newline();
        verboseGCLog.string("     AlignedChunkSize: ").unsigned(HeapParameters.getAlignedHeapChunkSize()).newline();
        verboseGCLog.string("  LargeArrayThreshold: ").unsigned(HeapParameters.getLargeArrayThreshold()).string("]").newline();
        if (SerialGCOptions.PrintHeapShape.getValue()) {
            HeapImpl.getHeapImpl().logImageHeapPartitionBoundaries(verboseGCLog);
        }
    }
    
    if (SubstrateGCOptions.VerboseGC.getValue()) {
        verboseGCLog.string("[");
        verboseGCLog.string("[");
        long startTime = System.nanoTime();
        if (SerialGCOptions.PrintGCTimeStamps.getValue()) {
            verboseGCLog.unsigned(TimeUtils.roundNanosToMillis(Timer.getTimeSinceFirstAllocation(startTime))).string(" msec: ");
        } else {
            verboseGCLog.unsigned(startTime);
        }
        verboseGCLog.string(" GC:").string(" before").string("  epoch: ").unsigned(getCollectionEpoch()).string("  cause: ").string(cause);
        if (SerialGCOptions.PrintHeapShape.getValue()) {
            heap.report(verboseGCLog);
        }
        verboseGCLog.string("]").newline();
    }
}

    //GC Logの出力箇所
private void printGCAfter(String cause) {
    Log verboseGCLog = Log.log();
    HeapImpl heap = HeapImpl.getHeapImpl();
    if (SubstrateGCOptions.PrintGC.getValue() || SubstrateGCOptions.VerboseGC.getValue()) {
        if (SubstrateGCOptions.PrintGC.getValue()) {
            Log printGCLog = Log.log();
            UnsignedWord sizeAfter = getChunkBytes();
            printGCLog.string("[");
            if (SerialGCOptions.PrintGCTimeStamps.getValue()) {
                long finishNanos = timers.collection.getClosedTime();
                printGCLog.unsigned(TimeUtils.roundNanosToMillis(Timer.getTimeSinceFirstAllocation(finishNanos))).string(" msec: ");
            }
            printGCLog.string(completeCollection ? "Full GC" : "Incremental GC");
            printGCLog.string(" (").string(cause).string(") ");
            printGCLog.unsigned(sizeBefore.unsignedDivide(1024));
            printGCLog.string("K->");
            printGCLog.unsigned(sizeAfter.unsignedDivide(1024)).string("K, ");
            printGCLog.rational(timers.collection.getMeasuredNanos(), TimeUtils.nanosPerSecond, 7).string(" secs");
            printGCLog.string("]").newline();
        }
        if (SubstrateGCOptions.VerboseGC.getValue()) {
            verboseGCLog.string(" [");
            long finishNanos = timers.collection.getClosedTime();
            if (SerialGCOptions.PrintGCTimeStamps.getValue()) {
                verboseGCLog.unsigned(TimeUtils.roundNanosToMillis(Timer.getTimeSinceFirstAllocation(finishNanos))).string(" msec: ");
            } else {
                verboseGCLog.unsigned(finishNanos);
            }
            verboseGCLog.string(" GC:").string(" after ").string("  epoch: ").unsigned(getCollectionEpoch()).string("  cause: ").string(cause);
            verboseGCLog.string("  policy: ");
            verboseGCLog.string(getPolicy().getName());
            verboseGCLog.string("  type: ").string(completeCollection ? "complete" : "incremental");
            if (SerialGCOptions.PrintHeapShape.getValue()) {
                heap.report(verboseGCLog);
            }
            if (!SerialGCOptions.PrintGCTimes.getValue()) {
                verboseGCLog.newline();
                verboseGCLog.string("  collection time: ").unsigned(timers.collection.getMeasuredNanos()).string(" nanoSeconds");
            } else {
                timers.logAfterCollection(verboseGCLog);
            }
            verboseGCLog.string("]");
            verboseGCLog.string("]").newline();
        }
    }
}
```

```Java
package com.oracle.svm.core.genscavenge;

import org.graalvm.nativeimage.ImageSingletons;
import org.graalvm.nativeimage.StackValue;
import org.graalvm.word.UnsignedWord;

import com.oracle.svm.core.SubstrateOptions;
import com.oracle.svm.core.Uninterruptible;
import com.oracle.svm.core.feature.AutomaticallyRegisteredFeature;
import com.oracle.svm.core.feature.InternalFeature;
import com.oracle.svm.core.heap.GCCause;
import com.oracle.svm.core.jfr.HasJfrSupport;
import com.oracle.svm.core.jfr.JfrEvent;
import com.oracle.svm.core.jfr.JfrGCName;
import com.oracle.svm.core.jfr.JfrGCNames;
import com.oracle.svm.core.jfr.JfrNativeEventWriter;
import com.oracle.svm.core.jfr.JfrNativeEventWriterData;
import com.oracle.svm.core.jfr.JfrNativeEventWriterDataAccess;
import com.oracle.svm.core.jfr.JfrTicks;
import com.oracle.svm.core.jfr.SubstrateJVM;
import com.oracle.svm.core.util.VMError;

class JfrGCEventSupport {
    private static final int MAX_PHASE_LEVEL = 4;

    private final JfrGCName gcName;
    private int currentPhase;

    JfrGCEventSupport(JfrGCName gcName) {
        this.gcName = gcName;
    }

    public long startGCPhasePause() {
        pushPhase();
        return JfrTicks.elapsedTicks();
    }

    public int stopGCPhasePause() {
        return popPhase();
    }

    // jdk.GarbageCollectionを出力する
    @Uninterruptible(reason = "Accesses a JFR buffer.")
    public void emitGarbageCollectionEvent(UnsignedWord gcEpoch, GCCause cause, long start) {
        if (SubstrateJVM.isRecording() && SubstrateJVM.get().isEnabled(JfrEvent.GarbageCollection)) {
            long pauseTime = JfrTicks.elapsedTicks() - start;

            JfrNativeEventWriterData data = StackValue.get(JfrNativeEventWriterData.class);
            JfrNativeEventWriterDataAccess.initializeThreadLocalNativeBuffer(data);

            JfrNativeEventWriter.beginSmallEvent(data, JfrEvent.GarbageCollection);
            JfrNativeEventWriter.putLong(data, start);
            JfrNativeEventWriter.putLong(data, pauseTime);
            JfrNativeEventWriter.putLong(data, gcEpoch.rawValue());
            JfrNativeEventWriter.putLong(data, gcName.getId());
            JfrNativeEventWriter.putLong(data, cause.getId());
            JfrNativeEventWriter.putLong(data, pauseTime);  // sum of pause
            JfrNativeEventWriter.putLong(data, pauseTime);  // longest pause
            JfrNativeEventWriter.endSmallEvent(data);
        }
    }

    
    @Uninterruptible(reason = "Accesses a JFR buffer.")
    public void emitGCPhasePauseEvent(UnsignedWord gcEpoch, int level, String name, long startTicks) {
        JfrEvent event = getGCPhasePauseEvent(level);
        if (SubstrateJVM.isRecording() && SubstrateJVM.get().isEnabled(event)) {
            long end = JfrTicks.elapsedTicks();
            JfrNativeEventWriterData data = StackValue.get(JfrNativeEventWriterData.class);
            JfrNativeEventWriterDataAccess.initializeThreadLocalNativeBuffer(data);

            JfrNativeEventWriter.beginSmallEvent(data, event);
            JfrNativeEventWriter.putLong(data, startTicks);
            JfrNativeEventWriter.putLong(data, end - startTicks);
            JfrNativeEventWriter.putEventThread(data);
            JfrNativeEventWriter.putLong(data, gcEpoch.rawValue());
            JfrNativeEventWriter.putString(data, name);
            JfrNativeEventWriter.endSmallEvent(data);
        }
    }

    /**
     * GCPhasePause events are used to group GC phases into a hierarchy. They don't have any
     * predefined meaning as they are used in a GC-specific way. The most descriptive part is the
     * phase name that the GC emits as part of those JFR events.
     */
    @Uninterruptible(reason = "Called from uninterruptible code.", mayBeInlined = true)
    private static JfrEvent getGCPhasePauseEvent(int level) {
        switch (level) {
            case 0:
                return JfrEvent.GCPhasePauseEvent;
            case 1:
                return JfrEvent.GCPhasePauseLevel1Event;
            case 2:
                return JfrEvent.GCPhasePauseLevel2Event;
            case 3:
                return JfrEvent.GCPhasePauseLevel3Event;
            case 4:
                return JfrEvent.GCPhasePauseLevel4Event;
            default:
                throw VMError.shouldNotReachHere("GC phase pause level must be between 0 and 4.");
        }
    }

    private void pushPhase() {
        assert currentPhase < MAX_PHASE_LEVEL;
        currentPhase++;
    }

    @Uninterruptible(reason = "Called from uninterruptible code.", mayBeInlined = true)
    private int popPhase() {
        assert currentPhase > 0;
        return --currentPhase;
    }
}

@AutomaticallyRegisteredFeature
class JfrGCEventFeature implements InternalFeature {
    @Override
    public boolean isInConfiguration(IsInConfigurationAccess access) {
        return SubstrateOptions.UseSerialGC.getValue();
    }

    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access) {
        if (HasJfrSupport.get()) {
            JfrGCName name = JfrGCNames.singleton().addGCName("serial");
            ImageSingletons.add(JfrGCEventSupport.class, new JfrGCEventSupport(name));
        }
    }
}

```
