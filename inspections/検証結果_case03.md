# Case03の検証結果

## 確認項目

無駄に大量のオブジェクト（小さい）を生成するケースが確認できるか



## 実施条件

1バイトのサイズを繰り返し確保する

```java{.line-numbers}
package com.example;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;

/**
 * 1バイトで繰り返し確保する
 *
 */
public class App {
    // https://atmarkit.itmedia.co.jp/ait/articles/0501/25/news127.html
    public static void printDigest(byte[] digest) {//ダイジェストを16進数で表示する
        for (int i = 0; i < digest.length; i++) {
            int d = digest[i];
            if (d < 0) {//byte型では128～255が負値になっているので補正
                d += 256;
            }
            if (d < 16) {//0～15は16進数で1けたになるので、2けたになるよう頭に0を追加
                System.out.print("0");
            }
            System.out.print(Integer.toString(d, 16));//ダイジェスト値の1バイトを16進数2けたで表示
        }
        System.out.println();
    }

    public static void main(String[] args) throws InterruptedException, NoSuchAlgorithmException {

        Long seed = System.currentTimeMillis();
        System.out.println("seed is " + seed);

        while ( ( System.currentTimeMillis() -seed ) < 600000) {
            


            ArrayList<byte[]> list = new ArrayList<>();
            Random random = new Random(seed);

            for (int index = 0; index < 3000000; index++) {
            
                byte[] buf = new byte[1];
                random.nextBytes(buf);

                list.add(buf);
            }

            System.out.println("List Size = " + list.size());

            byte[] data = list.get(random.nextInt(list.size()));
            MessageDigest sha3_256 = MessageDigest.getInstance("SHA3-256");

            printDigest(sha3_256.digest(data));
        }

    }

}

```


## Javaでの確認結果

サマリページでの警告～イベントの発生状況、発生時の詳細情報をJMCで確認することができた。

|     ページ     |   図   |           確認できたこと           |
| -------------- | ------ | ---------------------------------- |
| レポートページ | 図03-1 | GC関連のレポート情報が多数出ている |
| GCページ       | 図03-2 | GCの発生状況                       |


<figure><img src='images/221118_014556.png' width='600px'><figcaption>図03-1. レポート</figcaption></figure>

<figure><img src='images/221118_014708.png' width='600px'><figcaption>図03-2. GC</figcaption></figure>



## Nativeでの確認結果

ヒープ値が出ていない


|     ページ     |   図   |           確認できたこと           |
| -------------- | ------ | ---------------------------------- |
| レポートページ | 図03-2 | GC関連のレポート情報が多数出ている |
| GCページ       | 図03-3 | GCの発生状況                       |

<figure><img src='images/221118_015337.png' width='600px'><figcaption>図. タイトル</figcaption></figure>

<figure><img src='images/221118_015418.png' width='600px'><figcaption>図. タイトル</figcaption></figure>


                           Count  Size (bytes) 
=============================================================
 jdk.GCPhasePauseLevel2                  18102        593561
 jdk.GCPhasePauseLevel1                  15516        519678
 jdk.JavaMonitorWait                      5560        150120
 jdk.GCPhasePause                         3879        115077
 jdk.SafepointBegin                       2587         41398
 jdk.GarbageCollection                    2586         64650
 jdk.ExecuteVMOperation                   2586         51720
 jdk.ExceptionStatistics                   290          4930
 jdk.ActiveSetting                         251          8681
 jdk.InitialEnvironmentVariable             38          4291
 jdk.InitialSystemProperty                  37          1581
 jdk.JavaThreadStatistics                    2            26
 jdk.PhysicalMemory                          2            30
 jdk.ThreadStart                             2            26
 jdk.ThreadEnd                               1            11
 jdk.Metadata                                1         68869
 jdk.CheckPoint                              1           925
 jdk.JavaMonitorEnter                        1            26
 jdk.ActiveRecording                         1           142
 jdk.JVMInformation                          1           439
 jdk.OSInformation                           1            64
 jdk.ClassLoadingStatistics                  1            12