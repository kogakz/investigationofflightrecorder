#!/bin/bash
# ヒープサイズを 200MB
java \
-Dcom.sun.management.jmxremote=true \
-Djava.rmi.server.hostname=127.0.0.1 \
-Dcom.sun.management.jmxremote.port=7091 \
-Dcom.sun.management.jmxremote.authenticate=false \
-Dcom.sun.management.jmxremote.ssl=false \
-Dcom.sun.management.jmxremote.registry.ssl=false \
-Djava.net.preferIPv4Stack=true \
-XX:StartFlightRecording=settings=profile,duration=6m,name=app-startup,dumponexit=true,filename=./jfr/java/g1-startup.jfr \
-Xlog:gc*:log/java/gc.log:time,uptimemillis:filecount=5,filesize=3M \
-Xms350m -Xmx350m \
-jar target/case01-1.0-SNAPSHOT.jar
