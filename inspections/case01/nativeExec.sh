#!/bin/bash
./target/app \
-XX:+FlightRecorder \
-XX:StartFlightRecording="settings=jfc/17profile.jfc,duration=6m,name=native-startup,dumponexit=true,filename=jfr/native/native17-leak${1}-time${2}-innerLoop${3}.jfr" \
-XX:+PrintGC -XX:+VerboseGC \
-Xms300m -Xmx300m $1 $2 $3 2> log/native/gc.log

ls -la ./jfr/native/native17-leak${1}-time${2}-innerLoop${3}.jfr
jfr summary jfr/native/native17-leak${1}-time${2}-innerLoop${3}.jfr | grep HeapSumm
