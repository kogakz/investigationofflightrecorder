#!/bin/bash
java \
-Dcom.sun.management.jmxremote=true \
-Djava.rmi.server.hostname=127.0.0.1 \
-Dcom.sun.management.jmxremote.port=7091 \
-Dcom.sun.management.jmxremote.authenticate=false \
-Dcom.sun.management.jmxremote.ssl=false \
-Dcom.sun.management.jmxremote.registry.ssl=false \
-Djava.net.preferIPv4Stack=true \
-XX:+UseSerialGC \
-XX:StartFlightRecording=settings=profile,duration=10m,name=app-startup,dumponexit=true,filename=./jfr/java-serial/leak"$1"-time"$2"-innerLoop"$3".jfr \
-Xlog:gc*:log/java-serial/gc.log:time,uptimemillis:filecount=5,filesize=3M \
-Xms300m -Xmx300m \
-jar target/case01-1.0-SNAPSHOT.jar $1 $2 $3
