#!/bin/bash
# ヒープサイズを 200MB
./target/app \
-XX:+FlightRecorder \
-XX:StartFlightRecording="filename=jfr/native/test.jfr" \
-Xlog:gc*:log/java/testgc.log:time,uptimemillis:filecount=5,filesize=3M \
-Xms400m -Xmx400m
