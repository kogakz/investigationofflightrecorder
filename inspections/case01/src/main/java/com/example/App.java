package com.example;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class App {

    public static void main(String[] args) throws InterruptedException, NoSuchAlgorithmException {

        int leakSize = Integer.valueOf(args[0]); // 1回のループでリークするサイズ(バイト)
        int loopTime = Integer.valueOf(args[1]) * 1000; // 検証の実行時間
        int innerLoop = Integer.valueOf(args[2]); // ガベージコレクションを起こさせるサイズを調整(300バイト*指定回数)

        Long seed = System.currentTimeMillis();
        System.out.println("seed is " + seed);

        ArrayList<byte[]> oldList = new ArrayList<>();

        while ((System.currentTimeMillis() - seed) < loopTime) {

            ArrayList<byte[]> list = new ArrayList<>();

            for (int index = 0; index < innerLoop; index++) {

                byte[] buf = new byte[300];
                random.nextBytes(buf);

                list.add(buf);
            }

            oldList.add(new byte[leakSize]);

        }

    }

}
