# GraalVM CEのビルド手順

## 前提

Ubuntuのコンテナ
rootユーザ
githubへのアクセス権があること
amd64

## ビルド手順まとめ
1. pythonをビルドする
   1. 関連パッケージを入れる（build-essentialなど：gccとかlibffiはgraalvm事態にも必要だがこのタイミングで入る）
   2. pythonをビルドする
2. Labs-JDKを取得する
3. GraalVMのソースを取得する（タグを使ってバージョンを指定してLabs-JDKと合わせる）
4. graalのソース展開した配下のcompiler配下でmxを実行する
5. graalのソース展開した配下のvmに移動する
6. Graal22.3以下の場合、vm/mx.vm配下のceファイルをコピーして、native-imageも合わせて生成されるよう設定を追加する
7. vm配下でmx --env 6で作成した設定ファイル名 buildを実行する

## ビルド手順詳細

### コンテナ起動
docker -it run -v ~/.ssh:/root/.ssh --name graal Ubuntu

### pythonのインストール

https://www.python.jp/install/ubuntu/index.html

```shell
apt update
apt install 
apt -y install build-essential libbz2-dev libdb-dev \
  libreadline-dev libffi-dev libgdbm-dev liblzma-dev \
  libncursesw5-dev libsqlite3-dev libssl-dev \
  zlib1g-dev uuid-dev tk-dev

apt -y install wget

cd 
wget https://www.python.org/ftp/python/3.11.2/Python-3.11.2.tgz
tar -xvzf Python-3.11.2.tgz 
cd Python-3.11.2
./configure
make
make install

cd ..
rm Python-3.11.2.tgz 
rm -rf Python-3.11.2
```
### JDK19 のインストール
apt -y openjdk-19-jdk


### gitのインストール

```shell
apt -y install git
git clone https://github.com/graalvm/mx.git
```

### mx のインストール

```shell
cd graal 
git clone https://github.com/graalvm/mx.git
export PATH=$PWD/mx:$PATH

```

### Labs-JDKのインストール

Graalのバージョンに合わせたものを取得すること。

Graal23用のJDK20
~~~shell
wget https://github.com/graalvm/labs-openjdk-20/releases/download/jvmci-23.0-b09/labsjdk-ce-20+34-jvmci-23.0-b09-linux-amd64.tar.gz
tar -xvzf labsjdk-ce-20+34-jvmci-23.0-b09-linux-amd64.tar.gz
rm labsjdk-ce-20+34-jvmci-23.0-b09-linux-amd64.tar.gz
~~~

Graal22.3用のJDK19

```shell
wget https://github.com/graalvm/labs-openjdk-19/releases/download/jvmci-22.3-b12/labsjdk-ce-19.0.2+7-jvmci-22.3-b12-linux-amd64.tar.gz
tar -xvzf labsjdk-ce-19.0.2+7-jvmci-22.3-b12-linux-amd64.tar.gz
rm labsjdk-ce-19.0.2+7-jvmci-22.3-b12-linux-amd64.tar.gz
```

Graal22.3用のJDK11

https://github.com/graalvm/labs-openjdk-11/releases/tag/jvmci-22.3-b12

```shell
wget https://github.com/graalvm/labs-openjdk-11/releases/download/jvmci-22.3-b12/labsjdk-ce-11.0.18+9-jvmci-22.3-b12-linux-amd64.tar.gz
tar -xvzf labsjdk-ce-11.0.18+9-jvmci-22.3-b12-linux-amd64.tar.gz
rm labsjdk-ce-11.0.18+9-jvmci-22.3-b12-linux-amd64.tar.gz
```

### JAVA_HOMEの設定

ビルドしたいJDKのバージョンに合わせてJAVA_HOMEを設定する

```shell
export JAVA_HOME=/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/labsjdk-ce-11.0.19-jvmci-22.3-b14
```

### EXTRA_JAVA_HOMESの設定（オプション）

合わせてビルドチェックを行いたい他のバージョンのJDKがある場合は、EXTRA_JAVA_HOMESに対象のパスを指定する。
ただし、このパスに指定しても以降のビルド手順でできる生成結果には、影響しなかった。

### ソースの取得

取得する際に最新のものを取得するとJDKのサポートバージョンが変わっていたりするので、タグを指定して取得すること。

```shell
git clone git@github.com:oracle/graal.git -b vm-ce-22.3.1
cd graal
cd compiler
mx build

# ハイレベルの生成物作成
cd ../vm

# 生成前の状態
ls -ltr
total 440
drwxr-xr-x 2 kogakz kogakz   4096 Mar 11 20:14 docs
drwxr-xr-x 2 kogakz kogakz   4096 Mar 11 20:14 ci_includes
drwxr-xr-x 2 kogakz kogakz   4096 Mar 11 20:14 ci_common
-rw-r--r-- 1 kogakz kogakz  24166 Mar 11 20:14 ce-release-artifacts.json
drwxr-xr-x 8 kogakz kogakz   4096 Mar 11 20:14 benchmarks
-rw-r--r-- 1 kogakz kogakz 354225 Mar 11 20:14 THIRD_PARTY_LICENSE_CE.txt
-rw-r--r-- 1 kogakz kogakz   7239 Mar 11 20:14 README.md
-rw-r--r-- 1 kogakz kogakz  23491 Mar 11 20:14 LICENSE_GRAALVM_CE
-rw-r--r-- 1 kogakz kogakz   1611 Mar 11 20:14 GRAALVM-README.md
drwxr-xr-x 3 kogakz kogakz   4096 Mar 11 20:14 tests
drwxr-xr-x 7 kogakz kogakz   4096 Mar 11 20:14 src
drwxr-xr-x 4 kogakz kogakz   4096 Mar 13 07:39 mx.vm
drwxr-xr-x 5 kogakz kogakz   4096 Mar 13 08:39 mxbuild

```

### CE用の設定でビルド

```shell
mx --env ce build

# ビルド結果
# 以下ができている
#   latest_graalvm_home
#   latest_graalvm
$  ls -ltr
total 440
drwxr-xr-x 2 kogakz kogakz   4096 Mar 11 20:14 docs
drwxr-xr-x 2 kogakz kogakz   4096 Mar 11 20:14 ci_includes
drwxr-xr-x 2 kogakz kogakz   4096 Mar 11 20:14 ci_common
-rw-r--r-- 1 kogakz kogakz  24166 Mar 11 20:14 ce-release-artifacts.json
drwxr-xr-x 8 kogakz kogakz   4096 Mar 11 20:14 benchmarks
-rw-r--r-- 1 kogakz kogakz 354225 Mar 11 20:14 THIRD_PARTY_LICENSE_CE.txt
-rw-r--r-- 1 kogakz kogakz   7239 Mar 11 20:14 README.md
-rw-r--r-- 1 kogakz kogakz  23491 Mar 11 20:14 LICENSE_GRAALVM_CE
-rw-r--r-- 1 kogakz kogakz   1611 Mar 11 20:14 GRAALVM-README.md
drwxr-xr-x 3 kogakz kogakz   4096 Mar 11 20:14 tests
drwxr-xr-x 7 kogakz kogakz   4096 Mar 11 20:14 src
drwxr-xr-x 4 kogakz kogakz   4096 Mar 13 07:39 mx.vm
drwxr-xr-x 5 kogakz kogakz   4096 Mar 13 08:40 mxbuild
lrwxrwxrwx 1 kogakz kogakz     26 Mar 13 08:43 latest_graalvm_home -> ../sdk/latest_graalvm_home
lrwxrwxrwx 1 kogakz kogakz     21 Mar 13 08:43 latest_graalvm -> ../sdk/latest_graalvm

# リンク先の確認
# mxbuild配下に実態はある
ls -ltr  ../sdk/latest_graalvm_home
lrwxrwxrwx 1 kogakz kogakz 62 Mar 13 08:43 ../sdk/latest_graalvm_home -> mxbuild/linux-amd64/GRAALVM_CE_JAVA11/graalvm-ce-java11-22.3.0

ls -ltr ../sdk/latest_graalvm
lrwxrwxrwx 1 kogakz kogakz 37 Mar 13 08:43 ../sdk/latest_graalvm -> mxbuild/linux-amd64/GRAALVM_CE_JAVA11


# 中身の確認
ls latest_graalvm_home
GRAALVM-README.md  LICENSE.txt  THIRD_PARTY_LICENSE.txt  bin  conf  include  jmods  languages  legal  lib  release  tools

# bin配下
# natie-imageはない。
ls latest_graalvm_home/bin
gu  jar  jarsigner  java  javac  javadoc  javap  jcmd  jconsole  jdb  jdeprscan  jdeps  jfr  jhsdb  jimage  jinfo  jjs  jlink  jmap  jmod  jps  jrunscript  jshell  jstack  jstat  jstatd  keytool  pack200  polyglot  rmic  rmid  rmiregistry  serialver  unpack200

```

### 参考 最新ソース（23になるものを）をビルドした場合

native-imageもデフォルトで生成される

```shell

ls -ltr ../sdk/latest_graalvm_home
lrwxrwxrwx 1 root root 66 Mar 11 17:19 ../sdk/latest_graalvm_home -> mxbuild/linux-amd64/GRAALVM_CE_JAVA20/graalvm-ce-java20-23.0.0-dev

ls latest_graalvm_home/bin/
gu  jar  jarsigner  java  javac  javadoc  javap  jcmd  jconsole  jdb  jdeprscan  jdeps  jfr  jhsdb  jimage  jinfo  jlink  jmap  jmod  jpackage  jps  jrunscript  jshell  jstack  jstat  jstatd  jwebserver  keytool  native-image  native-image-configure  polyglot  rebuild-images  rmiregistry  serialver
```

### ビルドオプションの比較 

22と、23のビルドオプションの中身を比較してみると`COMPONENTS`,`NATIVE_IMAGES`の中身が結構違う。
プルリクエストを確認したところ、変更しているのはドキュメントとmxの設定ファイルのみだった
https://github.com/oracle/graal/pull/5995

変更前
```text
COMPONENTS=cmp,cov,dap,gu,gvm,icu4j,ins,insight,insightheap,jss,lg,lsp,nfi-libffi,poly,polynative,pro,rgx,sdk,tfl,tflm
NATIVE_IMAGES=graalvm-native-binutil,graalvm-native-clang,graalvm-native-clang-cl,graalvm-native-clang++,graalvm-native-ld,lib:jvmcicompiler`

変更後
COMPONENTS=cmp,cov,dap,gu,gvm,icu4j,ins,insight,insightheap,jss,lg,lsp,nfi-libffi,ni,nic,nil,poly,polynative,pro,rgx,sdk,svm,svmnfi,svmsl,tfl,tflm
NATIVE_IMAGES=graalvm-native-binutil,graalvm-native-clang,graalvm-native-clang-cl,graalvm-native-clang++,graalvm-native-ld,lib:jvmcicompiler,lib:native-image-agent,lib:native-image-diagnostics-agent,native-image



mx.vm配下の設定を確認したところ、`ce-complete`には、native-imageも設定されていることが分かった。
→`ce-complete`を指定してビルドしたところ、sulongというコンポーネントのビルドでエラーになった。
※`sulong`はhigh-performance LLVM bitcode runtimeとのこと。

https://github.com/oracle/graal/blob/master/sulong/README.md


22
```shell
cat mx.vm/ce
DYNAMIC_IMPORTS=/compiler,/graal-js,/regex,/sdk,/substratevm,/tools,/truffle
COMPONENTS=cmp,cov,dap,gu,gvm,icu4j,ins,insight,insightheap,jss,lg,lsp,nfi-libffi,poly,polynative,pro,rgx,sdk,tfl,tflm
NATIVE_IMAGES=graalvm-native-binutil,graalvm-native-clang,graalvm-native-clang-cl,graalvm-native-clang++,graalvm-native-ld,gu,lib:jvmcicompiler
DISABLE_INSTALLABLES=False
```

23
```shell
root@b738da27084c:~/graal23/vm/mx.vm# cat ce
DYNAMIC_IMPORTS=/compiler,/graal-js,/regex,/sdk,/substratevm,/tools,/truffle
COMPONENTS=cmp,cov,dap,gu,gvm,icu4j,ins,insight,insightheap,jss,lg,lsp,nfi-libffi,ni,nic,nil,poly,polynative,pro,rgx,sdk,svm,svmnfi,svmsl,tfl,tflm
NATIVE_IMAGES=graalvm-native-binutil,graalvm-native-clang,graalvm-native-clang-cl,graalvm-native-clang++,graalvm-native-ld,lib:jvmcicompiler,lib:native-image-agent,lib:native-image-diagnostics-agent,native-image
DISABLE_INSTALLABLES=False
```

mx.vm配下の設定を調べてみた
```shell
grep native-image * | grep -v ce-19 | grep -v ce-darwin | grep -v ce-win  | grep -v aarch
grep: __pycache__: Is a directory
grep: eclipse-settings: Is a directory
ce-complete:NATIVE_IMAGES=lib:pythonvm,graalvm-native-binutil,graalvm-native-clang,graalvm-native-clang-cl,graalvm-native-clang++,graalvm-native-ld,gu,lib:jsvm,lib:javavm,lib:graal-nodejs,lib:jvmcicompiler,lib:native-image-agent,lib:native-image-diagnostics-agent,lib:llvmvm,native-image,lib:rubyvm,wasm
ce-test:NATIVE_IMAGES=graalvm-native-binutil,graalvm-native-clang,graalvm-native-clang-cl,graalvm-native-clang++,graalvm-native-ld,gu,lib:jvmcicompiler,lib:native-image-agent,lib:native-image-diagnostics-agent,native-image,native-image-configure
mx_vm.py:            # Please see META-INF/native-image in the project for custom build options for native-image
mx_vm.py:ce_win_19_complete_components = ['bnative-image-configure', 'bpolyglot', 'cmp', 'cov', 'dap', 'gu', 'gvm', 'gwa', 'icu4j', 'ins', 'insight', 'insightheap', 'js', 'jss', 'lg', 'libpoly', 'llp', 'llrc', 'llrl', 'llrn', 'lsp', 'nfi-libffi', 'nfi', 'ni', 'nic', 'nil', 'njs', 'poly', 'polynative', 'pro', 'rgx', 'sdk', 'spolyglot', 'svm', 'svmnfi', 'svmsl', 'tfl', 'tflm', 'vvm']
mx_vm.py:ce_fastr_components = ce_components + llvm_components + ['R', 'bRMain', 'llrn', 'llp', 'sllvmvm', 'llrc', 'ni', 'nil', 'svm', 'bgu', 'svmsl', 'svmnfi', 'snative-image-agent', 'bnative-image', 'snative-image-diagnostics-agent', 'llrl']
mx_vm.py:mx_sdk_vm.register_vm_config('ce', llvm_components + ['java', 'libpoly', 'sjavavm', 'spolyglot', 'ejvm', 'sjsvm', 'sllvmvm', 'bnative-image', 'srubyvm', 'pynl', 'spythonvm', 'pyn', 'bwasm', 'cmp', 'gwa', 'icu4j', 'js', 'jss', 'lg', 'llp', 'nfi-libffi', 'nfi', 'ni', 'nil', 'pbm', 'pmh', 'pbi', 'rby', 'rbyl', 'rgx', 'sdk', 'llrc', 'llrn', 'llrl', 'snative-image-agent', 'snative-image-diagnostics-agent', 'svm', 'svmnfi', 'svmsl', 'tfl', 'tflm'], _suite, env_file='polybench-ce')
mx_vm.py:mx_sdk_vm.register_vm_config('ce', ['bnative-image', 'bpolybench', 'cmp', 'icu4j', 'lg', 'nfi', 'ni', 'nil', 'pbi', 'pbm', 'pmh', 'sdk', 'snative-image-agent', 'snative-image-diagnostics-agent', 'svm', 'svmnfi', 'svmsl', 'tfl', 'tflm'], _suite, dist_name='ce', env_file='polybench-ctw-ce')
mx_vm.py:mx_sdk_vm.register_vm_config('ce', ['pbm', 'pmh', 'pbi', 'ni', 'icu4j', 'js', 'jss', 'lg', 'nfi-libffi', 'nfi', 'tfl', 'svm', 'nil', 'rgx', 'sdk', 'cmp', 'tflm', 'svmnfi', 'svmsl', 'bnative-image', 'sjsvm', 'snative-image-agent', 'snative-image-diagnostics-agent'], _suite, env_file='polybench-nfi-ce')
mx_vm.py:mx_sdk_vm.register_vm_config('ce', llvm_components + ['sllvmvm', 'bnative-image', 'cmp', 'lg', 'llrc', 'llrl', 'llrn', 'nfi-libffi', 'nfi', 'ni', 'nil', 'pbm', 'pbi', 'sdk', 'snative-image-agent', 'snative-image-diagnostics-agent', 'svm', 'svmnfi', 'svmsl', 'tfl', 'tflm'], _suite, env_file='polybench-sulong-ce')
mx_vm.py:    mx_sdk_vm.register_vm_config('svm', ['bnative-image', 'bnative-image-configure', 'bpolyglot', 'cmp', 'gvm', 'nfi-libffi', 'nfi', 'ni', 'nil', 'nju', 'njucp', 'nic', 'poly', 'polynative', 'rgx', 'sdk', 'snative-image-agent', 'snative-image-diagnostics-agent', 'svm', 'svmnfi', 'svmsl', 'tfl', 'tflm'], _suite, env_file=False)
mx_vm.py:    mx_sdk_vm.register_vm_config('svm', ['bnative-image', 'bnative-image-configure', 'bpolyglot', 'cmp', 'gu', 'gvm', 'nfi-libffi', 'nfi', 'ni', 'nil', 'nju', 'njucp', 'nic', 'poly', 'polynative', 'rgx', 'sdk', 'snative-image-agent', 'snative-image-diagnostics-agent', 'svm', 'svmnfi', 'svmsl', 'svml', 'tfl', 'tflm'], _suite, env_file=False)
mx_vm_benchmark.py:        if self.name() == 'native-image-java-home':
mx_vm_benchmark.py:            self.output_dir = mx.join(os.path.abspath(self.root_dir), 'native-image-benchmarks', self.executable_name + '-' + vm.config_name())
mx_vm_benchmark.py:            self.base_image_build_args = [os.path.join(vm.home(), 'bin', 'native-image')]
mx_vm_benchmark.py:                clean_args[i + 1] = ' '.join([x for x in args[i + 1].split(' ') if "-Dnative-image" not in x])
mx_vm_benchmark.py:        clean_args = [x for x in clean_args if "-Dnative-image" not in x]
mx_vm_benchmark.py:                        mx.log('-Dnative-image.benchmark.stages=' + ','.join(self.stages_till_now[:-1]))
mx_vm_benchmark.py:                    mx.log('-Dnative-image.benchmark.stages=' + self.current_stage)
mx_vm_benchmark.py:                        mx.log('-Dnative-image.benchmark.' + param + '=')
mx_vm_benchmark.py:        agentlib_options = ['native-image-agent=config-output-dir=' + str(config.config_dir)] + config.extra_agentlib_options
mx_vm_benchmark.py:                mx_benchmark.add_java_vm(NativeImageVM('native-image', final_config_name), _suite, 10)
mx_vm_benchmark.py:        mx_benchmark.add_java_vm(NativeImageVM('native-image-java-home', java_home_config), _suite, 5)
mx_vm_gate.py:    native_image_cmd = join(mx_sdk_vm_impl.graalvm_output(), 'bin', 'native-image') + ('.cmd' if mx.get_os() == 'windows' else '')
mx_vm_gate.py:        mx.abort("Image building not accessible in GraalVM {}. Build GraalVM with native-image support".format(mx_sdk_vm_impl.graalvm_dist_name()))
mx_vm_gate.py:    with Task('Run SulongSuite tests as native-image', tasks, tags=[VmGateTasks.sulong]) as t:
mx_vm_gate.py:    with Task('Run SulongSuite tests as native-image with engine cache', tasks, tags=[VmGateTasks.sulong_aot]) as t:
mx_vm_gate.py:    with Task('Run Sulong interop tests as native-image', tasks, tags=[VmGateTasks.sulong]) as t:
ni-ce:NATIVE_IMAGES=native-image,lib:native-image-agent,lib:native-image-diagnostics-agent
suite.py:                "native-image.properties" : "file:mx.vm/polybench-instruments.properties",
suite.py:                "native-image.properties": "file:mx.vm/language-pmh.properties",
```


### ce-completeでビルドしてみた

以下のエラーが出て終了
AttributeError: module 'mx' has no attribute '_with_metaclass'

```shell
 mx --env ce-complete build 
Cloning https://github.com/oracle/truffleruby.git revision 939379fc43785567006ed246850473af50eceec1 to /home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/truffleruby with Git
Cloning into '/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/truffleruby'...
remote: Enumerating objects: 811774, done.
remote: Counting objects: 100% (2201/2201), done.
remote: Compressing objects: 100% (959/959), done.
remote: Total 811774 (delta 1222), reused 1924 (delta 1053), pack-reused 809573
Receiving objects: 100% (811774/811774), 276.64 MiB | 9.10 MiB/s, done.
Resolving deltas: 100% (537666/537666), done.
Cloning https://github.com/oracle/fastr.git revision a9ebce23dcf689c989b13a3af688b3a6447e1fc3 to /home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/fastr with Git
Cloning into '/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/fastr'...
remote: Enumerating objects: 279268, done.
remote: Counting objects: 100% (13983/13983), done.
remote: Compressing objects: 100% (3078/3078), done.
remote: Total 279268 (delta 6303), reused 13730 (delta 6106), pack-reused 265285
Receiving objects: 100% (279268/279268), 571.72 MiB | 9.12 MiB/s, done.
Resolving deltas: 100% (101383/101383), done.
Cloning https://github.com/graalvm/graalpython.git revision 192ef80a7ffadff76c0857f51bd39d248a39409b to /home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graalpython with Git
Cloning into '/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graalpython'...
remote: Enumerating objects: 280256, done.
remote: Counting objects: 100% (33249/33249), done.
remote: Compressing objects: 100% (7866/7866), done.
remote: Total 280256 (delta 20067), reused 32398 (delta 19505), pack-reused 247007
Receiving objects: 100% (280256/280256), 148.02 MiB | 9.14 MiB/s, done.
Resolving deltas: 100% (174096/174096), done.
Traceback (most recent call last):
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/mx/mx_enter.py", line 44, in <module>
    mx._main_wrapper()
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/mx/mx.py", line 18388, in _main_wrapper
    main()
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/mx/mx.py", line 18269, in main
    primary = _discover_suites(primarySuiteMxDir, load=should_load_suites)
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/mx/mx.py", line 3747, in _discover_suites
    _register_visit(primary)
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/mx/mx.py", line 3743, in _register_visit
    _register_visit(discovered[_suite_import.name])
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/mx/mx.py", line 3743, in _register_visit
    _register_visit(discovered[_suite_import.name])
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/mx/mx.py", line 3745, in _register_visit
    s._load()
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/mx/mx.py", line 1747, in _load
    self._load_extensions()
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/mx/mx.py", line 2233, in _load_extensions
    mod = __import__(extensionsName)
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/sulong/mx.sulong/mx_sulong.py", line 47, in <module>
    import mx_sulong_gate
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/sulong/mx.sulong/mx_sulong_gate.py", line 43, in <module>
    import mx_sulong_suite_constituents
  File "/home/kogakz/gitouhon2022-2nd/inspections/graalvm-src/graal/sulong/mx.sulong/mx_sulong_suite_constituents.py", line 119, in <module>
    class SulongTestSuiteMixin(mx._with_metaclass(abc.ABCMeta, object)):
AttributeError: module 'mx' has no attribute '_with_metaclass'
```

### mx.vm/ceのヒストリを追いかけてみた
https://github.com/oracle/graal/commits/master/vm/mx.vm/ce




https://github.com/oracle/graal/pull/5995/commits/f2518a66497d058e45ecd30c530c187a87ef90e9
変更履歴をみ
* (GR-44216) Native Image is now shipped as part of the GraalVM JDK and thus no longer needs to be installed via `gu install native-image`.
 

ビルド対象を23.0の変更履歴に合わせてビルドしてみたところ、正常にビルド完了した。
```shell
cp ce ce-with-native
ce-with-native
```

ビルド結果
```shell
ls latest_graalvm_home/bin
gu  jar  jarsigner  java  javac  javadoc  javap  jcmd  jconsole  jdb  jdeprscan  jdeps  jfr  jhsdb  jimage  jinfo  jjs  jlink  jmap  jmod  jps  jrunscript  jshell  jstack  jstat  jstatd  keytool  native-image  native-image-configure  pack200  polyglot  rebuild-images  rmic  rmid  rmiregistry  serialver  unpack200
```




