# GCHeapSummaryのテスト方法

### 変更ファイルリスト

https://github.com/oracle/graal/pull/4312/files

テストコード
- substratevm/src/com.oracle.svm.test/src/com/oracle/svm/test
  - jfr
    - JFRTest.java
      - 変更不要？
      - 特定のイベントに特化したテストコードにはなっていなかった
    - TestClassEvent.java
      - 変更不要？
      - 特定のイベントに特化したテストコードにはなっていなかった
    - TestGCEvents.java
      - GC関連のイベントのリスト
        - 追加必要？追加するとどうなる？
      - System.GCをテストコードとして実行している
    - TestStringEvent.java
      - 
    - TestThreadEvent.java
    - utils
      - JFR.java
      - JFRFileParser.java
      - LocalJFR.java
      - poolparsers
        - GCCauseConstantPoolParser.java
        - GCNameConstantPoolParser.java

### Testコードのシーケンス

JfrtTestはテストコード用の基底クラス

```plantuml
participant JfrtTest
participant LocalJfr
participant JfrFileParser
participant Recording


JfrtTest -> JfrtTest : startRecording
activate JfrtTest
JfrtTest -> LocalJfr : new
activate LocalJfr
JfrtTest -> LocalJfr : createRecording(JfrtTestの継承クラス.getName())
JfrtTest -> JfrtTest : getTestedEvents
activate JfrtTest
JfrtTest --> JfrtTest :テスト対象のイベント情報を返却( 拡張先クラスで定義）
deactivate JfrtTest 
JfrtTest -> JfrtTest : enableEvents テスト対象のイベントを有効化
activate JfrtTest 
JfrtTest -> Recording : enable
deactivate  JfrtTest 
JfrtTest -> LocalJfr : startRecording(Recording)
JfrtTest -> JfrtTest : testコード実行

JfrtTest -> JfrtTest : endRecording
activate JfrtTest
JfrtTest -> LocalJfr : endRecording
JfrtTest -> JfrtTest : checkRecording
activate JfrtTest
JfrtTest -> JfrFileParser  : parse(recording) パースできなかったらテスト失敗
activate JfrFileParser
JfrFileParser -> Recording : toFile
Recording --> JfrFileParser : RecordingInput
JfrFileParser -> JfrFileParser: parserFileHeader(RecordingInput)
activate JfrFileParser
note right
    assertEquals("File magic is not correct!", new String(JfrChunkWriter.FILE_MAGIC), new String(fileMagic));
    assertEquals("JFR version major is not correct!", JfrChunkWriter.JFR_VERSION_MAJOR, input.readRawShort());
    assertEquals("JFR version minor is not correct!", JfrChunkWriter.JFR_VERSION_MINOR, input.readRawShort());
    assertTrue("Chunk size is invalid!", input.readRawLong() > 0); // Chunk size.
    assertTrue("Constant pool positions is invalid!", constantPoolPosition > 0);
    assertTrue("Metadata positions is null!", metadataPosition != 0);
    assertTrue("Starting time is invalid!", startingTime > 0); // Starting time.
    Assert.assertTrue("Starting time is bigger than current time!", startingTime < JfrTicks.currentTimeNanos());
    assertTrue("Chunk start tick is invalid!", input.readRawLong() > 0); // ChunkStartTick.
    assertTrue("Tick frequency is invalid!", input.readRawLong() > 0); // Tick frequency.
    int shouldUseCompressedInt = input.readRawInt();
    assertTrue("Compressed int must be either 0 or 1!", shouldUseCompressedInt == 0 || shouldUseCompressedInt == 1); // ChunkWriteTick.
end note
deactivate JfrFileParser
JfrFileParser -> JfrFileParser : verifyConstantPools(RecordingInput,HeaderPosition)
activate JfrFileParser
note right
1. parseConstantPoolHeaders
  - parseConstantPoolHeader
    - Size of constant pools.
    - Constant pools region ID.
    - Timestamp
    - Constant poolのデータがあること
  - 各ConstantPoolがnull出ないこと
2. compareFoundAndExceptedIds
  - 期待するJfrTypeのIDが含まれていること
  - ※JfrFileParserのsupportedConstantPoolsに追加する必要ありそう
  - 例 GCCauseConstantPoolParser
end note
deactivate JfrFileParser
JfrFileParser -> JfrFileParser : parseMetadata
activate JfrFileParser
note right
1. parseMetadataHeader
  -  assertTrue("Metadata size is invalid!", input.readInt() > 0); // Size of metadata
  -  assertEquals(JfrChunkWriter.METADATA_TYPE_ID, input.readLong()); // Metadata region ID.
  -  assertTrue("Metadata timestamp is invalid!", input.readLong() > 0); // Timestamp.
end note
deactivate JfrFileParser
JfrFileParser --> JfrtTest
deactivate JfrFileParser
JfrtTest -> JfrtTest : cleanupRecording(recording) クリーンナップできなかったらテスト失敗
```



