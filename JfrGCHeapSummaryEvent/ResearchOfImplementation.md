# 過去実装の調査

## 対象

https://github.com/oracle/graal/pull/4312/files

の実装をもとに変更箇所を洗い出す

## まとめ

**substratevm/src/com.oracle.svm.core.genscavenge/src/com/oracle/svm/core/genscavenge**

ファイル一覧
|       ファイル名       |                        概要                         |         GCHeapSummaryの対応         |
| ---------------------- | --------------------------------------------------- | ----------------------------------- |
| GCImpl.java            | GC完了後にJfrGCEventSupportを呼び出してイベント出力 | JfrGCHeapSummaryEvent呼び出しを追加 |
| JfrGCEvents.java       | JfrGCEventSupportのラッパークラス                   | JfrGCHeapSummaryEventを作成         |
| JfrGCEventSupport.java | NativeBufferにJfrGCEventを出力するための実装定義    | JfrGCHeapSummaryEventSupportを作成  |

**substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jfr**

定型文字列はJfrのMetadataとして型定義され、コード＋文字列の対応しているものがあるようです。
例：
- GCCase : JavaLangSystemGC, UnitTest, TestGCInDeoptimizer, HintedGC
- GCName : 
- GCWhen : BeforeGC,AfterGC

それらに対応する型定義用クラスと、管理クラスが以下のクラス群です。


JFRのイベントの型（例：jdk.GarbageCollection）ごとにIDが付与されており、`JfrMetadataTypeLibrary.lookupPlatformEvent(name)`で取得できる。
各イベントのIDをそれぞれ持つJava上での型を定義し、初期化時に取得している。

明示的にJavaの型定義をする狙いは以下か？
- コーディング時にIDEで補完可能となる。
- 初期化時に取得することでAOTコンパイルでも効率的なコードが生成できる。（実行時に都度lookupを行わない）


|    ファイル名     |                                  概要                                  |                      GCHeapSummaryの対応                      |
| ----------------- | ---------------------------------------------------------------------- | ------------------------------------------------------------- |
| JfrEvent.java     | 各型の名前に対応するIDを保持する                                       | GCHeapSummaryを追加                                           |
| JfrGCCause.java   | GCCauseの定型文字列1つのIDと、値を持つ                                 | GCCauseと同様にGCWhenに対応する`JfrGCWhen`,`JfrGCWhens'を追加 |
| JfrGCCauses.java  | JfrGCCauseの生成、管理を責務。IDの割り当てとインスタンスの対応を持つ。 |                                                               |
| JfrGCName.java    | GCNameの定型文字列1つのIDと、値を持つ                                  |                                                               |
| JfrGCNames.java   | JfrGCNameの生成、管理を責務。IDの割り当てとインスタンスの対応を持つ。  |                                                               |
| JfrType.java      | GCCase,GCNameなどの型定義したもののIDを管理する                        | GCWhen,VirtualSpaceを追加                                     |
| JfrTypeRepository | JfrTypeに定義した型のIDと値をチャンクに書き込む                        | GCWhenのwriteメソッドを追加。VirtualSpaceは追加していない     |

### シーケンス

```plantuml
participant JfrGCEventFeature
participant GCImpl
participant JfrGCEvents
JfrGCEventFeature -> JfrGCEventSupport : new 
activate JfrGCEventSupport
JfrGCEventFeature -> ImageSingletons : add(JfrGCEventSupport.class, new JfrGCEventSupport(name))
GCImpl -> JfrGCEvents : emitGarbageCollectionEvent
JfrGCEvents -> ImageSingletons : lookup(JfrGCEventSupport)
JfrGCEvents -> JfrGCEventSupport : emitGarbageCollectionEvent
JfrGCEventSupport -> SubstrateJVM : isRecording() 
JfrGCEventSupport -> SubstrateJVM : get().isEnabled(JfrEvent.GarbageCollection)
JfrGCEventSupport -> JfrTicks : elapsedTicks
JfrGCEventSupport -> StackValue : get(JfrNativeEventWriterData.class)
JfrGCEventSupport -> JfrNativeEventWriterData: initializeThreadLocalNativeBuffer(JfrNativeEventWriterData)
JfrGCEventSupport -> JfrNativeEventWriter : beginSmallEvent(JfrNativeEventWriterData, JfrEvent.GarbageCollection);
JfrGCEventSupport -> JfrNativeEventWriter : putLong(JfrNativeEventWriterData, 各種データ)
JfrGCEventSupport -> JfrNativeEventWriter : endSmallEvent
```

## 詳細

### 各ファイルの確認

以降は最新の実装ファイルをもとに確認している。

#### GCImpl.java

GCの呼び出し口
1. GCの開始時刻を取得（JfrTicks.elapsedTicks)
2. GCを実施(doCollectImpl)
3. GCの完了後にイベントを出力(JfrGCEventSupport.emitGarbageCollectionEvent)

```Java
    private boolean collectImpl(GCCause cause, long requestingNanoTime, boolean forceFullGC) {
        boolean outOfMemory;
        precondition();

        NoAllocationVerifier nav = noAllocationVerifier.open();
        try {
            //1. 開始時刻を取得
            long startTicks = JfrTicks.elapsedTicks();
            try {
                //2. GCの実行
                outOfMemory = doCollectImpl(cause, requestingNanoTime, forceFullGC, false);
                if (outOfMemory) {
                    // Avoid running out of memory with a full GC that reclaims softly reachable
                    // objects
                    ReferenceObjectProcessing.setSoftReferencesAreWeak(true);
                    try {
                        //2. GCの実行
                        outOfMemory = doCollectImpl(cause, requestingNanoTime, true, true);
                    } finally {
                        ReferenceObjectProcessing.setSoftReferencesAreWeak(false);
                    }
                }
            } finally {
                //3. イベントを出力
                JfrGCEvents.emitGarbageCollectionEvent(getCollectionEpoch(), cause, startTicks);
            }
        } finally {
            nav.close();
        }

        postcondition();
        return outOfMemory;
    }
```

#### JfrGCEvents

JfrGCEventSupportの実装を呼び出すためのstaticメソッドを定義している。

```Java
class JfrGCEvents {
    public static long startGCPhasePause() {
        if (hasJfrSupport()) {
            return jfrSupport().startGCPhasePause();
        }
        return 0;
    }

    public static void emitGCPhasePauseEvent(UnsignedWord gcEpoch, String name, long start) {
        if (hasJfrSupport()) {
            int level = jfrSupport().stopGCPhasePause();
            if (start != 0) {
                jfrSupport().emitGCPhasePauseEvent(gcEpoch, level, name, start);
            }
        }
    }

    // 実装
    public static void emitGarbageCollectionEvent(UnsignedWord gcEpoch, GCCause cause, long start) {
        if (hasJfrSupport() && start != 0) {
            jfrSupport().emitGarbageCollectionEvent(gcEpoch, cause, start);
        }
    }

    // 生成されないケースがある？
    @Fold
    static boolean hasJfrSupport() {
        return ImageSingletons.contains(JfrGCEventSupport.class);
    }

    @Fold
    static JfrGCEventSupport jfrSupport() {
        return ImageSingletons.lookup(JfrGCEventSupport.class);
    }
}
```

#### JfrGCEventSupport.java

import com.oracle.svm.core.heap.GCCause;

emit～でイベント書き出しをしている。
https://sap.github.io/SapMachine/jfrevents/11.html
に書かれているものと合わなかったが、jfr metadataコマンドの出力項目と並びや型があっていることを確認した。

- jfr metadataの出力内容
  - JfrNativeEventWriter.putXXXの呼び出し順や数の対応があっていることをチェック済み
  - ソースコードの出力箇所ごとにコメントで記載
  
```Java
@Name("jdk.GarbageCollection")
@Category({"Java Virtual Machine", "GC", "Collector"})
@Label("Garbage Collection")
@Description("Garbage collection performed by the JVM")
class GarbageCollection extends jdk.jfr.Event {
@Label("Start Time")
@Timestamp("TICKS")
long startTime;

@Label("Duration")
@Timespan("TICKS")
long duration;

@Unsigned
@GcId
@Label("GC Identifier")
int gcId;

@Label("Name")
@Description("The name of the Garbage Collector")
String name;

@Label("Cause")
@Description("The reason for triggering this Garbage Collection")
String cause;

@Timespan("TICKS")
@Label("Sum of Pauses")
@Description("Sum of all the times in which Java execution was paused during the garbage collection")
long sumOfPauses;

@Timespan("TICKS")
@Label("Longest Pause")
@Description("Longest individual pause during the garbage collection")
long longestPause;
}
```

JfrGCEventSupportの実装との突き合わせ結果

```Java
package com.oracle.svm.core.genscavenge;

import org.graalvm.nativeimage.ImageSingletons;
import org.graalvm.nativeimage.StackValue;
import org.graalvm.word.UnsignedWord;

import com.oracle.svm.core.SubstrateOptions;
import com.oracle.svm.core.Uninterruptible;
import com.oracle.svm.core.feature.AutomaticallyRegisteredFeature;
import com.oracle.svm.core.feature.InternalFeature;
import com.oracle.svm.core.heap.GCCause;
import com.oracle.svm.core.jfr.HasJfrSupport;
import com.oracle.svm.core.jfr.JfrEvent;
import com.oracle.svm.core.jfr.JfrGCName;
import com.oracle.svm.core.jfr.JfrGCNames;
import com.oracle.svm.core.jfr.JfrNativeEventWriter;
import com.oracle.svm.core.jfr.JfrNativeEventWriterData;
import com.oracle.svm.core.jfr.JfrNativeEventWriterDataAccess;
import com.oracle.svm.core.jfr.JfrTicks;//時間関連？
import com.oracle.svm.core.jfr.SubstrateJVM;
import com.oracle.svm.core.util.VMError;

class JfrGCEventSupport {
    private static final int MAX_PHASE_LEVEL = 4;

    private final JfrGCName gcName;
    private int currentPhase;

    JfrGCEventSupport(JfrGCName gcName) {
        this.gcName = gcName;
    }

    //省略

    //collectImplから呼び出される個所
    @Uninterruptible(reason = "Accesses a JFR buffer.")
    public void emitGarbageCollectionEvent(UnsignedWord gcEpoch, GCCause cause, long start) {
        if (SubstrateJVM.isRecording() && SubstrateJVM.get().isEnabled(JfrEvent.GarbageCollection)) {
            long pauseTime = JfrTicks.elapsedTicks() - start;

            JfrNativeEventWriterData data = StackValue.get(JfrNativeEventWriterData.class);
            JfrNativeEventWriterDataAccess.initializeThreadLocalNativeBuffer(data);

            JfrNativeEventWriter.beginSmallEvent(data, JfrEvent.GarbageCollection); //@Label("Garbage Collection") @Description("Garbage collection performed by the JVM") class GarbageCollection extends jdk.jfr.Event 
            JfrNativeEventWriter.putLong(data, start);                              //@Label("Start Time") @Timestamp("TICKS") long startTime;
            JfrNativeEventWriter.putLong(data, pauseTime);                          //@Label("Duration")   @Timespan("TICKS")  long duration;
            JfrNativeEventWriter.putLong(data, gcEpoch.rawValue());                 //@Unsigned  @GcId     @Label("GC Identifier")  int gcId;
            JfrNativeEventWriter.putLong(data, gcName.getId());                     //@Label("Name")       @Description("The name of the Garbage Collector")  String name;
            JfrNativeEventWriter.putLong(data, cause.getId());                      //@Label("Cause")      @Description("The reason for triggering this Garbage Collection") String cause;
            JfrNativeEventWriter.putLong(data, pauseTime);  // sum of pause         //@Timespan("TICKS")   @Label("Sum of Pauses")  @Description("Sum of all the times in which Java execution was paused during the garbage collection")    long sumOfPauses;
            JfrNativeEventWriter.putLong(data, pauseTime);  // longest pause        //@Timespan("TICKS")   @Label("Longest Pause")  @Description("Longest individual pause during the garbage collection") long longestPause;
            JfrNativeEventWriter.endSmallEvent(data);
        }
    }

    @Uninterruptible(reason = "Accesses a JFR buffer.")
    public void emitGCPhasePauseEvent(UnsignedWord gcEpoch, int level, String name, long startTicks) {
        JfrEvent event = getGCPhasePauseEvent(level);
        if (SubstrateJVM.isRecording() && SubstrateJVM.get().isEnabled(event)) {
            long end = JfrTicks.elapsedTicks();
            JfrNativeEventWriterData data = StackValue.get(JfrNativeEventWriterData.class);
            JfrNativeEventWriterDataAccess.initializeThreadLocalNativeBuffer(data);

            JfrNativeEventWriter.beginSmallEvent(data, event);      //@Label("GC Phase Pause") class GCPhasePause extends jdk.jfr.Event
            JfrNativeEventWriter.putLong(data, startTicks);         //@Label("Start Time") @Timestamp("TICKS")  long startTime;
            JfrNativeEventWriter.putLong(data, end - startTicks);   //@Label("Duration") @Timespan("TICKS") long duration;
            JfrNativeEventWriter.putEventThread(data);              //@Label("Event Thread") @Description("Thread in which event was committed in") Thread eventThread;
            JfrNativeEventWriter.putLong(data, gcEpoch.rawValue()); //@Unsigned @GcId @Label("GC Identifier") int gcId;
            JfrNativeEventWriter.putString(data, name);             //@Label("Name")  String name;
            JfrNativeEventWriter.endSmallEvent(data);
        }
    }

    //省略

}

// JfrGCEventSupportのインスタンスを登録する
@AutomaticallyRegisteredFeature
class JfrGCEventFeature implements InternalFeature {
    @Override
    public boolean isInConfiguration(IsInConfigurationAccess access) {
        return SubstrateOptions.UseSerialGC.getValue();
    }

    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access) {
        if (HasJfrSupport.get()) {
            JfrGCName name = JfrGCNames.singleton().addGCName("serial");
            ImageSingletons.add(JfrGCEventSupport.class, new JfrGCEventSupport(name));
        }
    }
}

```

#### GenScavengeGCName.java
このクラスは廃止され、替わりにJfrGCnameで管理するようになっている

#### JfrGCName.java

GCの名前を管理しているクラス。
`JfrGCEventFeature->beforeAnalysis`内で`JfrGCNames.singleton().addGCName("serial")`で生成されている


```Java

public class JfrGCName {
    private final int id;
    private final String name;

    @Platforms(Platform.HOSTED_ONLY.class)
    protected JfrGCName(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Uninterruptible(reason = "Called from uninterruptible code.", mayBeInlined = true)
    public int getId() {
        return id;
    }
}
```

#### GCCause


```java
package com.oracle.svm.core.heap;

import java.util.ArrayList;

import org.graalvm.nativeimage.Platform;
import org.graalvm.nativeimage.Platforms;

import com.oracle.svm.core.Uninterruptible;
import com.oracle.svm.core.feature.AutomaticallyRegisteredFeature;
import com.oracle.svm.core.feature.InternalFeature;
import com.oracle.svm.core.util.DuplicatedInNativeCode;
import com.oracle.svm.core.util.VMError;

/**
 * This class holds garbage collection causes that are common and therefore shared between different
 * garbage collector implementations.
 */
public class GCCause {
    @Platforms(Platform.HOSTED_ONLY.class) private static final ArrayList<GCCause> HostedGCCauseList = new ArrayList<>();

    @DuplicatedInNativeCode public static final GCCause JavaLangSystemGC = new GCCause("java.lang.System.gc()", 0);
    @DuplicatedInNativeCode public static final GCCause UnitTest = new GCCause("UnitTest", 1);
    @DuplicatedInNativeCode public static final GCCause TestGCInDeoptimizer = new GCCause("TestGCInDeoptimizer", 2);
    @DuplicatedInNativeCode public static final GCCause HintedGC = new GCCause("Hint", 3);

    protected static GCCause[] GCCauses = new GCCause[]{JavaLangSystemGC, UnitTest, TestGCInDeoptimizer, HintedGC};

    private final int id;
    private final String name;

    @Platforms(Platform.HOSTED_ONLY.class)
    protected GCCause(String name, int id) {
        this.id = id;
        this.name = name;
        addGCCauseMapping();
    }

    @Platforms(Platform.HOSTED_ONLY.class)
    private void addGCCauseMapping() {
        synchronized (HostedGCCauseList) {
            while (HostedGCCauseList.size() <= id) {
                HostedGCCauseList.add(null);
            }
            VMError.guarantee(HostedGCCauseList.get(id) == null, name + " and another GCCause have the same id.");
            HostedGCCauseList.set(id, this);
        }
    }

    public String getName() {
        return name;
    }

    @Uninterruptible(reason = "Called from uninterruptible code.", mayBeInlined = true)
    public int getId() {
        return id;
    }

    public static GCCause fromId(int causeId) {
        return GCCauses[causeId];
    }

    public static GCCause[] getGCCauses() {
        return GCCauses;
    }

    @Platforms(Platform.HOSTED_ONLY.class)
    public static void cacheReverseMapping() {
        GCCauses = HostedGCCauseList.toArray(new GCCause[HostedGCCauseList.size()]);
    }
}

@AutomaticallyRegisteredFeature
class GCCauseFeature implements InternalFeature {
    @Override
    public void beforeCompilation(BeforeCompilationAccess access) {
        GCCause.cacheReverseMapping();
        access.registerAsImmutable(GCCause.GCCauses);
    }
}

```

#### JfrType


JDKの定義するJFRの型のIDに対応する列挙型


```java

/**
 * Maps JFR types against their IDs in the JDK.
 */
public enum JfrType {
    Class("java.lang.Class"),
    String("java.lang.String"),
    Thread("java.lang.Thread"),
    ThreadState("jdk.types.ThreadState"),
    ThreadGroup("jdk.types.ThreadGroup"),
    StackTrace("jdk.types.StackTrace"),
    ClassLoader("jdk.types.ClassLoader"),
    Method("jdk.types.Method"),
    Symbol("jdk.types.Symbol"),
    Module("jdk.types.Module"),
    Package("jdk.types.Package"),
    FrameType("jdk.types.FrameType"),
    GCCause("jdk.types.GCCause"),
    GCName("jdk.types.GCName"),
    VMOperation("jdk.types.VMOperationType"),
    GCWhen( "jdk.types.GCWhen"),
    VirtualSpace("jdk.types.VirtualSpace");

    private final long id;

    JfrType(String name) {
        this.id = JfrMetadataTypeLibrary.lookupType(name);
    }

    public long getId() {
        return id;
    }
}
```

#### JfrTypeRepository

JfrTypeで定義した型をchunkに書き込む

https://github.com/oracle/graal/pull/3720


```java
package com.oracle.svm.core.jfr;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.oracle.svm.core.heap.VMOperationInfos;
import org.graalvm.compiler.serviceprovider.JavaVersionUtil;
import org.graalvm.nativeimage.Platform;
import org.graalvm.nativeimage.Platforms;

import com.oracle.svm.core.SubstrateUtil;
import com.oracle.svm.core.Uninterruptible;
import com.oracle.svm.core.heap.GCCause;
import com.oracle.svm.core.heap.Heap;
import com.oracle.svm.core.jfr.traceid.JfrTraceId;

/**
 * Repository that collects and writes used classes, packages, modules, and classloaders.
 */
public class JfrTypeRepository implements JfrConstantPool {
    @Platforms(Platform.HOSTED_ONLY.class)
    public JfrTypeRepository() {
    }

    @Uninterruptible(reason = "Called from uninterruptible code.", mayBeInlined = true)
    public long getClassId(Class<?> clazz) {
        return JfrTraceId.load(clazz);
    }

    @Override
    public int write(JfrChunkWriter writer) {
        // Visit all used classes, and collect their packages, modules, classloaders and possibly
        // referenced classes.
        TypeInfo typeInfo = collectTypeInfo();

        // The order of writing matters as following types can be tagged during the write process
        int count = writeClasses(writer, typeInfo);
        count += writePackages(writer, typeInfo);
        count += writeModules(writer, typeInfo);
        count += writeClassLoaders(writer, typeInfo);
        count += writeGCCauses(writer);
        count += writeGCNames(writer);
        count += writeVMOperations(writer);
        return count;
    }

    private TypeInfo collectTypeInfo() {
        TypeInfo typeInfo = new TypeInfo();
        for (Class<?> clazz : Heap.getHeap().getLoadedClasses()) {
            if (JfrTraceId.isUsedPreviousEpoch(clazz)) {
                JfrTraceId.clearUsedPreviousEpoch(clazz);
                visitClass(typeInfo, clazz);
            }
        }
        return typeInfo;
    }

    private void visitClass(TypeInfo typeInfo, Class<?> clazz) {
        if (clazz != null && typeInfo.addClass(clazz)) {
            visitPackage(typeInfo, clazz.getPackage(), clazz.getModule());
            visitClass(typeInfo, clazz.getSuperclass());
        }
    }

    private void visitPackage(TypeInfo typeInfo, Package pkg, Module module) {
        if (pkg != null && typeInfo.addPackage(pkg, module)) {
            visitModule(typeInfo, module);
        }
    }

    private void visitModule(TypeInfo typeInfo, Module module) {
        if (module != null && typeInfo.addModule(module)) {
            visitClassLoader(typeInfo, module.getClassLoader());
        }
    }

    private void visitClassLoader(TypeInfo typeInfo, ClassLoader classLoader) {
        // The null class-loader is serialized as the "bootstrap" class-loader.
        if (classLoader != null && typeInfo.addClassLoader(classLoader)) {
            visitClass(typeInfo, classLoader.getClass());
        }
    }

    public int writeClasses(JfrChunkWriter writer, TypeInfo typeInfo) {
        if (typeInfo.getClasses().isEmpty()) {
            return EMPTY;
        }
        writer.writeCompressedLong(JfrType.Class.getId());
        writer.writeCompressedInt(typeInfo.getClasses().size());

        for (Class<?> clazz : typeInfo.getClasses()) {
            writeClass(writer, typeInfo, clazz);
        }
        return NON_EMPTY;
    }

    private static void writeClass(JfrChunkWriter writer, TypeInfo typeInfo, Class<?> clazz) {
        JfrSymbolRepository symbolRepo = SubstrateJVM.getSymbolRepository();
        writer.writeCompressedLong(JfrTraceId.getTraceId(clazz));  // key
        writer.writeCompressedLong(typeInfo.getClassLoaderId(clazz.getClassLoader()));
        writer.writeCompressedLong(symbolRepo.getSymbolId(clazz.getName(), true, true));
        writer.writeCompressedLong(typeInfo.getPackageId(clazz.getPackage()));
        writer.writeCompressedLong(clazz.getModifiers());
        if (JavaVersionUtil.JAVA_SPEC >= 17) {
            writer.writeBoolean(SubstrateUtil.isHiddenClass(clazz));
        }
    }

    private static int writePackages(JfrChunkWriter writer, TypeInfo typeInfo) {
        Map<String, PackageInfo> packages = typeInfo.getPackages();
        if (packages.isEmpty()) {
            return EMPTY;
        }
        writer.writeCompressedLong(JfrType.Package.getId());
        writer.writeCompressedInt(packages.size());

        for (Map.Entry<String, PackageInfo> pkgInfo : packages.entrySet()) {
            writePackage(writer, typeInfo, pkgInfo.getKey(), pkgInfo.getValue());
        }
        return NON_EMPTY;
    }

    private static void writePackage(JfrChunkWriter writer, TypeInfo typeInfo, String pkgName, PackageInfo pkgInfo) {
        JfrSymbolRepository symbolRepo = SubstrateJVM.getSymbolRepository();
        writer.writeCompressedLong(pkgInfo.id);  // id
        writer.writeCompressedLong(symbolRepo.getSymbolId(pkgName, true, true));
        writer.writeCompressedLong(typeInfo.getModuleId(pkgInfo.module));
        writer.writeBoolean(false); // exported
    }

    private static int writeModules(JfrChunkWriter writer, TypeInfo typeInfo) {
        Map<Module, Long> modules = typeInfo.getModules();
        if (modules.isEmpty()) {
            return EMPTY;
        }
        writer.writeCompressedLong(JfrType.Module.getId());
        writer.writeCompressedInt(modules.size());

        for (Map.Entry<Module, Long> modInfo : modules.entrySet()) {
            writeModule(writer, typeInfo, modInfo.getKey(), modInfo.getValue());
        }
        return NON_EMPTY;
    }

    private static void writeModule(JfrChunkWriter writer, TypeInfo typeInfo, Module module, long id) {
        JfrSymbolRepository symbolRepo = SubstrateJVM.getSymbolRepository();
        writer.writeCompressedLong(id);
        writer.writeCompressedLong(symbolRepo.getSymbolId(module.getName(), true));
        writer.writeCompressedLong(0); // Version, e.g. "11.0.10-internal"
        writer.writeCompressedLong(0); // Location, e.g. "jrt:/java.base"
        writer.writeCompressedLong(typeInfo.getClassLoaderId(module.getClassLoader()));
    }

    private static int writeClassLoaders(JfrChunkWriter writer, TypeInfo typeInfo) {
        Map<ClassLoader, Long> classLoaders = typeInfo.getClassLoaders();
        if (classLoaders.isEmpty()) {
            return EMPTY;
        }
        writer.writeCompressedLong(JfrType.ClassLoader.getId());
        writer.writeCompressedInt(classLoaders.size());

        for (Map.Entry<ClassLoader, Long> clInfo : classLoaders.entrySet()) {
            writeClassLoader(writer, clInfo.getKey(), clInfo.getValue());
        }
        return NON_EMPTY;
    }

    private static int writeGCCauses(JfrChunkWriter writer) {
        // GCCauses has null entries
        GCCause[] causes = GCCause.getGCCauses();
        int nonNullItems = 0;
        for (int index = 0; index < causes.length; index++) {
            if (causes[index] != null) {
                nonNullItems++;
            }
        }

        assert nonNullItems > 0;

        writer.writeCompressedLong(JfrType.GCCause.getId());
        writer.writeCompressedLong(nonNullItems);
        for (GCCause cause : causes) {
            if (cause != null) {
                writer.writeCompressedLong(cause.getId());
                writer.writeString(cause.getName());
            }
        }
        return NON_EMPTY;
    }

    private static int writeGCNames(JfrChunkWriter writer) {
        JfrGCName[] gcNames = JfrGCNames.singleton().getNames();
        assert gcNames != null && gcNames.length > 0;

        writer.writeCompressedLong(JfrType.GCName.getId());
        writer.writeCompressedLong(gcNames.length);
        for (JfrGCName name : gcNames) {
            writer.writeCompressedLong(name.getId());
            writer.writeString(name.getName());
        }
        return NON_EMPTY;
    }

    private static int writeVMOperations(JfrChunkWriter writer) {
        String[] vmOperationNames = VMOperationInfos.getNames();
        assert vmOperationNames.length > 0;
        writer.writeCompressedLong(JfrType.VMOperation.getId());
        writer.writeCompressedLong(vmOperationNames.length);
        for (int id = 0; id < vmOperationNames.length; id++) {
            writer.writeCompressedLong(id + 1); // id starts with 1
            writer.writeString(vmOperationNames[id]);
        }

        return NON_EMPTY;
    }



    private static void writeClassLoader(JfrChunkWriter writer, ClassLoader cl, long id) {
        JfrSymbolRepository symbolRepo = SubstrateJVM.getSymbolRepository();
        writer.writeCompressedLong(id);
        if (cl == null) {
            writer.writeCompressedLong(0);
            writer.writeCompressedLong(symbolRepo.getSymbolId("bootstrap", true));
        } else {
            writer.writeCompressedLong(JfrTraceId.getTraceId(cl.getClass()));
            writer.writeCompressedLong(symbolRepo.getSymbolId(cl.getName(), true));
        }
    }

    private static class PackageInfo {
        private final long id;
        private final Module module;

        PackageInfo(long id, Module module) {
            this.id = id;
            this.module = module;
        }
    }

    private static class TypeInfo {
        private final Set<Class<?>> classes = new HashSet<>();
        private final Map<String, PackageInfo> packages = new HashMap<>();
        private final Map<Module, Long> modules = new HashMap<>();
        private final Map<ClassLoader, Long> classLoaders = new HashMap<>();
        private long currentPackageId = 0;
        private long currentModuleId = 0;
        private long currentClassLoaderId = 0;

        boolean addClass(Class<?> clazz) {
            return classes.add(clazz);
        }

        Set<Class<?>> getClasses() {
            return classes;
        }

        boolean addPackage(Package pkg, Module module) {
            if (!packages.containsKey(pkg.getName())) {
                // The empty package represented by "" is always traced with id 0
                long id = pkg.getName().isEmpty() ? 0 : ++currentPackageId;
                packages.put(pkg.getName(), new PackageInfo(id, module));
                return true;
            } else {
                assert module == packages.get(pkg.getName()).module;
                return false;
            }
        }

        Map<String, PackageInfo> getPackages() {
            return packages;
        }

        long getPackageId(Package pkg) {
            if (pkg != null) {
                return packages.get(pkg.getName()).id;
            } else {
                return 0;
            }
        }

        boolean addModule(Module module) {
            if (!modules.containsKey(module)) {
                modules.put(module, ++currentModuleId);
                return true;
            } else {
                return false;
            }
        }

        Map<Module, Long> getModules() {
            return modules;
        }

        long getModuleId(Module module) {
            if (module != null) {
                return modules.get(module);
            } else {
                return 0;
            }
        }

        boolean addClassLoader(ClassLoader classLoader) {
            if (!classLoaders.containsKey(classLoader)) {
                classLoaders.put(classLoader, ++currentClassLoaderId);
                return true;
            } else {
                return false;
            }
        }

        Map<ClassLoader, Long> getClassLoaders() {
            return classLoaders;
        }

        long getClassLoaderId(ClassLoader classLoader) {
            if (classLoader != null) {
                return classLoaders.get(classLoader);
            } else {
                return 0;
            }
        }
    }
}
```


### 4312に関連するシーケンス

https://github.com/oracle/graal/pull/4312

```Java

    @Uninterruptible(reason = "Accesses a JFR buffer.")
    public void emitGarbageCollectionEvent(UnsignedWord gcEpoch, GCCause cause, long start) {
        if (SubstrateJVM.isRecording() && SubstrateJVM.get().isEnabled(JfrEvent.GarbageCollection)) {
            long pauseTime = JfrTicks.elapsedTicks() - start;

            JfrNativeEventWriterData data = StackValue.get(JfrNativeEventWriterData.class);
            JfrNativeEventWriterDataAccess.initializeThreadLocalNativeBuffer(data);

            JfrNativeEventWriter.beginSmallEvent(data, JfrEvent.GarbageCollection);
            JfrNativeEventWriter.putLong(data, start);
            JfrNativeEventWriter.putLong(data, pauseTime);
            JfrNativeEventWriter.putLong(data, gcEpoch.rawValue());
            JfrNativeEventWriter.putLong(data, gcName.getId());
            JfrNativeEventWriter.putLong(data, cause.getId());
            JfrNativeEventWriter.putLong(data, pauseTime);  // sum of pause
            JfrNativeEventWriter.putLong(data, pauseTime);  // longest pause
            JfrNativeEventWriter.endSmallEvent(data);
        }
    }
    Field	Type	Description

https://github.com/oracle/graal/pull/4312

jdk.GarbageCollection {
  startTime = 12:59:39.673
  duration = 1.148 ms
  gcId = 1
  name = "GenScavenge"
  cause = "java.lang.System.gc()"
  sumOfPauses = 1.148 ms
  longestPause = 1.148 ms
}


JFRタイプの定義をする
https://github.com/oracle/graal/blob/d0a332cdabaa33165b4176c6f925bd5b23c01f55/substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jfr/JfrType.java
public enum JfrType {
    Class("java.lang.Class"),
    String("java.lang.String"),
    Thread("java.lang.Thread"),
    ThreadState("jdk.types.ThreadState"),
    ThreadGroup("jdk.types.ThreadGroup"),
    StackTrace("jdk.types.StackTrace"),
    ClassLoader("jdk.types.ClassLoader"),
    Method("jdk.types.Method"),
    Symbol("jdk.types.Symbol"),
    Module("jdk.types.Module"),
    Package("jdk.types.Package"),
    FrameType("jdk.types.FrameType"),
    GCCause("jdk.types.GCCause"),
    GCName("jdk.types.GCName");

private static int writeGCCauses(JfrChunkWriter writer) {
        // GCCauses has null entries
        GCCause[] causes = GCCause.getGCCauses();
        int nonNullItems = 0;
        for (int index = 0; index < causes.length; index++) {
            if (causes[index] != null) {
                nonNullItems++;
            }
        }

        assert nonNullItems > 0;

        writer.writeCompressedLong(JfrType.GCCause.getId());
        writer.writeCompressedLong(nonNullItems);
        for (GCCause cause : causes) {
            if (cause != null) {
                writer.writeCompressedLong(cause.getId());
                writer.writeString(cause.getName());
            }
        }
        return NON_EMPTY;
    }

    private static int writeGCNames(JfrChunkWriter writer) {
        JfrGCName[] gcNames = JfrGCNames.singleton().getNames();
        assert gcNames != null && gcNames.length > 0;

        writer.writeCompressedLong(JfrType.GCName.getId());
        writer.writeCompressedLong(gcNames.length);
        for (JfrGCName name : gcNames) {
            writer.writeCompressedLong(name.getId());
            writer.writeString(name.getName());
        }
        return NON_EMPTY;
    }



JfrTypeRepository.javanにも合わせて修正
https://github.com/oracle/graal/blob/d0a332cdabaa33165b4176c6f925bd5b23c01f55/substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jfr/JfrTypeRepository.java
@@ -62,6 +63,8 @@ public int write(JfrChunkWriter writer) {
        count += writePackages(writer, typeInfo);
        count += writeModules(writer, typeInfo);
        count += writeClassLoaders(writer, typeInfo);
        count += writeGCCauses(writer);
        count += writeGCNames(writer);
        return count;

substratevm/src/com.oracle.svm.hosted/src/com/oracle/svm/hosted/jfr/JfrFeature.java
public void afterRegistration(AfterRegistrationAccess access) {
 ImageSingletons.add(JfrGCNames.class, new JfrGCNames());



cause	        GCCause         Allocation Failure  5
gcId            uint    	    775                 3
longestPause	Tickspan	    6347542             7
name	        GCName	        DefNew              4
startTime	    long: millis	467567143625        1
sumOfPauses	    Tickspan	    6347542             6
```