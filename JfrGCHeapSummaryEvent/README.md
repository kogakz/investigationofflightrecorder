# GCHeapSummaryの実装

## 実装検討

1. [ビルド方法を調べる](GraalBuild.md)
2. [過去の実装を調べる](ResearchOfImplementation.md)
3. [テスト方法を調べる](test.md)

## 今の状況

[Heapグラフを見られるところまで進んだ](trial/TrialSummary.md)

テストコードの実装はこれから
