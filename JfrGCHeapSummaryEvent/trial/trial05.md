# JfrGCHeapSummaryEventSupportのImageSingletonsへの登録追加

参考にしたPR
https://github.com/oracle/graal/pull/4950 

```java
@AutomaticallyRegisteredFeature
class JfrGCHeapSummaryEventFeature implements InternalFeature {

    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access) {
        if (HasJfrSupport.get()) {
            ImageSingletons.add(JfrGCHeapSummaryEventSupport.class, new JfrGCHeapSummaryEventSupport());
        }
    }
}
```

## 結果

1. 生成したGraalVMでアプリケーションをビルド
   1. 正常終了すること（OK)
2. 1で生成したアプリケーションを実行
   1. 正常終了すること（OK)
3. FlightRecordのイベント出力
   1. 既存のイベントが出力されること（OK)
   2. GCHeapSummaryイベントが出力されること（OK）


## 証跡

イベント出力されるようになった
```shell

Event Type                              Count  Size (bytes)
============================================================
jdk.GCHeapSummary                         217           958
```

```shell
 jfr summary  jfr/native/native-addRegistered.jfr

 Version: 2.0
 Chunks: 1
 Start: 2023-03-14 15:06:18 (UTC)
 Duration: 60 s

 Event Type                              Count  Size (bytes) 
=============================================================
 jdk.JavaMonitorWait                      3037         80246
 jdk.GCPhasePauseLevel2                   1665         52129
 jdk.GCPhasePauseLevel1                   1448         46103
 jdk.GCPhasePause                          362          9998
 jdk.ActiveSetting                         251          8179
 jdk.SafepointBegin                        218          3242
 jdk.GCHeapSummary                         217           958
 jdk.GarbageCollection                     217          5000
 jdk.ExecuteVMOperation                    217          4034
 jdk.ExceptionStatistics                    59           969
 jdk.InitialEnvironmentVariable             38          4886
 jdk.InitialSystemProperty                  37          1593
 jdk.JavaThreadStatistics                    2            24
 jdk.PhysicalMemory                          2            28
 jdk.ThreadStart                             2            26
 jdk.ThreadEnd                               1            11
 jdk.Metadata                                1         68869
 jdk.CheckPoint                              1           844
 jdk.JVMInformation                          1           282
 jdk.OSInformation                           1            64
 jdk.ClassLoadingStatistics                  1            12
 jdk.ActiveRecording                         1           140
 jdk.ThreadSleep                             0             0
 jdk.ThreadPark                              0             0
 jdk.JavaMonitorEnter                        0             0
 jdk.JavaMonitorInflate                      0             0
 jdk.BiasedLockRevocation                    0             0
 jdk.BiasedLockSelfRevocation                0             0
 jdk.BiasedLockClassRevocation               0             0
 jdk.ReservedStackActivation                 0             0
 jdk.ClassLoad                               0             0
 jdk.ClassDefine                             0             0
 jdk.ClassUnload                             0             0
 jdk.IntFlagChanged                          0             0
 jdk.UnsignedIntFlagChanged                  0             0
 jdk.LongFlagChanged                         0             0
 jdk.UnsignedLongFlagChanged                 0             0
 jdk.DoubleFlagChanged                       0             0
 jdk.BooleanFlagChanged                      0             0
 jdk.StringFlagChanged                       0             0
 jdk.MetaspaceSummary                        0             0
 jdk.MetaspaceGCThreshold                    0             0
 jdk.MetaspaceAllocationFailure              0             0
 jdk.MetaspaceOOM                            0             0
 jdk.MetaspaceChunkFreeListSummary           0             0
 jdk.PSHeapSummary                           0             0
 jdk.G1HeapSummary                           0             0
 jdk.ParallelOldGarbageCollection            0             0
 jdk.YoungGarbageCollection                  0             0
 jdk.OldGarbageCollection                    0             0
 jdk.G1GarbageCollection                     0             0
 jdk.G1MMU                                   0             0
 jdk.EvacuationInformation                   0             0
 jdk.GCReferenceStatistics                   0             0
 jdk.ObjectCountAfterGC                      0             0
 jdk.G1EvacuationYoungStatistics             0             0
 jdk.G1EvacuationOldStatistics               0             0
 jdk.G1BasicIHOP                             0             0
 jdk.G1AdaptiveIHOP                          0             0
 jdk.PromoteObjectInNewPLAB                  0             0
 jdk.PromoteObjectOutsidePLAB                0             0
 jdk.PromotionFailed                         0             0
 jdk.EvacuationFailed                        0             0
 jdk.ConcurrentModeFailure                   0             0
 jdk.GCPhasePauseLevel3                      0             0
 jdk.GCPhasePauseLevel4                      0             0
 jdk.GCPhaseConcurrent                       0             0
 jdk.AllocationRequiringGC                   0             0
 jdk.TenuringDistribution                    0             0
 jdk.G1HeapRegionTypeChange                  0             0
 jdk.Compilation                             0             0
 jdk.CompilerPhase                           0             0
 jdk.CompilationFailure                      0             0
 jdk.CompilerInlining                        0             0
 jdk.SweepCodeCache                          0             0
 jdk.CodeCacheFull                           0             0
 jdk.SafepointStateSynchronization           0             0
 jdk.SafepointWaitBlocked                    0             0
 jdk.SafepointCleanup                        0             0
 jdk.SafepointCleanupTask                    0             0
 jdk.SafepointEnd                            0             0
 jdk.Shutdown                                0             0
 jdk.ObjectAllocationInNewTLAB               0             0
 jdk.ObjectAllocationOutsideTLAB             0             0
 jdk.OldObjectSample                         0             0
 jdk.DumpReason                              0             0
 jdk.DataLoss                                0             0
 jdk.VirtualizationInformation               0             0
 jdk.SystemProcess                           0             0
 jdk.CPUInformation                          0             0
 jdk.CPUTimeStampCounter                     0             0
 jdk.CPULoad                                 0             0
 jdk.ThreadCPULoad                           0             0
 jdk.ThreadContextSwitchRate                 0             0
 jdk.NetworkUtilization                      0             0
 jdk.ClassLoaderStatistics                   0             0
 jdk.ThreadAllocationStatistics              0             0
 jdk.ExecutionSample                         0             0
 EndChunkPeriodEvents                        0             0
 jdk.NativeMethodSample                      0             0
 jdk.ThreadDump                              0             0
 EveryChunkPeriodEvents                      0             0
 jdk.NativeLibrary                           0             0
 jdk.ModuleRequire                           0             0
 jdk.ModuleExport                            0             0
 jdk.CompilerStatistics                      0             0
 jdk.CompilerConfiguration                   0             0
 jdk.CodeCacheStatistics                     0             0
 jdk.CodeCacheConfiguration                  0             0
 jdk.CodeSweeperStatistics                   0             0
 jdk.CodeSweeperConfiguration                0             0
 jdk.IntFlag                                 0             0
 jdk.UnsignedIntFlag                         0             0
 jdk.LongFlag                                0             0
 jdk.UnsignedLongFlag                        0             0
 jdk.DoubleFlag                              0             0
 jdk.BooleanFlag                             0             0
 jdk.StringFlag                              0             0
 jdk.ObjectCount                             0             0
 jdk.G1HeapRegionInformation                 0             0
 jdk.GCConfiguration                         0             0
 jdk.GCSurvivorConfiguration                 0             0
 jdk.GCTLABConfiguration                     0             0
 jdk.GCHeapConfiguration                     0             0
 jdk.YoungGenerationConfiguration            0             0
 jdk.ZPageAllocation                         0             0
 jdk.ZThreadPhase                            0             0
 jdk.ZStatisticsCounter                      0             0
 jdk.ZStatisticsSampler                      0             0
 jdk.ShenandoahHeapRegionStateChange         0             0
 jdk.ShenandoahHeapRegionInformation         0             0
 jdk.JavaExceptionThrow                      0             0
 jdk.JavaErrorThrow                          0             0
```
