# JfrTypeにjdk.types.GCWhen,metadataに合わせて出力部分実装


jfr metadataコマンドでイベントの型情報の詳細を取得
```java
@Name("jdk.GCHeapSummary")
@Category({"Java Virtual Machine", "GC", "Heap"})
@Label("Heap Summary")
class GCHeapSummary extends jdk.jfr.Event {
  @Label("Start Time")
  @Timestamp("TICKS")
  long startTime;

  @Unsigned
  @GcId
  @Label("GC Identifier")
  @Label("GC Identifier")
  int gcId;

  @Label("When")
  String when;

  @Label("Heap Space")
  VirtualSpace heapSpace;

  @Unsigned
  @DataAmount("BYTES")
  @Label("Heap Used")
  @Description("Bytes allocated by objects in the heap")
  long heapUsed;
}
```
上記に合わせてコーディングした結果（heapspace,heapUsedの個所は0固定で出力) 

## 結果

jfr printの結果が出るようになった。

```java

jdk.GCHeapSummary {
  startTime = 14:31:12.783
  gcId = 220
  when = "Before GC"
  heapSpace = {
    start = 0x00000000
    committedEnd = 0x00000000
    committedSize = 0 bytes
    reservedEnd = 0x00000000
    reservedSize = 0 bytes
  }
  heapUsed = 0 bytes
}

jdk.GCHeapSummary {
  startTime = 14:31:12.786
  gcId = 220
  when = "After GC"
  heapSpace = {
    start = 0x00000000
    committedEnd = 0x00000000
    committedSize = 0 bytes
    reservedEnd = 0x00000000
    reservedSize = 0 bytes
  }
  heapUsed = 0 bytes
}
```

## 実装

```java
class JfrGCHeapSummaryEventSupport {

    private final JfrGCWhen before; 
    private final JfrGCWhen after;

    public JfrGCHeapSummaryEventSupport(JfrGCWhen before, JfrGCWhen after){
        this.before = before;
        this.after = after;
    }
    
    @Uninterruptible(reason = "Accesses a JFR buffer.")
    public void emitGCHeapSummaryEventBeforeGC(UnsignedWord gcEpoch, long start, long heapUsed) {
        
        if (SubstrateJVM.isRecording() && SubstrateJVM.get().isEnabled(JfrEvent.GCHeapSummary)) {

            JfrNativeEventWriterData data = StackValue.get(JfrNativeEventWriterData.class);
            JfrNativeEventWriterDataAccess.initializeThreadLocalNativeBuffer(data);

            JfrNativeEventWriter.beginSmallEvent(data, JfrEvent.GCHeapSummary);

            JfrNativeEventWriter.putLong(data, 0L);  //   @Label("Start Time") @Timestamp("TICKS") long startTime;

            JfrNativeEventWriter.putLong(data, gcEpoch.rawValue()); //  @Label("GC Identifier") int gcId;
            JfrNativeEventWriter.putLong(data, before.getId()); //  @Label("When") String when;

            // VirtualSpace
            JfrNativeEventWriter.putLong(data, 0L);     // start
            JfrNativeEventWriter.putLong(data, 0L);     // committedEnd : ulong
            JfrNativeEventWriter.putLong(data, 0L);     // committedSize : ulong
            JfrNativeEventWriter.putLong(data, 0L);     // reservedEnd : ulong
            JfrNativeEventWriter.putLong(data, 0L);     // reservedSize : ulong

            JfrNativeEventWriter.putLong(data, heapUsed); //  @Unsigned @DataAmount("BYTES")  @Label("Heap Used") @Description("Bytes allocated by objects in the heap")  long heapUsed;

            JfrNativeEventWriter.endSmallEvent(data);
        }
    }
```
