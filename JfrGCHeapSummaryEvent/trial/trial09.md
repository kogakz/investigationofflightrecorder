# 時刻の取得もとを変更

  JfrTicks.elapsedTicks()

## 結果

実際の時刻にあう値になって、グラフも出るようになった


![JMCの結果](images/trial09_01.png)

## 証跡

`jfr print --events jdk.GCHeapSummary jfr/native/native-heapUsed.jfr`

```java
jdk.GCHeapSummary {
  startTime = 06:21:41.279
  gcId = 222
  when = "Before GC"
  heapSpace = {
    start = 0x00000000
    committedEnd = 0x00000000
    committedSize = 0 bytes
    reservedEnd = 0x00000000
    reservedSize = 0 bytes
  }
  heapUsed = 365.4 MB
}

jdk.GCHeapSummary {
  startTime = 06:21:41.283
  gcId = 222
  when = "After GC"
  heapSpace = {
    start = 0x00000000
    committedEnd = 0x00000000
    committedSize = 0 bytes
    reservedEnd = 0x00000000
    reservedSize = 0 bytes
  }
  heapUsed = 268.0 MB
}
```

JAVAでの実行例との比較
`jfr print --events jdk.GCHeapSummary jfr/java-serial/serialgc.jfr`

```java
jdk.GCHeapSummary {
  startTime = 06:30:06.131
  gcId = 335
  when = "Before GC"
  heapSpace = {
    start = 0xE7000000
    committedEnd = 0xFF2B0000
    committedSize = 386.7 MB
    reservedEnd = 0x100000000
    reservedSize = 400.0 MB
  }
  heapUsed = 385.8 MB
}

jdk.GCHeapSummary {
  startTime = 06:30:06.144
  gcId = 335
  when = "After GC"
  heapSpace = {
    start = 0xE7000000
    committedEnd = 0xFF2B0000
    committedSize = 386.7 MB
    reservedEnd = 0x100000000
    reservedSize = 400.0 MB
  }
  heapUsed = 290.1 MB
}
```
