# heapUsedの値の出力箇所を実装

GCLogで出力元を参照している実装をもとに`JfrTicks.elapsedTicks(),getChunkBytes().rawValue()`で出力を実装
※UnsignedWordはlong型の互換性がなかったが継承元WordBaseのメソッド`long rawValue()`で変換できることを確認した。

```java

    private void printGCAfter(String cause) {
      //省略
      UnsignedWord sizeAfter = getChunkBytes();
      //省略
      printGCLog.unsigned(sizeAfter.unsignedDivide(1024)).string("K, ");
      //省略
    }

    //
    public static UnsignedWord getChunkBytes() {
        UnsignedWord youngBytes = HeapImpl.getHeapImpl().getYoungGeneration().getChunkBytes();
        UnsignedWord oldBytes = HeapImpl.getHeapImpl().getOldGeneration().getChunkBytes();
        return youngBytes.add(oldBytes);
    }

    public interface UnsignedWord extends ComparableWord {
```





```
上記に合わせてコーディングした結果（heapspace,heapUsedの個所は0固定で出力) 


## 結果

- JFRにGCログの値と比べて妥当な値が出力されていた
  - GC前：361.6MB (370267K÷1024の四捨五入）
  - GC後：264.2MB (270560K÷1024の切り上げ）
- JMCで見たところグラフが出なかった。
  - startTimeの値が実際の時刻と一致していない（Javaの場合は一致している）


`jfr print --events jdk.GCHeapSummary jfr/native/native-heapUsed.jfr`

```java
jdk.GCHeapSummary {
  startTime = 15:22:13.671
  gcId = 221
  when = "Before GC"
  heapSpace = {
    start = 0x00000000
    committedEnd = 0x00000000
    committedSize = 0 bytes
    reservedEnd = 0x00000000
    reservedSize = 0 bytes
  }
  heapUsed = 361.6 MB
}

jdk.GCHeapSummary {
  startTime = 15:22:13.674
  gcId = 221
  when = "After GC"
  heapSpace = {
    start = 0x00000000
    committedEnd = 0x00000000
    committedSize = 0 bytes
    reservedEnd = 0x00000000
    reservedSize = 0 bytes
  }
  heapUsed = 264.2 MB
}

```

GCログの値

```text
[[119907450189858 GC: before  epoch: 221  cause: CollectOnAllocation]
[Full GC (CollectOnAllocation) 370267K->270560K, 0.0030883 secs]
 [119907453333171 GC: after   epoch: 221  cause: CollectOnAllocation  policy: adaptive  type: complete
  czollection time: 3088328 nanoSeconds]]
```
