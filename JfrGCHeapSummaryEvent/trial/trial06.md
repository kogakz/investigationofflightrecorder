# 出力箇所追加（GC後）

GCImpl->printAfterGCCからのイベント出力の呼び出しを追加した

## 結果

件数は想定通り
※jfr printで中身を見ようとしたら、エラーになったが、そもそもgcid以外出力していないためと想定

1. 生成したGraalVMでアプリケーションをビルド
   1. 正常終了すること（OK)
2. 1で生成したアプリケーションを実行
   1. 正常終了すること（OK)
3. FlightRecordのイベント出力
   1. 既存のイベントが出力されること（OK)
   2. GCHeapSummaryイベント出力数がepochの数×2になること(OK)

## 証跡

```
 Event Type                              Count  Size (bytes) 
=============================================================
jdk.GCHeapSummary                         440          1946
```

`grep epoch log/native/gc.log` のepochの最大値は220

```
[33265905714777 GC: after   epoch: 220  cause: CollectOnAllocation  policy: adaptive  type: complete
```

## 参考

jfr printを実施するとエラーになった。
GCHeapSummaryは、gcidしか書いてなかったので想定内だったが、他のイベントでもエラーになってしまったので適切なファイルフォーマットで出力できず、読み取り時にエラーになっていると想定。

```shell
jfr print --events jdk.GCHeapSummary jfr/native/native-addAfterGC.jfr 
jfr print: could not read recording at /home/kogakz/gitouhon2022-2nd/inspections/inspections/case01/jfr/native/native-addAfterGC.jfr. Trying to read at 808833194, but file is only 289916 bytes.
```

イベントを変えてもNG

```shell
jfr print --events jdk.GCPhasePauseLevel2 jfr/native/native-addAfterGC.jfr 
jfr print: could not read recording at /home/kogakz/gitouhon2022-2nd/inspections/inspections/case01/jfr/native/native-addAfterGC.jfr. Trying to read at 808833194, but file is only 289916 bytes.
```
