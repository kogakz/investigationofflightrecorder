# JfrEventにイベント名を追加してビルド

## 結果

1. GraalVMのビルド（NG：イベント名誤りが原因）

## 証跡

graal-master/substratevm/src/com.oracle.svm.core/src/com/oracle/svm/core/jfr
```java
public final class JfrEvent {
    public static final JfrEvent ThreadStart = create("jdk.ThreadStart");
    public static final JfrEvent ThreadEnd = create("jdk.ThreadEnd");
    public static final JfrEvent DataLoss = create("jdk.DataLoss");
    public static final JfrEvent ClassLoadingStatistics = create("jdk.ClassLoadingStatistics");
    public static final JfrEvent InitialEnvironmentVariable = create("jdk.InitialEnvironmentVariable");
    public static final JfrEvent InitialSystemProperty = create("jdk.InitialSystemProperty");
    public static final JfrEvent JavaThreadStatistics = create("jdk.JavaThreadStatistics");
    public static final JfrEvent JVMInformation = create("jdk.JVMInformation");
    :

    //追加 イベント名が正しくないとビルド時にエラーになる
    public static final JfrEvent GCHeapSummaryEvent = create("jdk.GCHeapSummaryEvent"); // Eventを間違えてつけていた
```
Caused by: com.oracle.svm.core.util.VMError$HostedError: Event/Type jdk.GCHeapSummaryEvent was not found! The most similar event/type is 'jdk.GCHeapSummary' (class jdk.jfr.internal.PlatformEventType). Take a look at 'metadata.xml' to see all available events.
