# 試行結果サマリ

1. [JfrEventにイベント名を追加してビルド(NG:ビルド失敗）](trial01.md)
   1. GraalVMのビルド（NG：イベント名誤りが原因）
2. [イベント名を修正してビルド(OK:アプリのデグレもなし）](trial02.md)
   1. 生成したGraalVMでアプリケーションをビルド（正常終了）
   2. 1で生成したアプリケーションを実行（正常終了）
   3. FlightRecordのイベント出力（変化なし：想定通り）
3. [JfrGCHeapSummaryEvent,JfrGCHeapSummaryEventSupportのクラス定義追加(OK:アプリのデグレもなし）](trial03.md))
   1. 生成したGraalVMでアプリケーションをビルド（正常終了）
   2. 1で生成したアプリケーションを実行（正常終了）
   3. FlightRecordのイベント出力（変化なし：想定通り）
4. [GCImpl->printBeforeGCからのイベント出力の呼び出し追加（NG:GCHeapSummaryイベントが出力されない）](trial04.md))
   1. 生成したGraalVMでアプリケーションをビルド（正常終了）
   2. 1で生成したアプリケーションを実行（正常終了）
   3. FlightRecordのイベント出力（GCHeapSummaryイベント出力：出ない（想定外））
5. [JfrGCHeapSummaryEventSupportのImageSingletonsへの登録追加(OK:イベント出力される)](trial05.md))
   1. 生成したGraalVMでアプリケーションをビルド（正常終了）
   2. 1で生成したアプリケーションを実行（正常終了）
   3. FlightRecordのイベント出力（GCHeapSummaryイベント：出力されている）
6. [## GCImpl->printAfterGCCからのイベント出力の呼び出し追加(OK：期待通りの数イベント出力された)](trial06.md))
   1. 生成したGraalVMでアプリケーションをビルド（正常終了）
   2. 1で生成したアプリケーションを実行（正常終了）
   3. FlightRecordのイベント出力（GCHeapSummaryイベント：epochの数×2出ている）

JfrXXXEvent,JfrXXXXEventSupport以外の部分の実装を再調査して追加

7. [JfrTypeにjdk.types.GCWhen,metadataに合わせて出力部分実装（NG:JMCの内容が見れない）](trial07.md))
   1. 生成したGraalVMでアプリケーションをビルド（正常終了）
   2. 1で生成したアプリケーションを実行（正常終了）
   3. FlightRecordのイベント出力(jfr printの出力が見れること（OK）
8. [heapUsedの値の出力箇所を実装（NG:JMCの内容が見れない）](trial08.md))
   1. 生成したGraalVMでアプリケーションをビルド（正常終了）
   2. 1で生成したアプリケーションを実行（正常終了）
   3. FlightRecordのイベント出力(heapUsedの値が見れること:OK）
   4. JCMのグラフが見れること（NG)
9. [時刻の取得もとを変更](trial09.md)
   1.  生成したGraalVMでアプリケーションをビルド（正常終了）
   2.  1で生成したアプリケーションを実行（正常終了）
   3.  FlightRecordのイベント出力(heapUsedの値が見れること:OK）
   4.  JCMのグラフが見れること（OK)
