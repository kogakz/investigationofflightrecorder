# FlightRecorderの調査

調べ方の観点の整理するため、まとめ方の参考になりそうなドキュメントを読んでみる。

以下で検索したものから選定

|             タイトル              |       記載       | ステータス |                      記載概要                      |
| --------------------------------- | :--------------: | :--------: | -------------------------------------------------- |
| JDKフライトレコーダー OpenJDK     |    RedHat公式    |   確認済   | 下参照                                             |
| ツールリファレンス                |      Oracle      |   確認済   | JFRの取得手順が書かれている                        |
| JMC（公式サイト）                 |      Oracle      |   確認済   | JMC公式サイト記載概要参照                          |
| トラブルシュート                  |      Oracle      |   確認済   | JFRやJMCを使ったトラブルシュートの説明             |
| 取得可能イベント一覧              |    個人サイト    |   確認済   | OpenJDKで取得できるイベントの一覧                  |
| Red Hat Developerのブログ記事翻訳 |  RedHat(ブログ)  |   確認済   | RedhatのOpenJDK8でのFlight Recorderの使い方        |
| その他公式                        |      Oracle      |   確認済   | 色々見たが、特に今回有用なものはなかった           |
| JDK Flight Recorderの概要まとめ   |    個人サイト    |   確認済   | 公式で書いている内容が、わかりやすくまとまっていた |
| foojay の Flight Recorderの記事   | Javaコミュニティ |   確認済   | いろいろあるが、特に今回有用なものはなかった       |
|                                   |                  |            |                                                    |


## JDKフライトレコーダー OpenJDK (Readhat公式）
https://access.redhat.com/documentation/ja-jp/openjdk/8/html-single/using_jdk_flight_recorder_with_openjdk/index

OpenJDK17の引数説明
https://access.redhat.com/documentation/ja-jp/openjdk/17/pdf/using_jdk_flight_recorder_with_openjdk/openjdk-17-using_jdk_flight_recorder_with_openjdk-ja-jp.pdf


### JMCで見れるパフォーマンス情報での分類
JMC コンソールは、実行中の JVM に関する以下のパフォーマンス情報を表示できます。

- CPU
- 例外
- ファイル I/O
- ロックインスタンス
- メモリー使用量
- メソッドプロファイリング
- MBean オブジェクト
- ソケット I/O
- スレッドおよびスレッドダンプ

### JMCでのページでの分類
JMCでは9つの表示ページがある。

- 自動分析結果
- 環境
- クラスローディング
- コンパイル
- ガベージコレクション
- GC 設定
- メモリー
- TLAB 割り当て
- 仮想マシンの操作

## ツールリファレンス(OpenJD11版)
https://docs.oracle.com/en/java/javase/11/tools/java.html#GUID-3B1CE181-CD30-4178-9602-230B800D4FAE

JFRの取得手順が書かれている

- プロセス起動時
- 実行中プロセスへの取得
  - jcmd
  - JDK Mission Control



## JMC公式サイト記載概要

<https://www.oracle.com/java/technologies/jdk-mission-control.html>
[読み込んだ結果](doc/JMC.md)、特に今回の調査で有用な情報はなかった。

## トラブルシュート調査結果
[読み込んだ結果](doc/troubleshoot/00_main.md)、JFRを使って調査できることはトラブルシュートガイドで一通り網羅されていた。

## トラブルシュート調査結果

取得可能イベント一覧 は、下記サイトのものを参考にできることが分かった。
https://bestsolution-at.github.io/jfr-doc/openjdk-18.html

別サイトもあり（以下のサイトのほうが読みやすかった）
https://sap.github.io/SapMachine/jfrevents/


## その他 公式サイトの調査

1. OpenJDKドキュメント
記録する対象の設定をsettingsオプションで設定できるという話

2. https://docs.oracle.com/en/java/javase/19/jfapi/flight-recorder-configurations.html

3. Flight Recorder API Programmer's Guide
https://docs.oracle.com/en/java/javase/19/jfapi/why-use-jfr-api.html
 フライトレコーダーのAPIを使って、独自のイベントやデータの収集をする方法の説明

4. JDK Mission Control User's Guide 8
https://docs.oracle.com/en/java/java-components/jdk-mission-control/8/user-guide/
`6 Flight Recorder`の箇所に概要的なことと、JMCを使って見れるものの説明があった。
   - スレッド
     - 各スレッドのStack Trace情報
       - 利用例：デッドロックの発生の特定
    - インスタンスのロック
      - スレッドがロックを取ったかどうか
    - メモリ
       - ヒープ メモリ
       - ガベージ コレクションによる一時停止時間
    - メソッドプロファイリング
      - メソッドにかかった時間
    - JVM Internalsページ
      - ガベージ コレクション
        - 一時停止時間と比較したヒープ使用量と、指定された期間中のヒープ使用量の変化など
    - 環境
      - プロセスページ
        - 実行中の同時プロセスと、これらのプロセスの競合する CPU 使用率
    - イベント ブラウザ
        - イベント タイプの統計
            - ボトルネックを調べるのに使う

## Red Hat Developerのブログ記事翻訳
<https://rheb.hatenablog.com/entry/get-started-with-jdk-flight-recorder-in-openjdk-8u>


## JDK Flight Recorderの概要まとめ

 https://koduki.github.io/docs/book-introduction-of-jfr/site/01/01-what_is_JFR.html

## foojay の Flight Recorderの記事

### JMCの開発リーダーのブログ

https://foojay.io/today/author/hirt/

### JDK Flight Recorderに関するブログ

https://foojay.io/today/category/tools/jdk-flight-recorder/

#### Brice Dutheil さんの記事

以下の記事は元のブログを呼んだほうが良い。元ブログの抜粋で途中で終わっていた。

  1. [Get Started with JDK Flight Recorder](https://foojay.io/today/using-java-flight-recorder-and-mission-control-part-1/)
  2. [Analyze JFR Recordings](https://foojay.io/today/using-java-flight-recorder-and-mission-control-part-2/)
  3. [Control JFR Programmatically](https://foojay.io/today/using-java-flight-recorder-and-mission-control-part-3/)
  4. [How & When to Use JDK Flight Recorder in Production](https://foojay.io/today/how-when-to-use-jdk-flight-recoder-in-production/)

[元記事](https://blog.arkey.fr/2020/06/28/using-jdk-flight-recorder-and-jdk-mission-control/)で参考になった点

  - 目的ごとに設定を複数指定する。
  - dumponexitは設定ごとに書くということ
  - 解析＆チューニング事例

### Gunnar Morling さんの記事

1. [Monitoring REST APIs with Custom JDK Flight Recorder Events](https://foojay.io/today/monitoring-rest-apis-with-custom-flight-recorder-events/)
August 26, 2020
[元記事](https://www.morling.dev/blog/rest-api-monitoring-with-custom-jdk-flight-recorder-events/)
カスタムイベントを使ってREST APIの監視をする例
- リクエスト数の追跡​​
- 実行時間の長いリクエストの特定

jdk.jfr.Eventを拡張して、

1. [Introducing JmFrX: A Bridge From JMX to Java Flight Recorder](https://foojay.io/today/introducing-jmfrx-a-bridge-from-jmx-to-java-flight-recorder/)
September 03, 2020
[元記事](https://www.morling.dev/blog/introducing-jmfrx-a-bridge-from-jmx-to-jdk-flight-recorder/)

3. [Towards Continuous Performance Regression Testing](https://foojay.io/today/towards-continuous-performance-regression-testing/)
February 25, 2021
[元記事](https://www.morling.dev/blog/towards-continuous-performance-regression-testing/)

### Marcus Hirt さんの記事

1. [A Closer Look at JFR Streaming](https://foojay.io/today/a-closer-look-at-jfr-streaming/)
August 17, 2020
JFR Streamingの概要が書かれていた

2. [Continuous Production Profiling and Diagnostics](https://foojay.io/today/continuous-production-profiling-and-diagnostics/)
November 09, 2020

3. [OpenJDK and the Future of Production Profiling](https://foojay.io/today/openjdk-and-the-future-of-production-profiling/)
June 24, 2021

4. [JMC 8.0.1 Released!](https://foojay.io/today/jmc-8-0-1-released/)
June 25, 2021

5. [Contributing to OpenJDK Mission Control](https://foojay.io/today/contributing-to-openjdk-mission-control/)
October 18, 2021

#### 他の方の記事

以下は、まだちゃんと読んでない

5. [The SolarWinds Hack for Java Developers](https://foojay.io/today/the-solarwinds-hack-for-java-developers/)
February 09, 2021

6. [Improved JFR Allocation Profiling in JDK 16](https://foojay.io/today/improved-jfr-allocation-profiling-in-jdk-16/)
February 17, 2021

10. [The Costs of Hidden Logging](https://foojay.io/today/the-costs-of-hidden-logging/)
October 11, 2021

12. [Monitoring Spring Boot Applications (Part 1)](https://foojay.io/today/monitoring-spring-boot-applications-part-1/)
January 12, 2022

17. [Thinking About Massive Throughput? Meet Virtual Threads!](https://foojay.io/today/thinking-about-massive-throughput-meet-virtual-threads/)
April 28, 2022


# 実装について調査

case07で確認したjdk.SocketReadの実装について調べてみる。
以下の順で調査する。

- openjdk
- graalvm

## openjdk

調査元
1. JDKのgithub https://github.com/openjdk/jdk

### 確認したこと

1. イベント名`jdk.SocketRead`で検索
設定を登録するテストコードが引っかかったが、本体は引っかからず

<figure><img src='images/221203_113809.png' width='600px'><figcaption>図. jdk.SocketReadでの検索結果</figcaption></figure>

2. `SocketRead`で検索


<figure><img src='images/221203_114028.png' width='600px'><figcaption>図. SocketReadでの検索結果</figcaption></figure>


testコードをjfcを除くと、４つのコードが引っかかった

| No  |        ソース名        |                                   概要                                    |
| --- | ---------------------- | ------------------------------------------------------------------------- |
| 1   | SocketReadEvent.java   | jdk.SocketReadの要素定義をしているクラス                                  |
| 2   | Snippets.java          | Flight Recorderデータを使用するためのサンプルプログラムのパッケージの一部 |
| 3   | PlatformEventType.java | JFR自体が抽象的に各イベントを扱うためのクラス                             |
| 4   | DCmdStart.java         | jcmdでJFR.startを指定するときのソースっぽい。                             |



[src/jdk.jfr/share/classes/jdk/jfr/events/SocketReadEvent.java](https://github.com/openjdk/jdk/blob/50d47de8358e2f22bf3a4a165d660c25ef6eacbc/src/jdk.jfr/share/classes/jdk/jfr/events/SocketReadEvent.java)

[SocketReadEvent](doc/SocketReadEvent.md)のコード確認結果

```java{.line-numbers}
import jdk.jfr.internal.Type;

// Event名をjdk.SocketReadとして設定
@Name(Type.EVENT_NAME_PREFIX + "SocketRead")
// Labelは要素に判読可能な名前を設定するannotation
@Label("Socket Read")
@Category("Java Application")
// Descrioption 文または2つを使用して要素を記述するannotation
@Description("Reading data from a socket")
// AbstractJDKEventはjdk.jfr.Eventの拡張
public final class SocketReadEvent extends AbstractJDKEvent {

```


---

対象のパッケージのjavadocを見たところ、Flight Recorderデータを使用するためのクラスが含まれています。
記録中のすべてのメソッド・サンプルのヒストグラムを表示するサンプル・プログラム。

 
https://docs.oracle.com/javase/jp/10/docs/api/jdk/jfr/consumer/package-summary.html


[src/jdk.jfr/share/classes/jdk/jfr/consumer/snippet-files/Snippets.java](https://github.com/openjdk/jdk/blob/b73363fd7b3295635a2ccce0cea72586643c5bb4/src/jdk.jfr/share/classes/jdk/jfr/consumer/snippet-files/Snippets.java)

```java{.line-numbers}
            r.enable("jdk.SocketWrite").withoutThreshold();
            r.enable("jdk.SocketRead").withoutThreshold();
```

---

JFR自体が抽象的に各イベントを扱うためのクラス

[src/jdk.jfr/share/classes/jdk/jfr/internal/PlatformEventType.java](https://github.com/openjdk/jdk/blob/c7f65438bb4a4fd449bd19b68574cfa4b42d7ca8/src/jdk.jfr/share/classes/jdk/jfr/internal/PlatformEventType.java)
```java{.line-numbers}

/**
 * Implementation of event type.
 *
 * To avoid memory leaks, this class must not hold strong reference to an event
 * class or a setting class
 */
public final class PlatformEventType extends Type {

            case Type.EVENT_NAME_PREFIX + "SocketRead"  :
            case Type.EVENT_NAME_PREFIX + "SocketWrite" :
```

---
jcmdでJFR.startを指定するときのソースっぽい。

[src/jdk.jfr/share/classes/jdk/jfr/internal/dcmd/DCmdStart.java](https://github.com/openjdk/jdk/blob/324bec19aa9b9d4944a7e1129d494d57a077ba51/src/jdk.jfr/share/classes/jdk/jfr/internal/dcmd/DCmdStart.java)
```java{.line-numbers}
/**
 * JFR.start
 *
 */
//Instantiated by native
    private boolean hasJDKEvents(Map<String, String> settings) {
        String[] eventNames = new String[7];
        eventNames[0] = "FileRead";
        eventNames[1] = "FileWrite";
        eventNames[2] = "SocketRead";
        eventNames[3] = "SocketWrite";
        eventNames[4] = "JavaErrorThrow";
        eventNames[5] = "JavaExceptionThrow";
        eventNames[6] = "FileForce";
        for (String eventName : eventNames) {
            if ("true".equals(settings.get(Type.EVENT_NAME_PREFIX + eventName + "#enabled"))) {
                return true;
            }
        }
        return false;
    }
```

`public String[] printHelp() `を見ると
```text{.line-numbers}
Syntax : JFR.start [options]

Options:

  delay           (Optional) Length of time to wait before starting to record
                  (INTEGER followed by 's' for seconds 'm' for minutes or h' for
                  hours, 0s)
  省略
  $ jcmd <pid> JFR.start
  $ jcmd <pid> JFR.start filename=dump.jfr
  $ jcmd <pid> JFR.start filename=%s
  $ jcmd <pid> JFR.start dumponexit=true
  $ jcmd <pid> JFR.start maxage=1h,maxsize=1000M
  $ jcmd <pid> JFR.start settings=profile
  $ jcmd <pid> JFR.start delay=5m,settings=my.jfc
  $ jcmd <pid> JFR.start gc=high method-profiling=high
  $ jcmd <pid> JFR.start jdk.JavaMonitorEnter#threshold=1ms
  $ jcmd <pid> JFR.start +HelloWorld#enabled=true +HelloWorld#stackTrace=true
  $ jcmd <pid> JFR.start settings=user.jfc com.example.UserDefined#enabled=true
  $ jcmd <pid> JFR.start settings=none +Hello#enabled=true
```

って感じだった。

## GraalVMのJFRの調査結果
[調査結果のまとめ](doc/graalvmJfr.md)